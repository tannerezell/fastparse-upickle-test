name := "parser-test"

version := "1.0"

scalaVersion := "2.11.8"

enablePlugins(ScalaJSPlugin)

libraryDependencies += "com.lihaoyi" %% "fastparse" % "0.4.1"

libraryDependencies += "com.lihaoyi" %% "upickle" % "0.4.3"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"

libraryDependencies += "mysql" % "mysql-connector-java" % "6.0.6"


libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.1" % "test"

libraryDependencies += "joda-time" % "joda-time" % "2.9.7"