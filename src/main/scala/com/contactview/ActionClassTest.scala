package com.contactview

import java.lang.reflect.Parameter

import scala.beans.BeanProperty
import scala.collection.mutable
import scala.util.{Success, Try}

/** ***********************************************************************
  * Copyright (C) 2010-2016 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 12/15/16
  * ************************************************************************/
class TestClass(str : String) {
  var hello : String = ""
  //    def getHello : String = hello
}

object ActionClassTest extends App {

  var hello : String = "hello"

  case class Constructor(arguments : Seq[Field])
  case class Field(name : String, className : ClassRef)
  case class Method(name : String, returnClass : ClassRef, arguments : Seq[Field])
  case class ClassRef(className : String, isArray : Boolean)
  case class ActionClass(className : String, ancestors : Seq[String], constructors : Seq[Constructor], fields : Seq[Field], methods : Seq[Method])


  val knownClasses = mutable.HashMap.empty[String, ActionClass]
  val knownAncestors = mutable.HashMap.empty[String, Seq[String]]

  def findAncestors(className : String, found : Seq[String])(originalClassName : String) : Seq[String] = {
    knownAncestors.get(className) match {
      case Some(ancestors) => ancestors
      case _ =>
        Try(Class.forName(className)) match {
          case Success(clazz) if clazz != null  && clazz.getSuperclass != null => findAncestors(clazz.getSuperclass.getCanonicalName, found ++ Seq(clazz.getCanonicalName))(originalClassName)
          case Success(clazz) if clazz != null => found ++ Seq(clazz.getCanonicalName)
          case _ =>
            knownAncestors += originalClassName -> found
            found
        }
    }
  }

  def class2ac(className : String) : Option[ActionClass] = {
    def param2field(parameter : Parameter) : Field = {
      val name = parameter.getName
      val clazz = parameter.getType
      val isArray = clazz.isArray
      val clz = ClassRef(clazz.getCanonicalName.replace("[]", ""), isArray)
      Field(name, clz)
    }

    knownClasses.get(className) match {
      case Some(clazz) => Some(clazz)
      case _ =>
        Try(Class.forName(className)) match {
          case Success(clazz) if clazz != null =>
            val ancestors = findAncestors(className, Seq())(className)//if (superClazz != null) Seq(superClazz.getCanonicalName) else Seq()
            val constructors = clazz.getDeclaredConstructors.map {
              ctor =>
                Constructor(ctor.getParameters.map(param2field))
            }
            val isArray = clazz.isArray
            val fields = clazz.getDeclaredFields.map {
              field =>
                Field(field.getName, ClassRef(field.getType.getCanonicalName, field.getType.isArray))
            }
            val methods = clazz.getDeclaredMethods.map {
              method =>
                Method(method.getName, ClassRef(method.getReturnType.getCanonicalName, method.getReturnType.isArray), method.getParameters.map(param2field))
            }
            val result = ActionClass(clazz.getCanonicalName, ancestors, constructors, fields, methods)
            knownClasses += className -> result
            Some(result)
          case anythingElse =>
            val result = ActionClass(className, Seq(), Seq(), Seq(), Seq())
            knownClasses += className -> result
            Some(result)
        }
    }
  }

  val str = class2ac("java.lang.String")
  def printAc(ac : ActionClass) : String = {
    s"className : ${ac.className}\n" +
      "\tAncestors:\n" +
        { ac.ancestors.map(v => s"\t\t$v\n").mkString("") } +
      "\tConstructors:\n" +
          { ac.constructors.map(ctor => s"\t\t${ac.className.split('.').last}(${ctor.arguments.map(v => v.name + " : " + v.className.className + { if (v.className.isArray) "[]" else "" }).mkString(", ")})\n").mkString } +
      "\tMethods:\n" +
          { ac.methods.map(method => s"\t\t${method.name}(${method.arguments.map(arg => arg.name + " : " + arg.className.className).mkString(", ")}) : ${method.returnClass.className + { if (method.returnClass.isArray) "[]" else "" }}\n").mkString } +
      "\tFields:\n" +
          { ac.fields.map(field => "\t\t" + field.name + " : " + field.className.className).mkString("\n")}
  }
  val int = class2ac("java.lang.Integer")



  /**
    * Determine if a given type toCheck is a sub type of compareTo
    * @param toCheck class name to check
    * @param compareTo class to compare to
    * @return True if toCheck is acceptable for compareTo
    */
  def doesInheritFrom(toCheck : String, compareTo : String) : Boolean = {
    toCheck == compareTo || findAncestors(toCheck, Seq())(toCheck).contains(compareTo)
  }


  def doClassRefsMatch(toCheck : Seq[ClassRef], compareTo : Seq[ClassRef]) : Boolean = {
    if (toCheck.length == compareTo.length) {
      // first test okay, now the more expensive tests
      val _types = toCheck zip compareTo
      _types.count{ case (l, r) => doesInheritFrom(r.className, l.className)} == toCheck.length
    } else {
      false
    }
  }
  def findConstructorByTypes(ac : ActionClass, types : Seq[ClassRef]) : Option[Constructor] = {
    ac.constructors.find {
      ctor =>
        val cRef: Seq[ClassRef] = ctor.arguments.map(_.className)
        doClassRefsMatch(cRef, types)
    }
  }

  def findMethodByTypes(ac : ActionClass, methodName : String, types : Seq[ClassRef]) : Option[Method] = {
    ac.methods.find {
      method =>
        if (method.name == methodName) {
          val mRef: Seq[ClassRef] = method.arguments.map(_.className)
          doClassRefsMatch(mRef, types)
        } else {
          false
        }
    }
  }

  def findFieldByName(ac : ActionClass, fieldName : String): Option[Field] = ac.fields.find(_.name == fieldName)

  str.foreach(v => println(printAc(v)))
  int.foreach(v => println(printAc(v)))
  println(doesInheritFrom("java.lang.String", "java.lang.Number"))
  str.foreach(str => println(findConstructorByTypes(str, Seq(ClassRef("char", isArray = true)))))
  str.foreach(str => println(findMethodByTypes(str, "indexOf", Seq(ClassRef("java.lang.String", isArray = false), ClassRef("int", isArray = false)))))
}
