package com.contactview

import java.text.SimpleDateFormat
import java.util.TimeZone

import fastparse.all._
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/22/16
  * ************************************************************************/
sealed trait ParsedLog
sealed trait WithTaskId extends ParsedLog {
  def taskId : Long
}
case class UnparsedStatement(stmt : String) extends ParsedLog
case class Script(scriptName : Option[String])
case class VarField(varName : String, className : String)
case class VarFields(vars : Seq[VarField])
sealed trait ApplicationConfig
case class DetailedApplicationConfig(name : String, tpe : String, id : Long, desc : String, enabled : Boolean, max : Long, valid : Boolean, cfg : ApplicationSettings) extends ApplicationConfig
case class SimplifiedApplication(name : String, script : Option[Script]) extends ApplicationConfig
sealed trait ApplicationSettings
case object DebugAppConfig extends ApplicationSettings
case class DetailedApplicationSettings(schema : String,
                                       time : Long,
                                       recordId : Long,
                                       desc : String,
                                       name : String,
                                       tpe : String,
                                       id : Long,
                                       enabled : Boolean,
                                       sessions : Long,
                                       script : Script,
                                       defaultScript : Option[Script],
                                       vars : VarFields,
                                       defaultVars : Option[VarFields]) extends ApplicationSettings
case class SimplifiedApplicationSettings(name : String, script : Script) extends ApplicationSettings

case class DigitReceived(date : Long, callId : Long, mediaId : String, taskId : Long, digit : String) extends WithTaskId
case class ExecutingStep(date : Long, application : ApplicationConfig, taskId : Long, stepId : Long, stepClass : String, stepDesc : String) extends WithTaskId
case class PushingAction(date : Long, application : ApplicationConfig, taskId : Long, action : String, tpe : String) extends WithTaskId
case class JtapiCallContact(id : Long,
                            implId : String,
                            state : String,
                            inbound : Boolean,
                            appName : String,
                            task : Long,
                            session : Long,
                            seqNum : Long,
                            cn : Long,
                            dn : Long,
                            cgn : Long,
                            ani : String,
                            dnis : String,
                            clid : String,
                            aType : String,
                            lrd : String,
                            ocn : Long,
                            route : Route,
                            OrigProtocolCallRef : String,
                            DestProtocolCallRef : String,
                            TP : Long)
case class Route(num : String)
sealed trait Trigger
case class UnparsedTrigger(unparsedStr : String) extends Trigger

sealed trait Task {
  def task : DebugTask
}
case class TaskStarted(task : DebugTask) extends Task
case class TaskEnded(task : DebugTask) extends Task

case class ExecuteTask(date : Long,
                       taskId : Long,
                       task : Task) extends WithTaskId
case class ContextVariable(name : String, classname : String, defaultValue : String)
case class ContextVariables(variables : Seq[ContextVariable])
case class StepFailure(date : Long, application : ApplicationConfig, taskId : Long, stepId : Long, stepClass : String, stepDesc : String, exception : String) extends WithTaskId

sealed trait StepDebug extends WithTaskId {
  def task : AppDebug
  def taskId: Long = task.id
}
case class BeforeStep(task : AppDebug) extends StepDebug
case class AfterStep(task : AppDebug) extends StepDebug

sealed trait DebugTask {
  def id : Long
}
case class AppDebug(state : String,
                    application : ApplicationConfig,
                    trigger : Trigger,
                    id : Long,
                    startTime : Long,
                    sourceId : Option[String],
                    destinationId : Option[String],
                    mediaInstanceId : Option[String],
                    sessionHandled : Boolean,
                    context : Option[ContextVariables],
                    privateContext : Option[ContextVariables],
                    numberOfExecutedSteps : Int,
                    currentStepDesc : String) extends DebugTask
case class WorkflowDebug(id : Long,
                         startTime : Long,
                         sourceId : Option[String],
                         destinationId : Option[String],
                         mediaInstanceId : Option[String],
                         sessionHandled : Boolean,
                         context : Option[ContextVariables],
                         privateContext : Option[ContextVariables],
                         numberOfExecutedSteps : Int,
                         currentStepDesc : String) extends DebugTask

object LogParserTest extends App {


  val testStr =
    """95398313: Mar 01 12:10:10.645 AKST %MIVR-APP_MGR-7-EXECUTING_STEP:Executing a step: Application=App[name=CSSD Mainline IVR,type=Cisco Script Application,id=8,desc=CSSD Mainline IVR,enabled=true,max=400,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2016-02-19 11:38:37.0,recordId=518,desc=CSSD Mainline IVR,name=CSSD Mainline IVR,type=Cisco Script Application,id=8,enabled=true,sessions=400,script=SCRIPT[CSSD/CSSD.aef],defaultScript=,vars=[<java.lang.String sCSQ_Interstate>,<java.lang.String sCSQ_Enforcement>,<java.lang.String sCSQ_SCU>,<java.lang.String sCSQ_Mod>,<java.lang.String sCSQ_Establish>,<java.lang.String sCSQ_Assistant>,<com.cisco.prompt.Playable pSpecialAnnouncement>,<com.cisco.prompt.Playable pIntroductionGreeting>,<com.cisco.prompt.Playable pMainMenu>,<com.cisco.prompt.Playable pCaseInformationMenu>,<com.cisco.prompt.Playable pCollectExtension>,<java.lang.String sKidsIvrExtension>,<java.lang.String sCaseInformationVoicemailBoxNumber>,<java.lang.String sVoicemailPilot>,<java.lang.String sCaseInformationLicensingExtension>,<com.cisco.prompt.Playable pKIDSSpecialAnnouncement>,<java.lang.String sEmployerAssistanceExtension>,<com.cisco.prompt.Playable pGeneralHoursAndLocationMenu>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript1>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript2>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript3>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript4>,<com.cisco.prompt.Playable pAfterHoursMenu>,<com.cisco.prompt.Playable pQueueHoldMessage1>,<com.cisco.prompt.Playable pQueueHoldMessage2>,<com.cisco.prompt.Playable pQueueHoldMessage3>,<com.cisco.prompt.Playable pInQueueMenuNoCallback>,<com.cisco.prompt.Playable pInQueueMenuWithCallback>,<java.lang.String sSpecialAnnouncementFilename>,<java.lang.String sSCUFilename>,<java.lang.String sMODFilename>,<java.lang.String sKIDSFilename>,<java.lang.String sCallbackApplicationName>],defaultVars=null]]],Task id=75000070673,Step id=691,Step Class=com.cisco.wfframework.steps.core.StepComment,Step Description= /* Allow prompt for Callback ... */""".stripMargin

  val space = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))
  val string = P (CharsWhile(!"\"\\".contains(_: Char)))
  val stringNoComma = P (CharsWhile(!",".contains(_: Char)))
  val stringNoNewline = P (CharsWhile(!"\n".contains(_: Char)))
  val stringNoCommaNoSpace = P (CharsWhile(!", ".contains(_: Char)))
  val stringNoCommaNoRBracket = P (CharsWhile(!",]".contains(_: Char)))
  val stringNoCommaNoRCurlyBracket = P (CharsWhile(!",}".contains(_: Char)))
  val stringNoCommaNoRClose = P (CharsWhile(!",>".contains(_: Char)))
  val boolean = P ("true" | "false").!.map(_.toBoolean)

  val lineIdParser = P( (digits.! ~ ":").map(_.toLong) ~ &(" " ~ logTimeParser))
  val months = List ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
  val tzAbbr = List(
    "ACDT",    "ACST",   "ACT",    "ACT",     "ADT",    "AEDT",    "AEST",    "AFT",     "AKDT",   "AKST",   "AMST",
    "AMT",     "AMT",    "ART",    "AST",     "AST",    "AWDT",    "AWST",    "AZOST",   "AZT",    "BDT",    "BDT",
    "BIOT",    "BIT",    "BOT",    "BRST",    "BRT",    "BST",     "BST",     "BST",     "BTT",    "CAT",    "CCT",
    "CDT",     "CDT",    "CEDT",   "CEST",    "CET",    "CHADT",   "CHAST",   "CHOT",    "ChST",   "CHUT",
    "CIST",    "CIT",    "CKT",    "CLST",    "CLT",    "COST",    "COT",     "CST",     "CST",    "CST",    "CST",
    "CST",     "CT",     "CVT",    "CWST",    "CXT",    "DAVT",    "DDUT",    "DFT",     "EASST",  "EAST",   "EAT",
    "ECT",     "ECT",    "EDT",    "EEDT",    "EEST",   "EET",     "EGST",    "EGT",     "EIT",    "EST",    "EST",
    "FET",     "FJT",    "FKST",   "FKST",    "FKT",    "FNT",     "GALT",    "GAMT",    "GET",    "GFT",    "GILT",
    "GIT",     "GMT",    "GST",    "GST",     "GYT",    "HADT",    "HAEC",    "HAST",    "HKT",    "HMT",    "HOVT",
    "HST",     "IBST",   "ICT",    "IDT",     "IOT",    "IRDT",    "IRKT",    "IRST",    "IST",    "IST",    "IST",
    "JST",     "KGT",    "KOST",   "KRAT",    "KST",    "LHST",    "LHST",    "LINT",    "MAGT",   "MART",   "MAWT",
    "MDT",     "MET",    "MEST",   "MHT",     "MIST",   "MIT",     "MMT",     "MSK",     "MST",    "MST",    "MST",
    "MUT",     "MVT",    "MYT",    "NCT",     "NDT",    "NFT",     "NPT",     "NST",     "NT",     "NUT",    "NZDT",
    "NZST",    "OMST",   "ORAT",   "PDT",     "PET",    "PETT",    "PGT",     "PHOT",    "PKT",    "PMDT",   "PMST",
    "PONT",    "PST",    "PST",    "PYST",    "PYT",    "RET",     "ROTT",    "SAKT",    "SAMT",   "SAST",   "SBT",
    "SCT",     "SGT",    "SLST",   "SRET",    "SRT",    "SST",     "SST",     "SYOT",    "TAHT",   "THA",    "TFT",
    "TJT",     "TKT",    "TLT",    "TMT",     "TOT",    "TVT",     "UCT",     "ULAT",    "USZ1",   "UTC",    "UYST",
    "UYT",     "UZT",    "VET",    "VLAT",    "VOLT",   "VOST",    "VUT",     "WAKT",    "WAST",   "WAT",    "WEDT",
    "WEST",    "WET",    "WIT",    "WST",     "YAKT",   "YEKT",    "Z")

  val monthParser = StringIn(months : _ *)
  val tzParser = StringIn(tzAbbr : _ *)
  val timeParser : P[Long] = P ( digits ~ ":" ~ digits ~ ":" ~ digits ~ "." ~ digits).!.map {
    time =>
      val fmt = new java.text.SimpleDateFormat("HH:mm:ss.SSS")
      fmt.parse(time).getTime
  }

  val fmt = DateTimeFormat.forPattern("MMM dd Y HH:mm:ss.SSS Z")
  val year = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date())
  val logTimeParser : P[Long] = P ((monthParser.! ~ " " ~ digits.! ~ " " ~ timeParser.!) ~ " " ~ tzParser.!).map {
    case (month, day, time, tz) =>
      val date = fmt.withOffsetParsed().parseDateTime(s"$month $day $year $time ${tzAbbr2Offset(tz)}")
      date.getMillis
  }

  val testStr2 =
    """
      |95398313:
      |Mar 01 12:10:10.645 AKST
      |%MIVR-APP_MGR-7-EXECUTING_STEP:Executing a step:
      |Application=
      |App[name=CSSD Mainline IVR,type=Cisco Script Application,id=8,desc=CSSD Mainline IVR,enabled=true,max=400,valid=true,
      |cfg=[ApplicationConfig[
      | schema=ApplicationConfig,
      | time=2016-02-19 11:38:37.0,
      | recordId=518,
      | desc=CSSD Mainline IVR,
      | name=CSSD Mainline IVR,
      | type=Cisco Script Application,
      | id=8,
      | enabled=true,
      | sessions=400,
      | script=SCRIPT[CSSD/CSSD.aef],
      | defaultScript=,
      | vars=[<java.lang.String sCSQ_Interstate>,<java.lang.String sCSQ_Enforcement>,<java.lang.String sCSQ_SCU>,<java.lang.String sCSQ_Mod>,<java.lang.String sCSQ_Establish>,<java.lang.String sCSQ_Assistant>,<com.cisco.prompt.Playable pSpecialAnnouncement>,<com.cisco.prompt.Playable pIntroductionGreeting>,<com.cisco.prompt.Playable pMainMenu>,<com.cisco.prompt.Playable pCaseInformationMenu>,<com.cisco.prompt.Playable pCollectExtension>,<java.lang.String sKidsIvrExtension>,<java.lang.String sCaseInformationVoicemailBoxNumber>,<java.lang.String sVoicemailPilot>,<java.lang.String sCaseInformationLicensingExtension>,<com.cisco.prompt.Playable pKIDSSpecialAnnouncement>,<java.lang.String sEmployerAssistanceExtension>,<com.cisco.prompt.Playable pGeneralHoursAndLocationMenu>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript1>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript2>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript3>,<com.cisco.prompt.Playable pGeneralHoursAndLocationInfoScript4>,<com.cisco.prompt.Playable pAfterHoursMenu>,<com.cisco.prompt.Playable pQueueHoldMessage1>,<com.cisco.prompt.Playable pQueueHoldMessage2>,<com.cisco.prompt.Playable pQueueHoldMessage3>,<com.cisco.prompt.Playable pInQueueMenuNoCallback>,<com.cisco.prompt.Playable pInQueueMenuWithCallback>,<java.lang.String sSpecialAnnouncementFilename>,<java.lang.String sSCUFilename>,<java.lang.String sMODFilename>,<java.lang.String sKIDSFilename>,<java.lang.String sCallbackApplicationName>],
      | defaultVars=null]]],
      |Task id=75000070673,
      |Step id=691,
      |Step Class=com.cisco.wfframework.steps.core.StepComment,
      |Step Description= /* Allow prompt for Callback ... */""".stripMargin


  val dateParser = P(digits ~ "-" ~ digits ~ "-" ~ digits)
  def strField(name : String, sep : P[Unit] = P("="), endP : P[Unit] = P(",").?) : P[String] = P(s"$name" ~ sep ~ stringNoComma.! ~ endP)
  def longField(name : String, sep : P[Unit] = P("=")) : P[Long] = P(s"$name" ~ sep ~ (P("+" | "-").? ~ digits).! ~ ",".?).map(_.toLong)
  def boolField(name : String) : P[Boolean] = P(s"$name=" ~ boolean.! ~ ",".?).map(_.toBoolean)
  def dtField(name : String) : P[Long] = P (s"$name=" ~  (dateParser ~ " " ~ timeParser).! ~ ",".?).map {
    datetime =>
      val fmt = new java.text.SimpleDateFormat("yyyy-mm-dddd HH:mm:ss.SSS")
      fmt.parse(datetime).getTime
  }
  def scriptField(name : String) : P[Script] = P ((s"$name=SCRIPT[" ~ stringNoCommaNoRBracket.! ~ "],").map(v => Script(Some(v))) | (s"$name=,").!.map(v => Script(None)))
  def varFieldParser : P[VarFields] = P("<" ~ stringNoCommaNoSpace.! ~ " " ~ stringNoCommaNoRClose.! ~ ">" ~ ",".?).map{
    case (clazz, varName) =>
      VarField(varName, clazz)
  }.rep.map(VarFields)
  def varField(name : String) = P (s"$name=[" ~ varFieldParser ~ "]" ~ ",".?)
  def defaultVarField : P[Option[VarFields]] = P ("defaultVars=" ~ ("null".!.map(_ => None) | "[" ~ varFieldParser.map(Some(_)) ~ "]"))

  val appCfg : P[ApplicationSettings] = P(
    P(
      "ApplicationConfig"
      ~ "["
      ~ strField("schema")
      ~ dtField("time")
      ~ longField("recordId")
      ~ strField("desc")
      ~ strField("name")
      ~ strField("type")
      ~ longField("id")
      ~ boolField("enabled")
      ~ longField("sessions")
      ~ scriptField("script")
      ~ scriptField("defaultScript").?
      ~ varField("vars")
      ~ defaultVarField
      ~ "]"
    ).map(v => DetailedApplicationSettings(v._1, v._2, v._3, v._4, v._5, v._6, v._7, v._8, v._9, v._10, v._11, v._12, v._13)) |
      P(CharsWhile(!"]".contains(_)) ~ "]".?).map(_ => DebugAppConfig)
  )
  val cfgField = P("cfg=[" ~ appCfg ~ "]")


  val e0 = P("%MIVR-APP_MGR-7-EXECUTING_STEP:Executing a step: Application=App")
  val e1 = P("%MIVR-APP_MGR-7-PUSHING_ACTION:Pushing action: Application=App")
  val a0 = strField("name")
  val a1 = strField("type")
  val a2 = longField("id")
  val a3 = strField("desc")
  val a4 = boolField("enabled")
  val a5 = longField("max")
  val a6 = boolField("valid")

  val a7 = longField("Task id") // we want this one
  val a8 = longField("Step id") // we want this one
  val a9 = strField("Step Class")
  val a10 = strField("Step Description", endP = P(CharsWhile(!"\n".contains(_)).? ~ "\n"))

  val a11 = strField("Action")
  val a12 = strField("Type", endP = P(CharsWhile(!"\n".contains(_)).? ~ "\n"))


  val app : P[ApplicationConfig] = P("[" ~ a0 ~ a1 ~ a2 ~ a3 ~ a4 ~ a5 ~ a6 ~ cfgField ~ "]".? ~ ",".?).map(v => DetailedApplicationConfig(v._1, v._2, v._3, v._4, v._5, v._6, v._7, v._8))
  val executeStep = P (a7 ~ a8 ~ a9 ~ "Step Description=" ~ (!(lineIdParser | End) ~ AnyChars(1)).rep.!.map(_.trim) ~ (NoTrace(NoCut(lineIdParser)) | End))
  val pushStep = P (a7 ~ a11 ~ "Type=" ~ CharsWhile(!"\n".contains(_)).! ~ ("\n" | End))

  val detailedExecuteStepParser = P(lineIdParser ~ " " ~ logTimeParser ~ space ~ e0 ~/ app ~ executeStep).map(v => ExecutingStep(v._2, v._3, v._4._1, v._4._2, v._4._3, v._4._4))
  val detailedPushActionParser = P (lineIdParser ~ " " ~ logTimeParser ~ space ~ e1 ~/ app ~ pushStep).map(v => PushingAction(v._2, v._3, v._4._1, v._4._2, v._4._3))
  val digitReceivedParser =
    P (lineIdParser ~ " " ~ logTimeParser ~ space ~ "%MIVR-SS_TEL-7-UNK:" ~
      longField("CallID", P(":")) ~ " " ~
      "MediaId:" ~ stringNoCommaNoSpace.! ~ " " ~
      longField("Task", ":") ~ " " ~
      P("Digit received: ") ~/ CharIn("0123456789#*ABCDE").!).map(v => DigitReceived(v._2, v._3, v._4, v._5, v._6))

  val routeParser = P("RP[" ~ strField("num") ~ "]" ~ ",".?).map(Route)
  val jtapiCallContactParser = P(
    "JTAPICallContact[" ~
    longField("id") ~
    strField("implId") ~
    strField("state") ~
    boolField("inbound") ~
    strField("App name") ~
    longField("task") ~
    longField("session") ~
    longField("seq num") ~
    longField("cn") ~
    longField("dn") ~
    longField("cgn") ~
    strField("ani") ~
    strField("dnis") ~
    strField("clid") ~
    strField("atype") ~
    strField("lrd") ~
    longField("ocn") ~
    routeParser ~
    strField("OrigProtocolCallRef") ~
    strField("DestProtocolCallRef") ~
    longField("TP")
  ).map(v => JtapiCallContact(v._1, v._2, v._3, v._4, v._5, v._6, v._7, v._8, v._9, v._10, v._11, v._12, v._13, v._14, v._15, v._16, v._17, v._18, v._19, v._20, v._21))


  val executeTaskParser = P(
    (lineIdParser ~ " " ~ logTimeParser ~ space ~ "%MIVR-ENG-7-UNK:Execute Task " ~ digits.!.map(_.toLong) ~ space ~ beforeExecutionParser ~ space).map(v => ExecuteTask(v._2, v._3, v._4)) |
      (lineIdParser ~ " " ~ logTimeParser ~ space ~ "%MIVR-ENG-7-UNK:" ~ afterExecutionParser ~ space).map(v => ExecuteTask(v._2, v._3.task.id, v._3))
  )

  val appTaskWrapperParser : P[DebugTask] = P (
    ("An object of com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper\n" //com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper" ~ "\n"
    ~ "State:" ~ stringNoNewline.! ~ "\n"
    ~ "Application:" ~ " App" ~ app ~ "\n" //discarding because we dont care
    ~ "Trigger:" ~ stringNoNewline.!.map(UnparsedTrigger) ~ "\n" //discarding
    ~ space ~ "An object of com.cisco.wfframework.engine.core.WFEngineWorkflowDebugTask\n" //found so far: com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper, com.cisco.wfframework.engine.core.WFEngineWorkflowDebugTask
    ~ space ~ "ID: " ~ digits.!.map(_.toLong) ~ "\n"
    ~ space ~ "Start time (ms): " ~ digits.!.map(_.toLong) ~ "\n"
    ~ space ~ "Source ID:" ~ stringNoNewline.!.? ~ "\n"
    ~ space ~ "Destination ID:" ~ stringNoNewline.!.? ~ "\n"
    ~ space ~ "Media Instance ID:" ~ stringNoNewline.!.? ~ "\n"
    ~ space ~ "Session handled:" ~ space ~ boolean ~ "\n"
    ~ space ~ "Context:\n"
    ~ space ~ contextParser
    ~ space ~ "Private Context:\n"
    ~ space ~ contextParser
    ~ space ~ "Number of executed steps: " ~ digits.!.map(_.toInt) ~ "\n"
    ~ space ~ "Current step: " ~ stringNoNewline.! ~ "\n").map(v => AppDebug(v._1, v._2, v._3, v._4, v._5, v._6, v._7, v._8, v._9, v._10, v._11, v._12, v._13))
      |
      (
        "An object of com.cisco.wfframework.engine.core.WFEngineWorkflowDebugTask\n" //com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper" ~ "\n"
          ~ space ~ "ID: " ~ digits.!.map(_.toLong) ~ "\n"
          ~ space ~ "Start time (ms): " ~ digits.!.map(_.toLong) ~ "\n"
          ~ space ~ "Source ID:" ~ stringNoNewline.!.? ~ "\n"
          ~ space ~ "Destination ID:" ~ stringNoNewline.!.? ~ "\n"
          ~ space ~ "Media Instance ID:" ~ stringNoNewline.!.? ~ "\n"
          ~ space ~ "Session handled:" ~ space ~ boolean ~ "\n"
          ~ space ~ "Context:\n"
          ~ space ~ contextParser
          ~ space ~ "Private Context:\n"
          ~ space ~ contextParser
          ~ space ~ "Number of executed steps: " ~ digits.!.map(_.toInt) ~ "\n"
          ~ space ~ "Current step: " ~/ stringNoNewline.! ~ "\n").map(v => WorkflowDebug(v._1, v._2, v._3, v._4, v._5, v._6, v._7, v._8, v._9, v._10)
      )
  )
  val workflowSteppingDebug : P[AppDebug] = P(
    ("An object of com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper\n" //com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper" ~ "\n"
      ~ "State:" ~ stringNoNewline.! ~ "\n"
      ~ "Application:" ~ " App" ~ app ~ "\n" //discarding because we dont care
      ~ "Trigger:" ~ stringNoNewline.!.map(UnparsedTrigger) ~ "\n" //discarding
      ~ space ~ "An object of com.cisco.wfframework.engine.core.WFEngineWorkflowDebugTask\n" //found so far: com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper, com.cisco.wfframework.engine.core.WFEngineWorkflowDebugTask
      ~ space ~ "ID: " ~ digits.!.map(_.toLong) ~ "\n"
      ~ space ~ "Start time (ms): " ~ digits.!.map(_.toLong) ~ "\n"
      ~ space ~ "Source ID:" ~ stringNoNewline.!.? ~ "\n"
      ~ space ~ "Destination ID:" ~ stringNoNewline.!.? ~ "\n"
      ~ space ~ "Media Instance ID:" ~ stringNoNewline.!.? ~ "\n"
      ~ space ~ "Session handled:" ~ space ~ boolean ~ "\n"
      ~ space ~ "Context:\n"
      ~ space ~ contextParser
      ~ space ~ "Private Context:\n"
      ~ space ~ contextParser
      ~ space ~ "Number of executed steps: " ~ digits.!.map(_.toInt) ~ "\n"
      ~ space ~ "Current step: " ~ stringNoNewline.! ~ "\n").map(v => AppDebug(v._1, v._2, v._3, v._4, v._5, v._6, v._7, v._8, v._9, v._10, v._11, v._12, v._13))
  )


  val bracketsParser = P(CharsWhile(!"{}".contains(_)) | P(space ~ "{" ~ CharsWhile(!"}".contains(_)) ~ "}" ~ space)).rep.!.map(_.trim)

  val contextValuesParser : P[ContextVariable] = P (
    space ~ "{" ~ stringNoComma.! ~ "," ~ stringNoComma.! ~ "," ~/ bracketsParser ~ "}\n"
  ).map(v => ContextVariable(v._1, v._2, v._3))

  val contextParser : P[Option[ContextVariables]] = P(
    space ~ "An object of com.cisco.wfframework.obj.WFWorkflowContext" ~ space ~ "{" ~ contextValuesParser.rep ~ space ~ "}"
  ).map {
    case Nil => None
    case vars => Some(ContextVariables(vars))
  }



  val beforeExecutionParser = P(">>>>> Before execution" ~/ space ~ appTaskWrapperParser ~ "<<<<<").map(TaskStarted)
  val afterExecutionParser = P(">>>>> After execution" ~/ space ~ appTaskWrapperParser ~ "<<<<<").map(TaskEnded)

  val beforeStepParser : P[StepDebug] = P(">>>>> Before stepping" ~/ space ~ workflowSteppingDebug ~ "<<<<<").map(BeforeStep)
  val afterStepParser : P[StepDebug] = P(">>>>> After stepping" ~/ space ~ workflowSteppingDebug ~ "<<<<<").map(AfterStep)

  val executeStepStr =
    """
      |95386551: Mar 01 12:09:51.456 AKST %MIVR-APP_MGR-7-EXECUTING_STEP:Executing a step: Application=App[name=DPA_0001-6529,type=Cisco Script Application,id=4008,desc=DPA_0001-6529269.0001-6529_Clerical Queue,enabled=true,max=40,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-12-02 10:31:25.0,recordId=303,desc=DPA_0001-6529269.0001-6529_Clerical Queue,name=DPA_0001-6529,type=Cisco Script Application,id=4008,enabled=true,sessions=40,script=SCRIPT[DPA_2690001-6599_v1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable MusicPrompt>],defaultVars=null]]],Task id=75000071038,Step id=193,Step Class=com.cisco.wfframework.steps.core.StepAssign,Step Description=Set todaysDateString = monthString + "/" +  dayString  + "/" + yearString
    """.stripMargin

  val digitStr = "95386611: Mar 01 12:09:51.761 AKST %MIVR-SS_TEL-7-UNK:CallID:643 MediaId:16552771/3 Task:75000071025 Digit received: #"

  val subflowExample =
    """
      |95386526: Mar 01 12:09:51.454 AKST %MIVR-APP_MGR-7-EXECUTING_STEP:Executing a step: Application=App[name=DPA_0001-6529,type=Cisco Script Application,id=4008,desc=DPA_0001-6529269.0001-6529_Clerical Queue,enabled=true,max=40,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-12-02 10:31:25.0,recordId=303,desc=DPA_0001-6529269.0001-6529_Clerical Queue,name=DPA_0001-6529,type=Cisco Script Application,id=4008,enabled=true,sessions=40,script=SCRIPT[DPA_2690001-6599_v1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable MusicPrompt>],defaultVars=null]]],Task id=75000071038,Step id=426,Step Class=com.cisco.wfframework.steps.core.StepCallSubflow,Step Description=todayIsAHoliday = Call Subflow(HolidayCheck, todayIsAHoliday)
      |95386527: Mar 01 12:09:51.454 AKST %MIVR-ENG-7-UNK:Execute step of Task 75000071038 : todayIsAHoliday = Call Subflow(HolidayCheck, todayIsAHoliday)
      |95386528: Mar 01 12:09:51.454 AKST %MIVR-APP_MGR-7-PUSHING_ACTION:Pushing action: Application=App[name=DPA_0001-6529,type=Cisco Script Application,id=4008,desc=DPA_0001-6529269.0001-6529_Clerical Queue,enabled=true,max=40,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-12-02 10:31:25.0,recordId=303,desc=DPA_0001-6529269.0001-6529_Clerical Queue,name=DPA_0001-6529,type=Cisco Script Application,id=4008,enabled=true,sessions=40,script=SCRIPT[DPA_2690001-6599_v1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable MusicPrompt>],defaultVars=null]]],Task id=75000071038,Action=HolidayCheck.aef,Type=Interruptible subflow
      |95386529: Mar 01 12:09:51.454 AKST %MIVR-APP_MGR-7-EXECUTING_STEP:Executing a step: Application=App[name=DPA_0001-6529,type=Cisco Script Application,id=4008,desc=DPA_0001-6529269.0001-6529_Clerical Queue,enabled=true,max=40,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-12-02 10:31:25.0,recordId=303,desc=DPA_0001-6529269.0001-6529_Clerical Queue,name=DPA_0001-6529,type=Cisco Script Application,id=4008,enabled=true,sessions=40,script=SCRIPT[DPA_2690001-6599_v1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable MusicPrompt>],defaultVars=null]]],Task id=75000071038,Step id=0,Step Class=com.cisco.wfframework.steps.core.StepStart,Step Description=Start
    """.stripMargin

  /* task aborted:
95347217: Mar 01 12:07:17.040 AKST %MIVR-APP_MGR-7-TASK_ABORTED:Application task aborted: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]]
,Application Trigger=ContactApplicationTrigger[time=1456866392575,locale=en_US,cfg=JTAPITriggerConfig[schema=ApplicationTriggerConfig,time=2015-11-23 09:52:03.0,recordId=475,desc=Cisco JTAPI Trigger,name=1008683,type=Cisco JTAPI Trigger,appName=Elections IVR,enabled=true,sessions=60,idleTimeout=5000,locale=en_US,parms={},taskGroups=[],controlClass=class com.cisco.call.CallControlChannel,controlGroupId=7,contactGroups=[GroupInfo[class=com.cisco.dialog.DialogChannel,id=0]],dn=1008683,redirectCSS=default,cmDeviceName=RP_1008683,cmDeviceInvalid=false,cmDescription=Elections IVR,cmDevicePoolUUID={2A8C1867-AD6A-4774-89E8-2B02B9C26F9F},cmDevicePoolName=ANC_CTI_GRP1_DP,cmCallingSearchSpaceUUID={1072ea52-ae2a-d5ae-3924-9f47065feaa3},cmCallingSearchSpaceName=ANC_CTI_CSS,cmLocationUUID={E26F2163-ED2A-9C1F-26AA-BD985ECF8053},cmLocationName=Hub_Anchorage,cmPartitionUUID={85209484-92DD-4335-8818-878CFDF0FC2A},cmPartitionName=ANC_Internal_PT,cmVoiceMailProfileUUID=,cmVoiceMailProfileName=None,cmCallPickUpGroupUUID=,cmCallPickUpGroupName=,cmDisplay=,cmExternalPhNumMask=,cmFwdBusyVM=false,cmFwdBusyDest=,cmFwdBusyCSSUUID=,cmFwdBusyCSSName=None,cmAlertingNameAscii=,cmPresenceGroupUUID=ad243d17-98b4-4118-8feb-5ff2e1b781ac,cmPresenceGroupName=Standard Presence group,campaignID=-1],contact=JTAPICallContact[id=592,implId=16552665/3,state=STATE_ABANDONED_IDX,inbound=true,App name=Elections IVR,task=75000070970,session=86000067173,seq num=0,cn=1008683,dn=1008683,cgn=9076882667,ani=null,dnis=null,clid=null,atype=DIRECT,lrd=null,ocn=1008683,route=RP[num=1008683],OrigProtocolCallRef=0000000000FC92D9033366E300000000,DestProtocolCallRef=null,TP=1009296]]
,Task id=75000070970,Task Class=class com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper,Exception=com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely
95347218: Mar 01 12:07:17.040 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely

 */

  /*
  session idle:

  95347234: Mar 01 12:07:17.041 AKST %MIVR-APP_MGR-7-APP_SESSION_IDLE:Idle application session: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]]
  ,Application Trigger=ContactApplicationTrigger[time=1456866392575,locale=en_US,cfg=JTAPITriggerConfig[schema=ApplicationTriggerConfig,time=2015-11-23 09:52:03.0,recordId=475,desc=Cisco JTAPI Trigger,name=1008683,type=Cisco JTAPI Trigger,appName=Elections IVR,enabled=true,sessions=60,idleTimeout=5000,locale=en_US,parms={},taskGroups=[],controlClass=class com.cisco.call.CallControlChannel,controlGroupId=7,contactGroups=[GroupInfo[class=com.cisco.dialog.DialogChannel,id=0]],dn=1008683,redirectCSS=default,cmDeviceName=RP_1008683,cmDeviceInvalid=false,cmDescription=Elections IVR,cmDevicePoolUUID={2A8C1867-AD6A-4774-89E8-2B02B9C26F9F},cmDevicePoolName=ANC_CTI_GRP1_DP,cmCallingSearchSpaceUUID={1072ea52-ae2a-d5ae-3924-9f47065feaa3},cmCallingSearchSpaceName=ANC_CTI_CSS,cmLocationUUID={E26F2163-ED2A-9C1F-26AA-BD985ECF8053},cmLocationName=Hub_Anchorage,cmPartitionUUID={85209484-92DD-4335-8818-878CFDF0FC2A},cmPartitionName=ANC_Internal_PT,cmVoiceMailProfileUUID=,cmVoiceMailProfileName=None,cmCallPickUpGroupUUID=,cmCallPickUpGroupName=,cmDisplay=,cmExternalPhNumMask=,cmFwdBusyVM=false,cmFwdBusyDest=,cmFwdBusyCSSUUID=,cmFwdBusyCSSName=None,cmAlertingNameAscii=,cmPresenceGroupUUID=ad243d17-98b4-4118-8feb-5ff2e1b781ac,cmPresenceGroupName=Standard Presence group,campaignID=-1],contact=JTAPICallContact[id=592,implId=16552665/3,state=STATE_ABANDONED_IDX,inbound=true,App name=Elections IVR,task=75000070970,session=86000067173,seq num=0,cn=1008683,dn=1008683,cgn=9076882667,ani=null,dnis=null,clid=null,atype=DIRECT,lrd=null,ocn=1008683,route=RP[num=1008683],OrigProtocolCallRef=0000000000FC92D9033366E300000000,DestProtocolCallRef=null,TP=1009296]]
  ,Task id=75000070970,Active Sessions=4,Max Sessions=60

   */

  //com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely
  def readUntil(until : P[Unit]) = P((!(until) ~ AnyChars(1)).rep.!)
  val parseException = P(readUntil(":") ~ ":"  ~ space ~ longField("Contact id", sep = ": ") ~ readUntil(P("\n" | End)))
  println (parseException.parse("com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely"))
  case class TaskAborted(lineId : Long, logTime : Long, appConfig : ApplicationConfig, appTrigger : String, taskId : Long, taskClass : String, exception : String, contactId : Option[Long], exceptionMessage : Option[String]) extends WithTaskId
  val taskAborted = P(
    lineIdParser ~ " "
    ~ logTimeParser ~ " "
    ~ "%MIVR-APP_MGR-7-TASK_ABORTED:Application task aborted: Application=App" ~ app
    ~ "Application Trigger=" ~ (!("Task id=") ~ AnyChars(1)).rep.!
    ~ ",".? ~ longField("Task id") ~ strField("Task Class") ~ "Exception=" ~ P(parseException | (CharsWhile(!"\n".contains(_)).! ~ P("\n" | End)))
  ).map { v =>
    v._7 match {
      case (contact : String, id : Long, message : String) => TaskAborted(v._1, v._2, v._3, v._4, v._5, v._6, contact, Some(id), Some(message.trim))
      case str : String => TaskAborted(v._1, v._2, v._3, v._4, v._5, v._6, str, None, None)
    }
  }

  /*
95347180: Mar 01 12:07:17.038 AKST %MIVR-APP_MGR-7-INTERRUPTING_TASK:Application task is interrupting: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]],Task id=75000070970,Interruption=com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely
95347182: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely

95347200: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-UNK:Clearing interruption for Task id=75000070970: Exception=com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely
95347201: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely

 */

  case class TaskInterrupting(lineId : Long, logTime : Long, appConfig : ApplicationConfig, taskId : Long, exception : String, contactId : Option[Long], exceptionMessage : Option[String]) extends WithTaskId
  val taskInterrupting = P(
    lineIdParser ~ " "
      ~ logTimeParser ~ " "
      ~ "%MIVR-APP_MGR-7-INTERRUPTING_TASK:Application task is interrupting: Application=App" ~ app
      ~ ",".? ~ longField("Task id") ~ "Interruption=" ~ P(parseException | (CharsWhile(!"\n".contains(_)).! ~ P("\n" | End)))
  ).map { v =>
    v._5 match {
      case (contact : String, id : Long, message : String) => TaskInterrupting(v._1, v._2, v._3, v._4, contact, Some(id), Some(message.trim))
      case str : String => TaskInterrupting(v._1, v._2, v._3, v._4, str, None, None)
    }
  }

  case class AppException(lineId : Long, logTime : Long, exception : String, contactId : Option[Long], exceptionMessage : Option[String]) extends ParsedLog
  val taskException = P(
    lineIdParser ~ " "
      ~ logTimeParser ~ " "
      ~ "%MIVR-APP_MGR-7-EXCEPTION:" ~ P(parseException | (CharsWhile(!"\n".contains(_)).! ~ P("\n" | End)))
  ).map { v =>
    v._3 match {
      case (contact : String, id : Long, message : String) => AppException(v._1, v._2, contact, Some(id), Some(message.trim))
      case str : String => AppException(v._1, v._2, str, None, None)
    }
  }

  println(taskInterrupting.parse("95347180: Mar 01 12:07:17.038 AKST %MIVR-APP_MGR-7-INTERRUPTING_TASK:Application task is interrupting: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]],Task id=75000070970,Interruption=com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely"))
  println(taskException.parse("95347201: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely"))


  val taskABortedStr = """95347217: Mar 01 12:07:17.040 AKST %MIVR-APP_MGR-7-TASK_ABORTED:Application task aborted: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]],Application Trigger=ContactApplicationTrigger[time=1456866392575,locale=en_US,cfg=JTAPITriggerConfig[schema=ApplicationTriggerConfig,time=2015-11-23 09:52:03.0,recordId=475,desc=Cisco JTAPI Trigger,name=1008683,type=Cisco JTAPI Trigger,appName=Elections IVR,enabled=true,sessions=60,idleTimeout=5000,locale=en_US,parms={},taskGroups=[],controlClass=class com.cisco.call.CallControlChannel,controlGroupId=7,contactGroups=[GroupInfo[class=com.cisco.dialog.DialogChannel,id=0]],dn=1008683,redirectCSS=default,cmDeviceName=RP_1008683,cmDeviceInvalid=false,cmDescription=Elections IVR,cmDevicePoolUUID={2A8C1867-AD6A-4774-89E8-2B02B9C26F9F},cmDevicePoolName=ANC_CTI_GRP1_DP,cmCallingSearchSpaceUUID={1072ea52-ae2a-d5ae-3924-9f47065feaa3},cmCallingSearchSpaceName=ANC_CTI_CSS,cmLocationUUID={E26F2163-ED2A-9C1F-26AA-BD985ECF8053},cmLocationName=Hub_Anchorage,cmPartitionUUID={85209484-92DD-4335-8818-878CFDF0FC2A},cmPartitionName=ANC_Internal_PT,cmVoiceMailProfileUUID=,cmVoiceMailProfileName=None,cmCallPickUpGroupUUID=,cmCallPickUpGroupName=,cmDisplay=,cmExternalPhNumMask=,cmFwdBusyVM=false,cmFwdBusyDest=,cmFwdBusyCSSUUID=,cmFwdBusyCSSName=None,cmAlertingNameAscii=,cmPresenceGroupUUID=ad243d17-98b4-4118-8feb-5ff2e1b781ac,cmPresenceGroupName=Standard Presence group,campaignID=-1],contact=JTAPICallContact[id=592,implId=16552665/3,state=STATE_ABANDONED_IDX,inbound=true,App name=Elections IVR,task=75000070970,session=86000067173,seq num=0,cn=1008683,dn=1008683,cgn=9076882667,ani=null,dnis=null,clid=null,atype=DIRECT,lrd=null,ocn=1008683,route=RP[num=1008683],OrigProtocolCallRef=0000000000FC92D9033366E300000000,DestProtocolCallRef=null,TP=1009296]],Task id=75000070970,Task Class=class com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper,Exception=com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely"""

  println (taskAborted.parse(taskABortedStr))

  val stepFailure = P(
    lineIdParser ~ " "
      ~ logTimeParser ~ " "
      ~ "%MIVR-APP_MGR-7-STEP_FAILURE:Failure to execute a step: Application=App" ~ app
      ~ longField("Task id")
      ~ longField("Step id")
      ~ strField("Step Class")
      ~ "Step Description=" ~ (!(",Exception=") ~ AnyChars(1)).rep.!
      ~ ",Exception=" ~ CharsWhile(!"\n".contains(_)).! ~ P("\n" | End)
  ).map(v => StepFailure(v._2, v._3, v._4, v._5, v._6, v._7, v._8.split(':').headOption.getOrElse(v._8)))
  println (stepFailure.parse("95398007: Mar 01 12:10:09.677 AKST %MIVR-APP_MGR-7-STEP_FAILURE:Failure to execute a step: Application=App[name=DPA_0001-6529,type=Cisco Script Application,id=4008,desc=DPA_0001-6529269.0001-6529_Clerical Queue,enabled=true,max=40,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-12-02 10:31:25.0,recordId=303,desc=DPA_0001-6529269.0001-6529_Clerical Queue,name=DPA_0001-6529,type=Cisco Script Application,id=4008,enabled=true,sessions=40,script=SCRIPT[DPA_2690001-6599_v1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable MusicPrompt>],defaultVars=null]]],Task id=75000070910,Step id=476,Step Class=com.cisco.wf.steps.ivr.OutputStep,Step Description=Play Prompt (--Triggering Contact--, MusicPrompt),Exception=com.cisco.contact.ContactInactiveException: Contact id: 530, Channel id: 89, Contact is in Terminated/Connected state"))
  val steppingProcessor : P[StepDebug] = P(
    (lineIdParser ~ " " ~ logTimeParser ~ space ~ "%MIVR-ENG-7-UNK:" ~ (beforeStepParser | afterStepParser) ~ space).map(_._3)
  )

  val logParser : P[ParsedLog] = P(
    executeTaskParser |
      taskAborted | taskInterrupting |
    steppingProcessor |
    stepFailure |
    detailedExecuteStepParser |
    detailedPushActionParser |
    digitReceivedParser
  )
  val ignoredParser = P (lineIdParser.? ~ CharsWhile(!"\n".contains(_)).!.? ~ "\n" ~ space).map(v => UnparsedStatement(v._2.getOrElse("")))

  val parser : P[Seq[ParsedLog]] = P((logParser | ignoredParser).rep ~ End)
  println ("parsing streams\n\n")
  val stream = {
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR063.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR064.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR065.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR066.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR067.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR068.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR069.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR070.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR071.log")) ++
    scala.io.Source.fromInputStream(getClass().getClassLoader.getResourceAsStream("./Cisco001MIVR072.log"))
  }.mkString
  parser.parse(stream) match {
    case Parsed.Success(logs, index) =>
      def isComplete(seq : Seq[ParsedLog]) : Boolean = {
        (seq.headOption, seq.lastOption) match {
          case (Some(ExecuteTask(_, _, startTask : TaskStarted)), Some(ExecuteTask(_, _, endTask : TaskEnded))) => true
          case _ => false
        }
      }
      val parsedLogs =
        logs
          .collect { case  withTask : WithTaskId => withTask }
          .groupBy(_.taskId)
//          .filter(v => isComplete(v._2))
          .filter(_._2.exists {
            case e :ExecutingStep => true
            case _ => false
          })
      println (s"num of tasks = ${parsedLogs.keys.size}")

      parsedLogs.map(v => v._1 -> isComplete(v._2)).foreach {
        case (taskId, isComplete) => println (s"taskId=$taskId isComplete=$isComplete")
      }

      val df = new SimpleDateFormat("HH:mm:ss.SSS")


      parsedLogs.foreach {
        case (taskId, logs) =>
//          logs.foreach(println)
          def log2str(log : ParsedLog) : String = log match {
            case withId : WithTaskId =>
              withId match {
                case TaskAborted(lineId, logTime, appConfig, appTrigger, taskId, taskClass, exception, contactId, exceptionMessage) =>
                  exception match {
                    case "com.cisco.app.impl.ContactInactiveInterruption" => s"${df.format(new java.util.Date(logTime))}: Caller abandoned."
                    case _ =>
                      exceptionMessage match {
                        case Some(msg) => s"${df.format(new java.util.Date(logTime))}: Task $taskId aborted: $msg"
                        case None => s"${df.format(new java.util.Date(logTime))}: Task $taskId aborted: $exception"
                      }
                  }
                case TaskInterrupting(lineId, logTime, appConfig, taskId, exception, contactId, exceptionMessage) =>
                  exception match {
                    case "com.cisco.app.impl.ContactInactiveInterruption" => s"${df.format(new java.util.Date(logTime))}: Caller abandoned."
                    case _ =>
                      exceptionMessage match {
                        case Some(msg) => s"${df.format(new java.util.Date(logTime))}: Task $taskId interrupted: $msg"
                        case None => s"${df.format(new java.util.Date(logTime))}: Task $taskId interrupted: $exception"
                      }
                  }
                case DigitReceived(date, callId, mediaId, taskId, digit) => s"${df.format(new java.util.Date(date))}: Received Digit $digit"
                case ExecutingStep(date, application, taskId, stepId, stepClass, stepDesc) =>
                  val desc = stepDesc.split('\n').headOption.map(_ + "...").getOrElse(stepDesc)
                  s"${df.format(new java.util.Date(date))}: Executing step ${stepId}: ${desc}"
                case PushingAction(date, application, taskId, action, tpe) => s""
                case ExecuteTask(date, taskId, task) =>
                  //todo: copy private context to simmplified application for matching in contactview
                  task match {
                    case task : TaskStarted =>
                      task.task match {
                        case AppDebug(state, application : DetailedApplicationConfig, trigger, id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled, context, privateContext, numberOfExecutedSteps, currentStepDesc) =>
                          s"${df.format(new java.util.Date(date))}: Executing Application ${application.name}"
                        case WorkflowDebug(id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled, context, privateContext, numberOfExecutedSteps, currentStepDesc) =>
                          s"${df.format(new java.util.Date(date))}: Debugging Workflow with taskId ${taskId}"
                      }
                    case task : TaskEnded =>
                      task.task match {
                        case AppDebug(state, application : DetailedApplicationConfig, trigger, id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled, context, privateContext, numberOfExecutedSteps, currentStepDesc) =>
                          s"${df.format(new java.util.Date(date))}: Finished Executing Application ${application.name}"
                        case WorkflowDebug(id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled, context, privateContext, numberOfExecutedSteps, currentStepDesc) =>
                          s"${df.format(new java.util.Date(date))}: Finished Debugging Workflow with taskId ${taskId}"
                      }
                  }
                case StepFailure(date, application, taskId, stepId, stepClass, stepDesc, exception) =>
                  s"${df.format(new java.util.Date(date))}: ${stepDesc.split('\n').headOption.map(_ + "...").getOrElse(stepDesc)} threw exception $exception"

              }
            case AppException(lineId, logTime, exception, contactId, Some(exceptionMessage)) => s"Application encountered the error: $exception"
            case AppException(lineId, logTime, exception, contactId, exceptionMessage) => s"Application threw the exception $exception"
            case UnparsedStatement(stmt) => s""
          }
          println (s"TaskId: $taskId")
          logs.map("\t" + log2str(_)).filterNot(_ == "\t").foreach(println)
          val digitUsage = processSteps(logs)
          digitUsage.foreach {
            case (step, digits) =>
              println (s"\tstep=${step.stepClass}")
              digits.foreach {
                digit =>
                  println (s"\t\tdigit=${digit.digit}")
              }
          }
      }
      val stepIdMap : Map[Long, String] =
        logs.collect { case e : ExecutingStep if e.stepClass == "com.cisco.wf.steps.ivr.MenuStep" => e.stepId -> e.stepDesc }.toMap

      val report = processMenuSteps(parsedLogs.flatMap(_._2).toSeq)
      report.foreach {
        case (stepId, digits) =>
          println (s"step=${stepIdMap(stepId)}")
          digits.foreach {
            case (key, count) => println (s"\tkey '$key' was pressed $count time(s)")
          }
      }
      sealed trait SimplifiedLogs

      def appcfg(app : ApplicationSettings) : ApplicationSettings = app match {
        case d@DebugAppConfig =>d
        case DetailedApplicationSettings(schema, time, recordId, desc, name, tpe, id, enabled, sessions, script, defaultScript, vars, defaultVars) => SimplifiedApplicationSettings(name, script)
        case s@SimplifiedApplicationSettings(name, script) =>s
      }
      def applicationcfg(application : ApplicationConfig) : ApplicationConfig = application match {
        case DetailedApplicationConfig(name, tpe, id, desc, enabled, max, valid, cfg) =>
          val script = {
            cfg match {
              case d@DebugAppConfig => None
              case DetailedApplicationSettings(schema, time, recordId, desc, name, tpe, id, enabled, sessions, script, defaultScript, vars, defaultVars) => Some(script)
              case d@SimplifiedApplicationSettings(name, script) => Some(script)
            }
          }
          SimplifiedApplication(name, script)
        case d@SimplifiedApplication(name, script) => d
      }

      val filteredLogs : Seq[ParsedLog] = logs.filter {
        case e : UnparsedStatement => false
        case _ => true
      }.map {
        case wid : WithTaskId =>
          wid match {
            case TaskAborted(lineId, logTime, appConfig, appTrigger, taskId, taskClass, exception, contactId, exceptionMessage) =>
              TaskAborted(lineId, logTime, applicationcfg(appConfig), "", taskId, taskClass, exception, contactId, exceptionMessage)
            case TaskInterrupting(lineId, logTime, appConfig, taskId, exception, contactId, exceptionMessage) =>
              TaskInterrupting(lineId, logTime, applicationcfg(appConfig), taskId, exception, contactId, exceptionMessage)
            case d@DigitReceived(date, callId, mediaId, taskId, digit) => d
            case ExecutingStep(date, application, taskId, stepId, stepClass, stepDesc) => ExecutingStep(date, applicationcfg(application), taskId, stepId, stepClass, stepDesc)
            case PushingAction(date, application, taskId, action, tpe) => PushingAction(date, applicationcfg(application), taskId, action, tpe)
            case d@ExecuteTask(date, taskId, task) => d
            case StepFailure(date, application, taskId, stepId, stepClass, stepDesc, exception) =>
              StepFailure(date, applicationcfg(application), taskId, stepId, stepClass, stepDesc, exception)
            case debug : StepDebug => debug match {
              case BeforeStep(task@com.contactview.AppDebug(state, application, trigger, id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled, context, privateContext, numberOfExecutedSteps, currentStepDesc)) =>
                BeforeStep(
                  AppDebug(
                    state,
                    applicationcfg(application),
                    UnparsedTrigger(""),
                    id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled,
                    context, privateContext, numberOfExecutedSteps, currentStepDesc
                  )
                )
              case AfterStep(task@com.contactview.AppDebug(state, application, trigger, id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled, context, privateContext, numberOfExecutedSteps, currentStepDesc)) =>
                AfterStep(
                  AppDebug(
                    state,
                    applicationcfg(application),
                    UnparsedTrigger(""),
                    id, startTime, sourceId, destinationId, mediaInstanceId, sessionHandled,
                    context, privateContext, numberOfExecutedSteps, currentStepDesc
                  )
                )
            }
          }
        case appException : AppException => appException
      }
      val pickled = upickle.default.write[Seq[ParsedLog]](filteredLogs)
      println (s"pickled size=${pickled.length}")
      filteredLogs.foreach(println)
    case f : Parsed.Failure =>
      println (s"failed parsing log: $f")
  }

  val beforestr =
    """2103598: Jan 06 17:46:18.056 MST %MIVR-ENG-7-UNK:>>>>> Before stepping
      |An object of com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper
      |State: STARTED
      |Application: App[name=[MEDT:10.1.20.207]/opt/cisco/uccx/Script1.aef$$DEBUG$$10.1.20.207$$25000000172$$,type=Cisco Debugged Script Application,id=-1,desc=Debug session of the [MEDT:10.1.20.207]/opt/cisco/uccx/Script1.aef script,enabled=true,max=1,valid=true,cfg=[com.cisco.app.impl.ApplicationCfgImpl@327d4f]]
      |Trigger: RemoteApplicationTrigger[time=1483749974828,locale=en_US,cfg=null,host=10.1.20.207,parameters={}]
      |   An object of com.cisco.wfframework.engine.core.WFEngineWorkflowDebugTask
      |   ID: 25000000172
      |   Start time (ms): 1483749974827
      |   Source ID:
      |   Destination ID:
      |   Media Instance ID: null
      |   Session handled: false
      |   Context:
      |   An object of com.cisco.wfframework.obj.WFWorkflowContext
      |   {
      |   }
      |   Private Context:
      |   An object of com.cisco.wfframework.obj.WFWorkflowContext
      |   {
      |   }
      |   Number of executed steps: 2
      |   Current step:  /* Add Comment */
      |<<<<<
    """.stripMargin
  println (steppingProcessor.parse(beforestr))

  def processSteps(steps : Seq[ParsedLog]) : Map[ExecutingStep, Seq[DigitReceived]] = {
    // return step -> seq[digits]
    val consumers =
      Seq(
        "com.cisco.wf.steps.ivr.ParseInputStep",
        "com.cisco.wf.steps.ivr.ExtendedParseInputStep",
        "com.cisco.wf.steps.ivr.ExplicitConfirmStep",
        "com.cisco.wf.steps.ivr.MenuStep",
        "com.cisco.wf.steps.ivr.StepRecording",
        "com.cisco.wf.steps.ivr.ExtendedOutputStep"
      )
    def isConsumer(str : String) = consumers.contains(str)
    // filter out only relevant types
    val _steps = steps.collect {
      case e : ExecutingStep => e
      case d : DigitReceived => d
    }

    def consume(isConsuming : Option[ExecutingStep], tail : Seq[ParsedLog], buffer : Seq[DigitReceived], result : Map[ExecutingStep, Seq[DigitReceived]]) : Map[ExecutingStep, Seq[DigitReceived]] = {
      def updateResult(step : ExecutingStep, toAdd : Seq[DigitReceived]) : Map[ExecutingStep, Seq[DigitReceived]] = {
        result.get(step) match {
          case Some(digits) => result ++ Map(step -> (digits ++ buffer ++ toAdd))
          case None => result ++ Map(step -> (buffer ++ toAdd))
        }
      }

      tail.headOption match {
        case Some(step : ExecutingStep) if !isConsumer(step.stepClass) => consume(isConsuming = None, tail.tail, buffer, result)
        case Some(step : ExecutingStep) if isConsumer(step.stepClass) && isConsuming.isDefined =>
          consume(isConsuming = Some(step), tail = tail.tail, buffer = Seq(), updateResult(step, buffer))
        case Some(step : ExecutingStep) if isConsumer(step.stepClass) && isConsuming.isEmpty =>
          consume(isConsuming = Some(step), tail = tail.tail, buffer = Seq(), updateResult(step, buffer))
        case Some(digit : DigitReceived) if isConsuming.isDefined =>
          consume(isConsuming = isConsuming, tail = tail.tail, buffer = Seq(), updateResult(isConsuming.get, buffer ++ Seq(digit)))
        case Some(digit : DigitReceived) if isConsuming.isEmpty =>
          consume(isConsuming = isConsuming, tail = tail.tail, buffer = buffer ++ Seq(digit), result = result)
        case None => result
      }
    }

    consume(None, _steps, Seq(), Map())
  }

  def processMenuSteps(steps : Seq[ParsedLog]) : Map[Long, Map[String, Int]] = {
    def isConsumer(str : String) = str == "com.cisco.wf.steps.ivr.MenuStep"
    val filteredSteps = steps.collect {
      case e : ExecutingStep => e
      case d : DigitReceived => d
    }
    def consume(isConsuming : Option[Long], tail : Seq[ParsedLog], buffer : Seq[String], result : Map[Long, Seq[String]]) : Map[Long, Map[String, Int]] = {
      def updateResult(id : Long, toAdd : Seq[String]) : Map[Long, Seq[String]] = {
        result.get(id) match {
          case Some(digits) => result ++ Map(id -> (digits ++ buffer ++ toAdd))
          case None => result ++ Map(id -> (buffer ++ toAdd))
        }
      }

      tail.headOption match {
        case Some(step : ExecutingStep) if !isConsumer(step.stepClass) => consume(isConsuming = None, tail.tail, buffer, result)
        case Some(step : ExecutingStep) if isConsumer(step.stepClass) && isConsuming.isDefined =>
          consume(isConsuming = Some(step.stepId), tail = tail.tail, buffer = Seq(), updateResult(step.stepId, buffer))
        case Some(step : ExecutingStep) if isConsumer(step.stepClass) && isConsuming.isEmpty =>
          consume(isConsuming = Some(step.stepId), tail = tail.tail, buffer = Seq(), updateResult(step.stepId, buffer))
        case Some(digit : DigitReceived) if isConsuming.isDefined =>
          consume(isConsuming = isConsuming, tail = tail.tail, buffer = Seq(), updateResult(isConsuming.get, buffer ++ Seq(digit.digit)))
        case Some(digit : DigitReceived) if isConsuming.isEmpty =>
          consume(isConsuming = isConsuming, tail = tail.tail, buffer = buffer ++ Seq(digit.digit), result = result)
        case None =>
          result.map(v => v._1 -> v._2.groupBy(identity).map(v => v._1 -> v._2.size))
      }
    }
    consume(None, filteredSteps, Seq(), Map())
  }

  /*
    consumer step -> digits
    digits -> consumer step -> digits

    consumers:
    menu
    get digit string
    explicit confirmation
    record message
   */

  //todo: get timezone from UCCX and use that for the offset, abbreviations are not reliable
  def tzAbbr2Offset(tz : String) = tz match {
    case "ACDT" => "+1030"
    case "ACST" => "+0930"
    case "ACT" => "-0500"
//    case "ACT" => "+0630"
    case "ADT" => "-0300"
    case "AEDT" => "+1100"
    case "AEST" => "+1000"
    case "AFT" => "+0430"
    case "AKDT" => "-0800"
    case "AKST" => "-0900"
    case "AMST" => "-0300"
    case "AMT" => "-0400"
    case "AMT" => "+0400"
    case "ART" => "-0300"
    case "AST" => "+0300"
    case "AST" => "-0400"
    case "AWST" => "+0800"
    case "AZOST" => "0000"
    case "AZOT" => "-0100"
    case "AZT" => "+0400"
    case "BDT" => "+0800"
    case "BIOT" => "+0600"
    case "BIT" => "-1200"
    case "BOT" => "-0400"
    case "BRST" => "-0200"
    case "BRT" => "-0300"
    case "BST" => "+0600"
    case "BST" => "+1100"
    case "BST" => "+0100"
    case "BTT" => "+0600"
    case "CAT" => "+0200"
    case "CCT" => "+0630"
    case "CDT" => "-0500"
    case "CDT" => "-0400"
    case "CEST" => "+0200"
    case "CET" => "+0100"
    case "CHADT" => "+1345"
    case "CHAST" => "+1245"
    case "CHOT" => "+0800"
    case "CHOST" => "+0900"
    case "CHST" => "+1000"
    case "CHUT" => "+1000"
    case "CIST" => "-0800"
    case "CIT" => "+0800"
    case "CKT" => "-1000"
    case "CLST" => "-0300"
    case "CLT" => "-0400"
    case "COST" => "-0400"
    case "COT" => "-0500"
    case "CST" => "-0600"
    case "CST" => "+0800"
    case "ACST" => "+0930"
    case "ACDT" => "+1030"
    case "CST" => "-0500"
    case "CT" => "+0800"
    case "CVT" => "-0100"
    case "CWST" => "+0845"
    case "CXT" => "+0700"
    case "DAVT" => "+0700"
    case "DDUT" => "+1000"
    case "DFT" => "+0100"
    case "EASST" => "-0500"
    case "EAST" => "-0600"
    case "EAT" => "+0300"
    case "ECT" => "-0400"
    case "ECT" => "-0500"
    case "EDT" => "-0400"
    case "AEDT" => "+1100"
    case "EEST" => "+0300"
    case "EET" => "+0200"
    case "EGST" => "0000"
    case "EGT" => "-0100"
    case "EIT" => "+0900"
    case "EST" => "-0500"
    case "AEST" => "+1000"
    case "FET" => "+0300"
    case "FJT" => "+1200"
    case "FKST" => "-0300"
    case "FKT" => "-0400"
    case "FNT" => "-0200"
    case "GALT" => "-0600"
    case "GAMT" => "-0900"
    case "GET" => "+0400"
    case "GFT" => "-0300"
    case "GILT" => "+1200"
    case "GIT" => "-0900"
    case "GMT" => "0000"
    case "GST" => "-0200"
    case "GST" => "+0400"
    case "GYT" => "-0400"
    case "HADT" => "-0900"
    case "HAEC" => "+0200"
    case "HAST" => "-1000"
    case "HKT" => "+0800"
    case "HMT" => "+0500"
    case "HOVST" => "+0800"
    case "HOVT" => "+0700"
    case "ICT" => "+0700"
    case "IDT" => "+0300"
    case "IOT" => "+0300"
    case "IRDT" => "+0430"
    case "IRKT" => "+0800"
    case "IRST" => "+0330"
    case "IST" => "+0530"
    case "IST" => "+0100"
    case "IST" => "+0200"
    case "JST" => "+0900"
    case "KGT" => "+0600"
    case "KOST" => "+1100"
    case "KRAT" => "+0700"
    case "KST" => "+0900"
    case "LHST" => "+1030"
    case "LHST" => "+1100"
    case "LINT" => "+1400"
    case "MAGT" => "+1200"
    case "MART" => "-93000"
    case "MAWT" => "+0500"
    case "MDT" => "-0600"
    case "MET" => "+0100"
    case "MEST" => "+0200"
    case "MHT" => "+1200"
    case "MIST" => "+1100"
    case "MIT" => "-93000"
    case "MMT" => "+0630"
    case "MSK" => "+0300"
    case "MST" => "+0800"
    case "MST" => "-0700"
    case "MUT" => "+0400"
    case "MVT" => "+0500"
    case "MYT" => "+0800"
    case "NCT" => "+1100"
    case "NDT" => "-23000"
    case "NFT" => "+1100"
    case "NPT" => "+0545"
    case "NST" => "-33000"
    case "NT" => "-33000"
    case "NUT" => "-1100"
    case "NZDT" => "+1300"
    case "NZST" => "+1200"
    case "OMST" => "+0600"
    case "ORAT" => "+0500"
    case "PDT" => "-0700"
    case "PET" => "-0500"
    case "PETT" => "+1200"
    case "PGT" => "+1000"
    case "PHOT" => "+1300"
    case "PHT" => "+0800"
    case "PKT" => "+0500"
    case "PMDT" => "-0200"
    case "PMST" => "-0300"
    case "PONT" => "+1100"
    case "PST" => "-0800"
    case "PST" => "+0800"
    case "PYST" => "-0300"
    case "PYT" => "-0400"
    case "RET" => "+0400"
    case "ROTT" => "-0300"
    case "SAKT" => "+1100"
    case "SAMT" => "+0400"
    case "SAST" => "+0200"
    case "SBT" => "+1100"
    case "SCT" => "+0400"
    case "SGT" => "+0800"
    case "SLST" => "+0530"
    case "SRET" => "+1100"
    case "SRT" => "-0300"
    case "SST" => "-1100"
    case "SST" => "+0800"
    case "SYOT" => "+0300"
    case "TAHT" => "-1000"
    case "THA" => "+0700"
    case "TFT" => "+0500"
    case "TJT" => "+0500"
    case "TKT" => "+1300"
    case "TLT" => "+0900"
    case "TMT" => "+0500"
    case "TRT" => "+0300"
    case "TOT" => "+1300"
    case "TVT" => "+1200"
    case "ULAST" => "+0900"
    case "ULAT" => "+0800"
    case "USZ1" => "+0200"
    case "UTC" => "0000"
    case "UYST" => "-0200"
    case "UYT" => "-0300"
    case "UZT" => "+0500"
    case "VET" => "-0400"
    case "VLAT" => "+1000"
    case "VOLT" => "+0400"
    case "VOST" => "+0600"
    case "VUT" => "+1100"
    case "WAKT" => "+1200"
    case "WAST" => "+0200"
    case "WAT" => "+0100"
    case "WEST" => "+0100"
    case "WET" => "0000"
    case "WIT" => "+0700"
    case "WST" => "+0800"
    case "YAKT" => "+0900"
    case "YEKT" => "+0500"
    case _ => "+0"
  }

  /* task aborted:
  95347217: Mar 01 12:07:17.040 AKST %MIVR-APP_MGR-7-TASK_ABORTED:Application task aborted: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]],Application Trigger=ContactApplicationTrigger[time=1456866392575,locale=en_US,cfg=JTAPITriggerConfig[schema=ApplicationTriggerConfig,time=2015-11-23 09:52:03.0,recordId=475,desc=Cisco JTAPI Trigger,name=1008683,type=Cisco JTAPI Trigger,appName=Elections IVR,enabled=true,sessions=60,idleTimeout=5000,locale=en_US,parms={},taskGroups=[],controlClass=class com.cisco.call.CallControlChannel,controlGroupId=7,contactGroups=[GroupInfo[class=com.cisco.dialog.DialogChannel,id=0]],dn=1008683,redirectCSS=default,cmDeviceName=RP_1008683,cmDeviceInvalid=false,cmDescription=Elections IVR,cmDevicePoolUUID={2A8C1867-AD6A-4774-89E8-2B02B9C26F9F},cmDevicePoolName=ANC_CTI_GRP1_DP,cmCallingSearchSpaceUUID={1072ea52-ae2a-d5ae-3924-9f47065feaa3},cmCallingSearchSpaceName=ANC_CTI_CSS,cmLocationUUID={E26F2163-ED2A-9C1F-26AA-BD985ECF8053},cmLocationName=Hub_Anchorage,cmPartitionUUID={85209484-92DD-4335-8818-878CFDF0FC2A},cmPartitionName=ANC_Internal_PT,cmVoiceMailProfileUUID=,cmVoiceMailProfileName=None,cmCallPickUpGroupUUID=,cmCallPickUpGroupName=,cmDisplay=,cmExternalPhNumMask=,cmFwdBusyVM=false,cmFwdBusyDest=,cmFwdBusyCSSUUID=,cmFwdBusyCSSName=None,cmAlertingNameAscii=,cmPresenceGroupUUID=ad243d17-98b4-4118-8feb-5ff2e1b781ac,cmPresenceGroupName=Standard Presence group,campaignID=-1],contact=JTAPICallContact[id=592,implId=16552665/3,state=STATE_ABANDONED_IDX,inbound=true,App name=Elections IVR,task=75000070970,session=86000067173,seq num=0,cn=1008683,dn=1008683,cgn=9076882667,ani=null,dnis=null,clid=null,atype=DIRECT,lrd=null,ocn=1008683,route=RP[num=1008683],OrigProtocolCallRef=0000000000FC92D9033366E300000000,DestProtocolCallRef=null,TP=1009296]],Task id=75000070970,Task Class=class com.cisco.app.impl.WFWorkflowAppDebugTaskWrapper,Exception=com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely
95347218: Mar 01 12:07:17.040 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveException: Contact id: 592, Contact terminated remotely

   */

  /*
  session idle:

  95347234: Mar 01 12:07:17.041 AKST %MIVR-APP_MGR-7-APP_SESSION_IDLE:Idle application session: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]],Application Trigger=ContactApplicationTrigger[time=1456866392575,locale=en_US,cfg=JTAPITriggerConfig[schema=ApplicationTriggerConfig,time=2015-11-23 09:52:03.0,recordId=475,desc=Cisco JTAPI Trigger,name=1008683,type=Cisco JTAPI Trigger,appName=Elections IVR,enabled=true,sessions=60,idleTimeout=5000,locale=en_US,parms={},taskGroups=[],controlClass=class com.cisco.call.CallControlChannel,controlGroupId=7,contactGroups=[GroupInfo[class=com.cisco.dialog.DialogChannel,id=0]],dn=1008683,redirectCSS=default,cmDeviceName=RP_1008683,cmDeviceInvalid=false,cmDescription=Elections IVR,cmDevicePoolUUID={2A8C1867-AD6A-4774-89E8-2B02B9C26F9F},cmDevicePoolName=ANC_CTI_GRP1_DP,cmCallingSearchSpaceUUID={1072ea52-ae2a-d5ae-3924-9f47065feaa3},cmCallingSearchSpaceName=ANC_CTI_CSS,cmLocationUUID={E26F2163-ED2A-9C1F-26AA-BD985ECF8053},cmLocationName=Hub_Anchorage,cmPartitionUUID={85209484-92DD-4335-8818-878CFDF0FC2A},cmPartitionName=ANC_Internal_PT,cmVoiceMailProfileUUID=,cmVoiceMailProfileName=None,cmCallPickUpGroupUUID=,cmCallPickUpGroupName=,cmDisplay=,cmExternalPhNumMask=,cmFwdBusyVM=false,cmFwdBusyDest=,cmFwdBusyCSSUUID=,cmFwdBusyCSSName=None,cmAlertingNameAscii=,cmPresenceGroupUUID=ad243d17-98b4-4118-8feb-5ff2e1b781ac,cmPresenceGroupName=Standard Presence group,campaignID=-1],contact=JTAPICallContact[id=592,implId=16552665/3,state=STATE_ABANDONED_IDX,inbound=true,App name=Elections IVR,task=75000070970,session=86000067173,seq num=0,cn=1008683,dn=1008683,cgn=9076882667,ani=null,dnis=null,clid=null,atype=DIRECT,lrd=null,ocn=1008683,route=RP[num=1008683],OrigProtocolCallRef=0000000000FC92D9033366E300000000,DestProtocolCallRef=null,TP=1009296]],Task id=75000070970,Active Sessions=4,Max Sessions=60

   */

  /*
  95347180: Mar 01 12:07:17.038 AKST %MIVR-APP_MGR-7-INTERRUPTING_TASK:Application task is interrupting: Application=App[name=Elections IVR,type=Cisco Script Application,id=2,desc=Elections IVR,enabled=true,max=60,valid=true,cfg=[ApplicationConfig[schema=ApplicationConfig,time=2014-10-07 10:49:25.0,recordId=276,desc=Elections IVR,name=Elections IVR,type=Cisco Script Application,id=2,enabled=true,sessions=60,script=SCRIPT[Elections/SOA_Elections_Main1.aef],defaultScript=,vars=[<com.cisco.prompt.Playable pInvalidSocialSecurityNumber>,<java.lang.String sPromptDir>,<java.lang.String sBrokerServiceURL>],defaultVars=null]]],Task id=75000070970,Interruption=com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely
95347182: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely

  95347200: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-UNK:Clearing interruption for Task id=75000070970: Exception=com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely
95347201: Mar 01 12:07:17.039 AKST %MIVR-APP_MGR-7-EXCEPTION:com.cisco.app.impl.ContactInactiveInterruption: Contact id: 592, Contact terminated remotely

   */

  /*
  here's how i see this parser working:

  RMI connection established collects logs in bursts
  after every burst is received it is then parsed
  for every task/application started a new actor is fired off; the purpose of this actor is to receive new partial logs relating to the task
    upon either task ended or aborting should complete the request and send off for writing.
    in the event we have "private" fields that should not report key presses, they should be processed prior to comitting for write.

    additionally, using this actor model; it should be possible to "stream" data to connceted websocket clients in real time, allowing for a real-time view of
    the callers progression

    from a reporting perspective, we should capture all the raw data (especially useful for playback) however we should summarize the important bits
    such as the menu option key patterns so when reporting over a large period of time, we can use precomputed data rather than recomputing

    for menu optio nreporting:
      need to look at step config for "flush input buffer property"
        - if not flushed, should visually indicate the menu took from buffer and proceeded; rather than having waited for the prompt
        - if flushed, only assign data pressed after step started exxecuting



      - secondary thoughts
        - it may be worth grabbing the WFWorkflow and serializing the steps to json/AST for storage
        could grab context in realtime to get variable data rather than logging it manually; downside may be additional load in executing AEFs via debugging
          - maybe optimize and do so only on specific steps?
          - this may introduce the possibility for race conditions and likely be less useful than just simply logging it here
        - note how long through the prompt they actually press key - map this back to timing from ASR'd data from the prompt assumign pre-recorded
   */

  /* separate idea: variable tracking
    it should be possible to identify all the variables modified by an action in ContactView and insert code which automatically logs the before, after results

    things like, strings, integers, and the like should all be easy

    difficults will be in documents and prompts (do we really want to log and track the changes here?) may be fun for stuff like generated prompts though.. things to consider
   */
}
