package com.contactview

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/24/16
  * ************************************************************************/


object MathParserTest extends App {
  //  val number: P[Int] = P( CharIn('0'to'9').rep(1).!.map(_.toInt) )
  //  val parens: P[Int] = P( "(" ~/ addSub ~ ")" )
  //  val factor: P[Int] = P( number | parens )
  //
  //  val divMul: P[Int] = P( factor ~ (CharIn("*/").! ~/ factor).rep ).map(eval)
  //  val addSub: P[Int] = P( divMul ~ (CharIn("+-").! ~/ divMul).rep ).map(eval)
  //  val expr: P[Int]   = P( addSub ~ End )

  import fastparse.all._
  val space = P ( CharsWhile(" ".contains(_: Char)).? )
  val whitespace = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))

  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )

  def strChars(exclude : String = "\"\\") = P( CharsWhile(!exclude.contains(_: Char)))
  val string = P( space ~ "\"" ~/ (strChars() | escape).rep.! ~ "\"")

  trait Expr
  trait OperatorImd extends Expr {
    def right : Expr
  }
  trait Operator extends OperatorImd {
    def left : Expr
    def right : Expr
  }
  case class Literal(value : String) extends Expr
  case class Grouping(expr : Expr) extends Expr
  case class AddOpImd(right : Expr) extends OperatorImd
  case class SubOpImd(right : Expr) extends OperatorImd
  case class MulOpImd(right : Expr) extends OperatorImd
  case class DivOpImd(right : Expr) extends OperatorImd
  case class AddOp(left : Expr, right : Expr) extends Operator
  case class SubOp(left : Expr, right : Expr) extends Operator
  case class DivOp(left : Expr, right : Expr) extends Operator
  case class MulOp(left : Expr, right : Expr) extends Operator

  def ooParser(exprParser : Parser[Expr], supportedOps : Seq[Parser[OperatorImd]]) : Parser[Expr] = {
    def factor : P[Expr] = P (exprParser | parens)
    def div = P("/" ~/ factor).map(v => DivOpImd(v))
    def mul = P("*" ~/ factor).map(v => MulOpImd(v))
    def add = P("+" ~/ divMul).map(v => AddOpImd(v))
    def sub = P("-" ~/ divMul).map(v => SubOpImd(v))

    def parens : P [Grouping] = P ("(" ~/ addSub ~ ")" | "[" ~/ addSub ~ "]").map(Grouping)


    def divMul = P (factor ~ (div | mul).rep).map(foldValues)
    def addSub = P (divMul ~ (add | sub).rep).map(foldValues)
//    val expr = P (addSub ~ End)

    def foldValues(value : (Expr, Seq[OperatorImd])) : Expr = {
      val (base, ops) = value
      ops.foldLeft(base) {
        case (l : Expr, r : OperatorImd) => imd2op(l, r)
      }
    }

    def imd2op(l : Expr, imd : OperatorImd) : Operator = imd match {
      case AddOpImd(r) => AddOp(l, r)
      case SubOpImd(r) => SubOp(l, r)
      case MulOpImd(r) => MulOp(l, r)
      case DivOpImd(r) => DivOp(l, r)
    }
    P (addSub ~ End)
  }



//  val expr = ooParser(P(digits).!.map(Literal))
  val expr = ooParser(P(string).map(Literal), Seq())

  expr.parse("(\"3\"+\"4\")") match {
    case Parsed.Success(value, _) => println(evaluate(value))
    case failure : Parsed.Failure =>
      println ("failed to parse string!")
      println(failure)
  }

  def evaluate(expr : Expr) : Int = expr match {
    case Literal(i) => i.toInt
    case AddOp(l, r) => evaluate(l) + evaluate(r)
    case SubOp(l, r) => evaluate(l) - evaluate(r)
    case DivOp(l, r) => evaluate(l) / evaluate(r)
    case MulOp(l, r) => evaluate(l) * evaluate(r)
    case Grouping(e) => evaluate(e)
  }
}
