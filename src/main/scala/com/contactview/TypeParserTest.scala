package com.contactview

import java.util.UUID

import fastparse.all._

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/23/16
  * ************************************************************************/


object TypeParserTest extends App {
  // thought experiment:
  // would it be possible to identify the type in parsing, then finish parsing the expression
  // with a parser based on the identified type

  // a string parser should be able to parse + [string|int]
  // "hello" + 5 should result in
  // "hello" = Parser[Parser[String]]



  val space = P ( CharsWhile(" ".contains(_: Char)).? )
  val whitespace = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))

  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )

  def strChars(exclude : String = "\"\\") = P( CharsWhile(!exclude.contains(_: Char)))
  val string = P( space ~ "\"" ~/ (strChars() | escape).rep.! ~ "\"")

  case class WrappedParser(parser : Parser[Expr], dt : DataType)

  case class ParserContext(parsers : WrappedParser*) {
    def getParsersByType(dt : DataType) : Parser[Expr] = {
      val _parsers = parsers.filter(e => e.dt == dt).map(_.parser)
      combineParsers(_parsers)
    }
    def getCombinedParsers = combineParsers(parsers.map(_.parser))
    private def combineParsers(_parsers : Seq[Parser[Expr]]) = _parsers.tail.foldLeft(_parsers.head) { case (l, r) => l | r }

  }

  def queueNumAgentsLoggedIn(parserContext : ParserContext) : Parser[Expr] = P("number of agents logged into " ~ parserContext.getParsersByType(String)).map(QueueNumberOfAgentsLoggedIn)

  case class QueueNumberOfAgentsLoggedIn(queueName : Expr) extends VariableOrAction { def dt : DataType = Int }

//  val typeParsers : Parser[Expr] = P(stringParser | intParser | promptParser)


  sealed trait DataType {
    def literalParser : WrappedParser
    def parser(left : Expr)(implicit parserContext : ParserContext) : Parser[Expr]

    def opParser(op : String, parser : Parser[Expr]) : Parser[Expr] = P(space ~ op ~ space ~ parser).opaque("expected " + op + "")
  }

  case object String extends DataType {
//    P(string).map(v => Literal(v, String))

    override def literalParser: WrappedParser = WrappedParser(P(string).map(v => Literal(v, String)), this)

    def parser(left : Expr)(implicit parserContext : ParserContext) : Parser[Expr] = {
      val int = parserContext.getParsersByType(Int)
      val str = parserContext.getParsersByType(String)
        opParser("+", str).map(v => AddOp(left, v))
      }
  }

  case object Prompt extends DataType {

    override def literalParser: WrappedParser = WrappedParser(P("P[" ~ strChars("\"\\]").! ~ "]").map(v => Literal(v, Prompt)), this)

    def parser(left : Expr)(implicit parserContext : ParserContext) : Parser[Expr] =  {
      val prompt = parserContext.getParsersByType(Prompt)
      opParser("+", prompt).map(v => AddOp(left, v))
    }
  }

  case object Int extends DataType {

    override def literalParser: WrappedParser = WrappedParser(P(digits).!.map(v => Literal(v, Int)), this)

    override def parser(left: Expr)(implicit parserContext : ParserContext): Parser[Expr] = {
      val int = parserContext.getParsersByType(Int)
      val str = parserContext.getParsersByType(String)
      opParser("+",  int | str).map(v => AddOp(left, v)) |
      opParser("-", int).map(v => SubOp(left, v)) |
      opParser("/", int).map(v => DivOp(left, v)) |
      opParser("*", int).map(v => MulOp(left, v)) |
      opParser("%", int).map(v => ModOp(left, v)) |
      opParser(">", int).map(v => GtOp(left, v)) |
      opParser(">=", int).map(v => GtEOp(left, v)) |
      opParser("<", int).map(v => LtOp(left, v)) |
      opParser("<=", int).map(v => LtEOp(left, v)) |
      opParser("==", int).map(v => CmpOp(left, v))
    }
  }

  sealed trait Expr
  case class Literal(value : String, dt : DataType) extends Expr

  sealed trait VariableOrAction extends Expr {
    def dt : DataType
  }
  case class VariableRef(uuid : UUID, dt : DataType) extends VariableOrAction
  case class Variable(uuid : UUID, name : String, value : String, dt : DataType) extends Expr

  case class Reference(ref : VariableOrAction) extends Expr

  sealed trait Operator extends Expr {
    def left : Expr
    def right : Expr
  }

  // Operators
  case class CmpOp(left : Expr, right : Expr) extends Operator
  case class AddOp(left : Expr, right : Expr) extends Operator
  case class SubOp(left : Expr, right : Expr) extends Operator
  case class DivOp(left : Expr, right : Expr) extends Operator
  case class MulOp(left : Expr, right : Expr) extends Operator
  case class ModOp(left : Expr, right : Expr) extends Operator
  case class GtOp(left : Expr, right : Expr) extends Operator
  case class GtEOp(left : Expr, right : Expr) extends Operator
  case class LtOp(left : Expr, right : Expr) extends Operator
  case class LtEOp(left : Expr, right : Expr) extends Operator



  /*
  the way I envision this working:
  a type parser identifies the type first
  then that parser begins to parse the rest of the string with parses valid for that type
  these subsequent parses should return an expression for their part of the parse
   */

  def extractParserFromExpr(expr : Expr, left : Option[Expr] = None)(implicit parserContext : ParserContext) : Option[Parser[Expr]] = expr match {
    case lit : Literal => Some(lit.dt.parser(left.getOrElse(expr)))
    case op  : Operator => extractParserFromExpr(op.right, left = Some(expr))
    case vr  : VariableOrAction => Some(vr.dt.parser(left.getOrElse(expr)))
    case ref : Reference => Some(ref.ref.dt.parser(left.getOrElse(expr)))
  }

  def parseExpression(expression : String, index : Int = 0, expr : Option[Expr] = None)(implicit parserContext : ParserContext) : Either[Expr, Parsed.Failure] = {
    def parser : Parser[Expr] = expr match {
      case Some(_expr : Expr) => extractParserFromExpr(_expr).getOrElse(parserContext.getCombinedParsers)
      case None => parserContext.getCombinedParsers
    }

    P(parser | End.opaque("expected end")).parse(expression, index) match {
      case Parsed.Success(left : Expr, _index) => parseExpression(expression, _index, Some(left))
      case Parsed.Success(_, _) => Left(expr.get)
      case fail : Parsed.Failure => Right(fail)
    }
  }



  // to test:
  // need to be able to parse: number of agents logged into "queue name"

  implicit val parserContext = ParserContext(
    String.literalParser,
    Int.literalParser,
    Prompt.literalParser
  )

  println (parseExpression("3 + 5 / 1 * 5 %2"))
  println (parseExpression("P[30] - P[100.wav]"))
//  parseExpression(inputString) match {
//    case Left(expr) =>
//      println(expr)
//          println(upickle.default.write(expr))
//    case Right(failure) =>
//      println (failure)
//      println ("failed to parse")
//  }

  val tStr = "56 + number of agents logged into \"the queue\" == 3"

  println (parseExpression(tStr))

  /*
  each parser needs to be able to parse given parsers that are dynamically available
  each parser needs to be able to parse within the same type. i.e; need to be able to pass a string parser containing itself

   */

  def generateParser() : Parser[String] = {
    println ("parsing!!")
    P (string)
  }

  val dp = P ("hello " ~ generateParser())

  println(dp.parse("hello \"world\""))
}
