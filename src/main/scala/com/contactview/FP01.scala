package com.contactview

import fastparse.all._

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/27/16
  * ************************************************************************/


object FP01 extends App {

  object Lexical {
    import fastparse.all._
    def kw(s: String) = s ~ !(letter | digit | "_")
    val comment = P( "#" ~ CharsWhile(_ != '\n', min = 0) )
    val wscomment = P( (CharsWhile(" \n".toSet, min = 1) | Lexical.comment | "\\\n").rep )
    val nonewlinewscomment = P( (CharsWhile(" ".toSet, min = 1) | Lexical.comment | "\\\n").rep )

    val identifier: P[Ast.identifier] =
      P( (letter|"_") ~ (letter | digit | "_").rep ).!.filter(!keywordList.contains(_)).map(v => Ast.identifier(v))
    val letter     = P( lowercase | uppercase )
    val lowercase  = P( CharIn('a' to 'z') )
    val uppercase  = P( CharIn('A' to 'Z') )
    val digit      = P( CharIn('0' to '9') )

    val keywordList = Set(
      "and",       "del",       "from",      "not",       "while",
      "as",        "elif",      "global",    "or",        "with",
      "assert",    "else",      "if",        "pass",      "yield",
      "break",     "except",    "import",    "print",
      "class",     "exec",      "in",        "raise",
      "continue",  "finally",   "is",        "return",
      "def",       "for",       "lambda",    "try"
    )

    val stringliteral: P[String] = P( stringprefix.? ~ (longstring | shortstring) )
    val stringprefix: P0 = P(
      "r" | "u" | "ur" | "R" | "U" | "UR" | "Ur" | "uR" | "b" | "B" | "br" | "Br" | "bR" | "BR"
    )
    val shortstring: P[String] = P( shortstring0("'") | shortstring0("\"") )
    def shortstring0(delimiter: String) = P( delimiter ~ shortstringitem(delimiter).rep.! ~ delimiter)
    def shortstringitem(quote: String): P0 = P( shortstringchar(quote) | escapeseq )
    def shortstringchar(quote: String): P0 = P( CharsWhile(!s"\\\n${quote(0)}".contains(_)) )

    val longstring: P[String] = P( longstring0("'''") | longstring0("\"\"\"") )
    def longstring0(delimiter: String) = P( delimiter ~ longstringitem(delimiter).rep.! ~ delimiter)
    def longstringitem(quote: String): P0 = P( longstringchar(quote) | escapeseq | !quote ~ quote.take(1)  )
    def longstringchar(quote: String): P0 = P( CharsWhile(!s"\\${quote(0)}".contains(_)) )

    val escapeseq: P0 = P( "\\" ~ AnyChar )


    val longinteger: P[BigInt] = P( integer ~ ("l" | "L") )
    val integer: P[BigInt] = P( octinteger | hexinteger | bininteger | decimalinteger)
    val decimalinteger: P[BigInt] = P( nonzerodigit ~ digit.rep | "0" ).!.map(scala.BigInt(_))
    val octinteger: P[BigInt] = P( "0" ~ ("o" | "O") ~ octdigit.rep(1).! | "0" ~ octdigit.rep(1).! ).map(scala.BigInt(_, 8))
    val hexinteger: P[BigInt] = P( "0" ~ ("x" | "X") ~ hexdigit.rep(1).! ).map(scala.BigInt(_, 16))
    val bininteger: P[BigInt] = P( "0" ~ ("b" | "B") ~ bindigit.rep(1).! ).map(scala.BigInt(_, 2))
    val nonzerodigit: P0 = P( CharIn('1' to '9') )
    val octdigit: P0 = P( CharIn('0' to '7') )
    val bindigit: P0 = P( "0" | "1" )
    val hexdigit: P0 = P( digit | CharIn('a' to 'f', 'A' to 'F') )


    val floatnumber: P[BigDecimal] = P( pointfloat | exponentfloat )
    val pointfloat: P[BigDecimal] = P( intpart.? ~ fraction | intpart).!.map(BigDecimal(_))
    val exponentfloat: P[BigDecimal] = P( (intpart | pointfloat) ~ exponent ).!.map(BigDecimal(_))
    val intpart: P[BigDecimal] = P( digit.rep(1) ).!.map(BigDecimal(_))
    val fraction: P0 = P( "." ~ digit.rep(1) )
    val exponent: P0 = P( ("e" | "E") ~ ("+" | "-").? ~ digit.rep(1) )


    val imagnumber = P( (floatnumber | intpart) ~ ("j" | "J") )
  }

  object Ast {
    case class identifier(name : String)

    trait Expr
    trait Literal extends Expr



    case class Num(value : Any) extends Expr

    case class Str(value : String) extends Literal

    case class Attribute(lhs : Expr, id : identifier) extends Expr

    sealed trait boolop
    object boolop{
      case object And extends boolop
      case object Or extends boolop
    }

    sealed trait operator
    case object operator{
      case object Add extends operator
      case object Sub  extends operator
      case object Mult  extends operator
      case object Div  extends operator
      case object Mod  extends operator
      case object Pow  extends operator
      case object LShift  extends operator
      case object RShift  extends operator
      case object BitOr  extends operator
      case object BitXor  extends operator
      case object BitAnd  extends operator
      case object FloorDiv extends operator
    }

    sealed trait unaryop
    object unaryop{

      case object Invert extends unaryop
      case object Not extends unaryop
      case object UAdd extends unaryop
      case object USubextends extends unaryop
    }
    sealed trait cmpop
    object cmpop{

      case object Eq extends cmpop
      case object NotEq extends cmpop
      case object Lt extends cmpop
      case object LtE extends cmpop
      case object Gt extends cmpop
      case object GtE extends cmpop
      case object Is extends cmpop
      case object IsNot extends cmpop
      case object In extends cmpop
      case object NotIn extends cmpop
    }

    type expr = Expr
    object expr{
      case class BoolOp(op: boolop, values: Seq[expr]) extends expr
      case class BinOp(left: expr, op: operator, right: expr) extends expr
      case class UnaryOp(op: unaryop, operand: expr) extends expr
//      case class Lambda(args: arguments, body: expr) extends expr
      case class IfExp(test: expr, body: expr, orelse: expr) extends expr
      case class Dict(keys: Seq[expr], values: Seq[expr]) extends expr
      case class Set(elts: Seq[expr]) extends expr
//      case class ListComp(elt: expr, generators: Seq[comprehension]) extends expr
//      case class SetComp(elt: expr, generators: Seq[comprehension]) extends expr
//      case class DictComp(key: expr, value: expr, generators: Seq[comprehension]) extends expr
//      case class GeneratorExp(elt: expr, generators: Seq[comprehension]) extends expr
      // the grammar constrains where yield expressions can occur
      case class Yield(value: Option[expr]) extends expr
      // need sequences for compare to distinguish between
      // x < 4 < 3 and (x < 4) < 3
      case class Compare(left: expr, ops: Seq[cmpop], comparators: Seq[expr]) extends expr
      case class Call(func: expr, args: Seq[expr]) extends expr // keywords: Seq[keyword], starargs: Option[expr], kwargs: Option[expr]) extends expr
      case class Repr(value: expr) extends expr
//      case class Num(n: Any) extends expr // a number as a PyObject.
//      case class Str(s: string) extends expr // need to raw: specify, unicode, etc?
      // other bools: Option[literals]?

      // the following expression can appear in assignment context
//      case class Attribute(value: expr, attr: identifier, ctx: expr_context) extends expr
//      case class Subscript(value: expr, slice: slice, ctx: expr_context) extends expr
//      case class Name(id: identifier, ctx: expr_context) extends expr
//      case class List(elts: Seq[expr], ctx: expr_context) extends expr
//      case class Tuple(elts: Seq[expr], ctx: expr_context) extends expr
    }
    case class keyword(arg: identifier, value: expr)

  }

  def op[T](s: P0, rhs: T) = s.!.map(_ => rhs)
  val LShift = op("<<", Ast.operator.LShift)
  val RShift = op(">>", Ast.operator.RShift)
  val Lt = op("<", Ast.cmpop.Lt)
  val Gt = op(">", Ast.cmpop.Gt)
  val Eq = op("==", Ast.cmpop.Eq)
  val GtE = op(">=", Ast.cmpop.GtE)
  val LtE = op("<=", Ast.cmpop.LtE)
  val NotEq = op("<>" | "!=", Ast.cmpop.NotEq)
  val In = op("in", Ast.cmpop.In)
  val NotIn = op("not" ~ "in", Ast.cmpop.NotIn)
  val Is = op("is", Ast.cmpop.Is)
  val IsNot = op("is" ~ "not", Ast.cmpop.IsNot)
  val comp_op = P( LtE|GtE|Eq|Gt|Lt|NotEq|In|NotIn|IsNot|Is )
  val Add = op("+", Ast.operator.Add)
  val Sub = op("-", Ast.operator.Sub)
  val Pow = op("**", Ast.operator.Pow)
  val Mult= op("*", Ast.operator.Mult)
  val Div = op("/", Ast.operator.Div)
  val Mod = op("%", Ast.operator.Mod)
  val FloorDiv = op("//", Ast.operator.FloorDiv)
  val BitOr = op("|", Ast.operator.BitOr)
  val BitAnd = op("&", Ast.operator.BitAnd)
  val BitXor = op("^", Ast.operator.BitXor)

  val NUMBER = P( Lexical.floatnumber | Lexical.longinteger | Lexical.integer | Lexical.imagnumber ).map(Ast.Num)
  val STRING = Lexical.stringliteral
  val NAME: P[Ast.identifier] = Lexical.identifier

  def Chain(p: P[Ast.expr], op: P[Ast.operator]) = P( p ~ (op ~ p).rep ).map{
    case (lhs, chunks) =>
      chunks.foldLeft(lhs) { case (lhs, (op, rhs)) =>
        Ast.expr.BinOp(lhs, op, rhs)
      }
  }

  case class LoggedInAgents(queueName : Ast.Expr) extends Ast.Expr
  val lia = P("logged in agents " ~ atom).map(LoggedInAgents)
  val atom : Parser[Ast.Expr] =STRING.rep(1).map(_.mkString).map(Ast.Str) | NUMBER | lia
  val trailer : Parser[Ast.Expr => Ast.Expr] = {
    val attribute = P("." ~ NAME).map(id => (lhs : Ast.Expr) => Ast.Attribute(lhs, id))
    val call = P ("(" ~ atom.rep(sep=",") ~ ")").map(args => (lhs : Ast.Expr) => Ast.expr.Call(lhs, args))
    P( attribute | call )
  }

  val power = P(atom ~ trailer.rep ~ (Pow ~ factor).?).map {
    case (lhs, trailers, rhs) =>
      val left = trailers.foldLeft(lhs) { case (l, r) => r(l) }
      rhs match {
        case None => left
        case Some((op, right)) => Ast.expr.BinOp(left, op, right)
      }
  }

  val factor : Parser[Ast.Expr] = P ( ("+" | "-") ~ factor | power)
  val term: P[Ast.expr] = P( Chain(factor, Mult | Div | Mod | FloorDiv) )
  val arith_expr: P[Ast.expr] = P( Chain(term, Add | Sub) )
  val shift_expr: P[Ast.expr] = P( Chain(arith_expr, LShift | RShift) )
  val and_expr: P[Ast.expr] = P( Chain(shift_expr, BitAnd) )
  val xor_expr: P[Ast.expr] = P( Chain(and_expr, BitXor) )
  val expr: P[Ast.expr] = P( Chain(xor_expr, BitOr) )


  println (expr.parse("3+2"))

  val comparison : P[Ast.Expr] = P(expr ~ (comp_op ~ expr).rep).map {
    case (lhs, Nil) => lhs
    case (lhs, chunks) =>
      val (op, vals) = chunks.unzip
      Ast.expr.Compare(lhs, op, vals)
  }
  val not_test : Parser[Ast.Expr] = P( ("not" ~ not_test).map(Ast.expr.UnaryOp(Ast.unaryop.Not, _)) | comparison )
  val and_test : Parser[Ast.Expr] = P( not_test.rep(1, Lexical.kw("and")) ).map {
    case Seq(x) => x
    case xs => Ast.expr.BoolOp(Ast.boolop.And, xs)
  }
  val or_test = P( and_test.rep(1, Lexical.kw("or")) ).map{
    case Seq(x) => x
    case xs => Ast.expr.BoolOp(Ast.boolop.Or, xs)
  }
  val test: P[Ast.expr] = {
    val ternary = P( or_test ~ (Lexical.kw("if") ~ or_test ~ Lexical.kw("else") ~ test).? ).map{
      case (x, None) => x
      case (x, Some((test, neg))) => Ast.expr.IfExp(test, x, neg)
    }
    P( ternary )
  }

  println(test.parse("3+4==5-3"))


}
