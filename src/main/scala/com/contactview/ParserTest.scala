package com.contactview

import java.util.UUID

import fastparse.core.Parser

import scala.scalajs.js.annotation.JSExport

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 8/27/15
  * ************************************************************************/



@JSExport("ParserTest")
object ParserTest extends App {

  /*
    goals of the parsing system

    support literals, references and operations
    provide useful error messages
    provide data types that are easily serializable

    goals for actions

    action parsers only available if all the requirements for that action are met (certain types being avialable, etc)
    actions provide a kind of template/structure defining their requirements
   */

  /*
  Version 1 goals:
    - basic parsers : allow for quick and easy parsing
    - modular : allows dynamic loading of parsers and inline parsers for basic expressions
    - provide ui text information (english only)
  not goals for version 1:
    - support operation expressions, i.e: 1 + 2 will not be supported.
    - not "self documenting"
  known obstacles:
    - better error reporting for InlineActions will be difficult
    - inline actions are very fragile. it is difficult to modle InlineActions to work with References due to sealed nature.
    - upickle will not like InlineActions to join the ranks of Literal or Reference because the generic nature
    - parser should only be available if all the data types the parser depends on are available in the context but this is hard to enforce when parsers are not built dynamically
  wish list:
    - Would like a single "unified" Templating capability that could define the UI information as well as the parser information and have it built dynamically while preserving the types for the parser
      i.e: val prioritySet = Template("set",CallContact,"priority to",Int) could convert to a parser that we could map on:
      prioritySet.toParser.map(p => ... where p would be of type: (CallContactExpr, IntExpr) and would be used as such:
      prioritySet.toParser.map(case (caller : CallContactExpr, priority : IntExpr) => Priority(Priority.Operation.SET, caller, priority)
      ideally toParser would take the context and produce something like Option[Parser[Action]] type where
      if required parts of the template do note exist such as there are no "IntExpr" available,
      then the parser would not render and be considered invalid and ideally including error information as to why.
      This information could then also be translated to the UI indicating the availability of actions.
   */

//  implicit val context = ActionContext(List(Variable(CallContact, "the caller"), Variable(Queue, "myqueue")))
//  context.loadPackage(ActionsGeneral)
//  context.loadPackage(ActionsUccx)
//
//
//  println (upickle.default.write(Literal("hello world!")))
//  println (upickle.default.write(IntExpr(Literal("hello world!"))))
//
//
//  println (upickle.default.write(Basic(IntExpr(Literal("hello!")))))
//  val json = context.writeJson(context.parse("set the caller priority to 5").get.value)
//  println(json)
//  val actionJson = context.readJson(json.get)
//  println(actionJson)
//
//  actionJson match {
//    case Some(action) =>
//      context.processor(action) match {
//        case Some(processor) => println(processor.toString(action).getOrElse(""))
//        case _ =>
//      }
//    case _ =>
//  }
//
//  println(context.parse("set the caller priority booo").asInstanceOf[Result.Failure].traced.traceParsers)
//  println(context.parseIndex("set the caller priority booo"))
//  println(context.parseIndex("set the caller priority by 69"))
//  println(context.parseIndex("set the caller priority by total number of agents logged in to myqueue"))
//  println(context.parseIndex("set the caller priority by total number of agents logged in"))
//
////  println (upickle.default.write(context.parse("set the caller priority to my int").get.value))




//  def parserField(fieldName : String) : Parser[String] = {
//    val str = s"\"$fieldName\":"
//    P (str ~ string ~ ",".?)
//  }
//
//  val menuParser : Parser[Menu2] = P("{" ~ parserField("name").? ~ parserField("caller").map(CallContact) ~ "}").map(v => Menu2(v._1, v._2))
//  println (menuParser.parse(valueMapStr))

  // this works, but order of fields cannot be gauranteed and doing this doesn't give us any improvement out of simply getting from the map directly
//----

//
//  trait VarType
//  case class vtString(value : String) extends VarType
//  case class vtInt(value : Int) extends VarType
//
//  case class AddOp() extends VarType
//
//
//  case class Variable(uuid : UUID, name : String)
//
//  val vars = List(Variable(UUID.randomUUID(), "voicemail message"), Variable(UUID.randomUUID(), "PIN"))
//
//  import fastparse.all._
//
//  def varSeq = vars.map(_.name).toSeq
//
//  val varParser = StringIn(varSeq : _*).!.map(name => vars.find(p => p.name == name).get)
//
//  val space = P ( CharsWhile(" \r\n".contains(_: Char)).? )
//  val strChars = P (CharsWhile(!"\"\\".contains(_: Char)))
//  val strCapture = strChars.!.map(vtString)
//  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
//  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
//  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )
//  val string = P( space.? ~ "\"" ~/ (strChars | escape).rep.! ~ "\"").map(vtString)
//  val digits = P (CharsWhile('0' to '9' contains (_: Char))).!.map(v => vtInt(v.toInt))
//  val op : P[VarType] = P("+").map(v => AddOp())
//
//  val expr = P (space.? ~ (varParser | string | op)).rep
//
//
//
//  val strExpr = expr.filter(v => !v.exists(!_.isInstanceOf[vtString]))
//
//  println(strExpr.parse(""""Hello world!""""))
//
//  trait ResultType {
//    def className : String
//  }
//  case object StringResultType extends ResultType { def className = "java.lang.String" }
//  case object IntegerResultType extends ResultType { def className = "java.lang.Integer" }
//  case class DynamicResultType(className : String) extends ResultType
//
//  trait Literal {
//    def resultType : ResultType
//  }
//  case class StringLiteral(value : String) extends Literal { def resultType = StringResultType }
//  case class IntegerLiteral(value : Int) extends Literal { def resultType = IntegerResultType }
//
//  case class VariableRef(uuid : UUID, name : String, resultType : ResultType)
//
////  val digits        = P( CharsWhile('0' to '9' contains (_: Char)))
//  val exponent      = P( CharIn("eE") ~ CharIn("+-").? ~ digits )
//  val fractional    = P( "." ~ digits )
//  val integral      = P( "0" | CharIn('1' to '9') ~ digits.? )
//
//  val number = P( CharIn("+-").? ~ integral ~ fractional.? ~ exponent.? ).!.map(
//    x => IntegerLiteral(x.toInt)
//  )
//  val string2 = P( space.? ~ "\"" ~/ (strChars | escape).rep.! ~ "\"").map(StringLiteral)
//
//  val exp = P (number | string2 | op)
//
//  val stmt = (space.? ~ exp).rep
//  op
//
//  println (stmt.parse(""""Hello world!!" + 34"""))
//
//  val strStmt = stmt.filter {
//    case s : Literal => s.resultType == StringResultType
//    case _ => false
//  }


//  case class Variable(name : String) {
//    require(!name.contains('+'))
//  }
//  case class Context(variables : List[Variable]) {
//    @JSExport
//    def addVariable(variable : Variable) : Context = Context(variables ++ List(variable))
//  }
//
//  var inString =
//    """Hello
//      |world
//    """.stripMargin
//  println (inString)
//
//
//
//
//  val var1 = Variable("my int variable 3")
//  val ctx = Context(List(var1))
//
//  trait Expr
//  case class VariableRef(variable : String) extends Expr
//  case class AddOp() extends Expr
//  case class IntLiteral(value : Int) extends Expr
//
//  import fastparse.all._
//
//  val varParser : P[VariableRef] = StringIn(ctx.variables.map(_.name) :_*).!.map(VariableRef)
//
//  val inputString = "my int variable 3+3"
//
//
//  val space = P ( CharsWhile(" \r\n".contains(_: Char)).? )
//  val digits = P( CharsWhile('0' to '9' contains (_: Char))).!.map(v => IntLiteral(v.toInt))
//  val addParser = P ("+").map(v => AddOp())
//
//  val exprParser = P(varParser | addParser | digits).rep
//  println(exprParser.parse(inputString))
//
//  val hwParser = P(Start ~ "Hello" ~ space ~ "world" ~ space ~ End).!
//  println (hwParser.parse(inString))


  //boolean parser
  import fastparse.all._

//  object  WsApi extends fastparse.WhitespaceApi.Wrapper ({
//    import fastparse.all._
//    P(CharsWhile(" \r\n".contains(_: Char)).?)
//  })
//
//  import fastparse.noApi._
//  import WsApi._

  sealed trait expr
  sealed trait operator
  case class operation(expr1 : expr, operator : operator, expr2 : expr) extends expr
  case class grouping(expr : expr) extends expr
  case class term(string : String) extends expr
  case object AND extends operator
  case object OR extends operator

  val space = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val strCharsNoSpace = P (CharsWhile(!"\"\\ ()".contains(_: Char)))
  val strChars = P (CharsWhile(!"\"\\()".contains(_: Char)))
  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
//  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
//  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )
  val string : P[expr] = P( ("\"" ~/ strChars.rep.! ~ "\"" | strCharsNoSpace.rep.! ) ~ space.?).map(_.trim).filter(_.nonEmpty).map(v => term(v))



  val or_p : P[operator] = P(IgnoreCase("or") ~ space.?).map(_ => OR)
  val and_p : P[operator] = P(IgnoreCase("and") ~ space.?).map(_ => AND)
  val operators = P(or_p | and_p)
  val base : P[expr] = P((string | expr_p) ~ (space.? ~ operators ~/ expr_p).rep).map {
    case (str, ops) =>
      ops match {
        case Nil => str
        case Seq(op) => operation(str, op._1, op._2)
        case ops => ops.foldLeft(str) { case (l, (o, r)) => operation(l, o, r) }
      }
  }
  val parens = P( P("(" ~/ expr_p ~ ")").map(grouping))
  val expr_p : P[expr] = P(parens | base)// | parens)// | base)//.map(seq)
  val parser = P(base)

  val result = parser.parse("(hello AND (goodbye OR \"cisco uccx\" OR uccx)) OR foobar OR me").get.value
  println(result)
//  println(getRequiredKeys(result))
  println (evaluateExpr("hello me uccx", result))

  def evaluateExpr(str : String, expr : expr) : Boolean = {
    def doEval(str : String, expr : expr) : Boolean = {
      expr match {
        case operation(expr1, operator, expr2) =>
          val _expr1 = doEval(str, expr1)
          val _expr2 = doEval(str, expr2)
          operator match {
            case AND => _expr1 && _expr2
            case OR => _expr1 || _expr2
          }
        case grouping(expr) => doEval(str, expr)
        case term(string) => str.matches(s".*\\b${string.toLowerCase}\\b.*")
      }
    }
    doEval(str.toLowerCase, expr)
  }

  //this doesnt workas intended
//  def getRequiredKeys(expr : expr) : Seq[expr] = {
//    println (s"get required keys expr=$expr")
//    def goDeep(expr : expr) : Seq[expr] = {
//      expr match {
//        case operation(expr1, operator, expr2) =>
//          val _1 = {
//            expr1 match {
//              case term(string) => Seq(expr1)
//              case _ => goDeep(expr1)
//            }
//          }
//          _1 ++ goDeep(expr2)
//        case grouping(expr) => goDeep(expr)
//        case term(string) => Seq(term(string))
//      }
//    }
//    expr match {
//      case operation(expr1, operator, expr2) =>
//        goDeep(expr1) ++ goDeep(expr2)
//      case grouping(expr) => getRequiredKeys(expr)
//      case term(string) => Seq(term(string))
//    }
//  }
}
