package com.contactview

import com.contactview.JavaParserTest.IntegerLiteral.OctalNumeral

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 11/8/16
  * ************************************************************************/


object JavaParserTest extends App {

  object  WsApi extends fastparse.WhitespaceApi.Wrapper ({
    import fastparse.all._
    P(P(Whitespace.parser.opaque("whitespace") | Comment.parser.opaque("comment")).rep.?)
  })
  import fastparse.noApi._
  import WsApi._

  trait Token {
    val parser : P[_]
  }
  trait expr
  case class C(value : Char) extends expr

  val hexDigit : P[Unit] = P(CharIn(('0' to '9') ++ ('a' to 'f') ++ ('A' to 'F')))

  // the following implements chapter 3 of the java language specification: http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html

  object UnicodeEscape extends Token {
    /*
      Unicode escape
        UnicodeInputCharacter:
          UnicodeEscape
          RawInputCharacter

        UnicodeEscape:
            \ UnicodeMarker HexDigit HexDigit HexDigit HexDigit

        UnicodeMarker:
            u
            UnicodeMarker u

        RawInputCharacter:
            any Unicode character

        HexDigit: one of
            0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F
    */
    val unicodeMarker : P[Unit] = P("u")
    val unicodeEscape : P[C] = P("\\" ~ unicodeMarker ~ hexDigit.rep(min = 4, max = 4).!).map(v => C(Integer.parseInt(v, 16).toChar))
    val rawInputCharacter : P[C] = P(AnyChar).!.map(v => C(v.toCharArray()(0)))
    val unicodeInputCharacter : P[C] = P (unicodeEscape | rawInputCharacter)
    val parser : P[C] = P(unicodeInputCharacter)
  }

  object LineTerminator extends Token {
    /*
      LineTerminator:
        the ASCII LF character, also known as "newline"
        the ASCII CR character, also known as "return"
        the ASCII CR character followed by the ASCII LF character
     */
    val lineTerminator : P[Unit] = P("\n" | "\r" | "\n\r")
    val parser : P[Unit] = P(lineTerminator)
  }

  object InputCharacter extends Token {
    /*
      InputCharacter:
        UnicodeInputCharacter but not CR or LF
     */
    val inputCharacter : P[C] = P(UnicodeEscape.parser ~ !LineTerminator.parser)
    val parser : P[C] = P(inputCharacter)
  }

  object InputElementsAndTokens extends Token {
    /*
      Input:
          InputElementsopt Subopt

      InputElements:
          InputElement
          InputElements InputElement

      InputElement:
          WhiteSpace
          Comment
          Token

      Token:
          Identifier
          Keyword
          Literal
          Separator
          Operator

      Sub:
          the ASCII SUB character, also known as "control-Z"
     */

    val parser = ???
  }

  object Whitespace extends Token {
    /*
        WhiteSpace:
            the ASCII SP character, also known as "space"
            the ASCII HT character, also known as "horizontal tab"
            the ASCII FF character, also known as "form feed"
            LineTerminator
     */
    val whitespace : P[Unit]  = CharsWhile(" \t\f".contains(_))
    val parser : P[Unit] = P( whitespace | LineTerminator.parser )
  }

  object Comment extends Token {
    /*
      Comment:
          TraditionalComment
          EndOfLineComment

      TraditionalComment:
          / * CommentTail

      EndOfLineComment:
          / / CharactersInLineopt

      CommentTail:
          * CommentTailStar
          NotStar CommentTail

      CommentTailStar:
          /
          * CommentTailStar
          NotStarNotSlash CommentTail

      NotStar:
          InputCharacter but not *
          LineTerminator

      NotStarNotSlash:
          InputCharacter but not * or /
          LineTerminator

      CharactersInLine:
          InputCharacter
          CharactersInLine InputCharacter
     */
    object  WsApi extends fastparse.WhitespaceApi.Wrapper ({
      import fastparse.all._
      P(P(Whitespace.parser.opaque("whitespace")).rep.?)
    })
    import fastparse.noApi._
    import WsApi._
    case class Comment(comment : String)
    val c : P[Comment]  = P(P("//" ~ (!LineTerminator.parser ~ AnyChar).rep).!.map(Comment) | P("/*" ~/ P((!("*/" | "/*") ~ AnyChar) | c ).rep.! ~/ "*/").map(Comment))

    val parser = c.map(_ => ())
  }

  object Identifiers extends Token {
    /*
      Identifier:
          IdentifierChars but not a Keyword or BooleanLiteral or NullLiteral

      IdentifierChars:
          JavaLetter
          IdentifierChars JavaLetterOrDigit

      JavaLetter:
          any Unicode character that is a Java letter (see below)

      JavaLetterOrDigit:
          any Unicode character that is a Java letter-or-digit (see below)
     */
    val javaLetterOrDigit : P[String] = P(javaLetter | CharIn('0' to '9').!)
    val javaLetter : P[String]= P(CharIn(('a' to 'z') ++ ('A' to 'Z')).!)
    val identifierChars : P[String] = P(javaLetter  ~ javaLetterOrDigit.rep).!
    val identifier = P(identifierChars.filter(!Keywords.keywords.contains(_))) //fixme: not a keyword or bool or null

    val parser = P(identifier)
  }

  object Keywords {
    /*
        Keyword: one of
          abstract   continue   for          new         switch
          assert     default    if           package     synchronized
          boolean    do         goto         private     this
          break      double     implements   protected   throw
          byte       else       import       public      throws
          case       enum       instanceof   return      transient
          catch      extends    int          short       try
          char       final      interface    static      void
          class      finally    long         strictfp    volatile
          const      float      native       super       while
     */
    val keywords =
      Seq(
        "abstract", "continue", "for", "new", "switch ",
        "assert", "default", "if", "package", "synchronized ",
        "boolean", "do", "goto", "private", "this ", "break",
        "double", "implements", "protected", "throw ", "byte",
        "else", "import", "public", "throws ", "case", "enum",
        "instanceof", "return", "transient ", "catch", "extends",
        "int", "short", "try ", "char", "final", "interface",
        "static", "void ", "class", "finally", "long", "strictfp",
        "volatile ", "const", "float", "native", "super", "while"
      )
  }

  object Literal extends Token {
    /*
      Literal:
        IntegerLiteral
        FloatingPointLiteral
        BooleanLiteral
        CharacterLiteral
        StringLiteral
        NullLiteral
     */
    val parser = ???
  }

  object IntegerLiteral extends Token {
    /*
        IntegerLiteral:
            DecimalIntegerLiteral
            HexIntegerLiteral
            OctalIntegerLiteral
            BinaryIntegerLiteral

        DecimalIntegerLiteral:
            DecimalNumeral IntegerTypeSuffixopt

        HexIntegerLiteral:
            HexNumeral IntegerTypeSuffixopt

        OctalIntegerLiteral:
            OctalNumeral IntegerTypeSuffixopt

        BinaryIntegerLiteral:
            BinaryNumeral IntegerTypeSuffixopt

        IntegerTypeSuffix: one of
            l L
     */

    val integerTypeSuffix = P ("l" | "L")
    val binaryIntegerLiteral = P (BinaryNumeral.binaryNumeral ~ integerTypeSuffix.?)
    val octalIntegerLiteral = P (OctalNumeral.octalNumeral ~ integerTypeSuffix.?)
    val hexIntegerLiteral = P (HexNumeral.hexNumeral ~ integerTypeSuffix.?)
    val decimalLiteral = P (DecimalNumeral.decimalNumeral ~ integerTypeSuffix.?)

    object DecimalNumeral {
      /*
          DecimalNumeral:
              0
              NonZeroDigit Digitsopt
              NonZeroDigit Underscores Digits

          Digits:
              Digit
              Digit DigitsAndUnderscoresopt Digit

          Digit:
              0
              NonZeroDigit

          NonZeroDigit: one of
              1 2 3 4 5 6 7 8 9

          DigitsAndUnderscores:
              DigitOrUnderscore
              DigitsAndUnderscores DigitOrUnderscore

          DigitOrUnderscore:
              Digit
              _

          Underscores:
              _
              Underscores _
       */

      val underScores = P(CharsWhile(_ == "_"))
      val digitOrUnderscore : P0 = P(digit | "_")
      val digitsAndUnderscores : P0 =  P(digitOrUnderscore | digitsAndUnderscores ~ digitOrUnderscore)
      val nonZeroDigit = P(CharIn('1' to '9'))
      val digit = P("0" | nonZeroDigit)
//      val digits = P((digit | digit ~ digitsAndUnderscores.? ~ digit).rep)
      val digits = CharIn("0123456789")
//      val decimalNumeral = P("0" | nonZeroDigit ~ digits.? | nonZeroDigit ~ underScores ~ digits)
      val decimalNumeral = P("0" | (nonZeroDigit ~ ("_" | digits).?)).rep
    }

    object HexNumeral {
      /*
        HexNumeral:
            0 x HexDigits
            0 X HexDigits

        HexDigits:
            HexDigit
            HexDigit HexDigitsAndUnderscoresopt HexDigit

        HexDigit: one of
            0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F

        HexDigitsAndUnderscores:
            HexDigitOrUnderscore
            HexDigitsAndUnderscores HexDigitOrUnderscore

        HexDigitOrUnderscore:
            HexDigit
            _
       */
      val hexDigit = CharIn("0123456789abcdefABCDEF")
      val hexNumeral = P("0" ~ ("x" | "X") ~ hexDigit ~ (hexDigit | "_").rep)
    }

    object OctalNumeral {
      /*
          OctalNumeral:
              0 OctalDigits
              0 Underscores OctalDigits

          OctalDigits:
              OctalDigit
              OctalDigit OctalDigitsAndUnderscoresopt OctalDigit

          OctalDigit: one of
              0 1 2 3 4 5 6 7

          OctalDigitsAndUnderscores:
              OctalDigitOrUnderscore
              OctalDigitsAndUnderscores OctalDigitOrUnderscore

          OctalDigitOrUnderscore:
              OctalDigit
              _
       */
      val octalDigit = CharIn("01234567")
      val octalNumeral = P("0" ~ (octalDigit | "_").rep(min=1))
    }

    object BinaryNumeral {
      /*
      BinaryNumeral:
          0 b BinaryDigits
          0 B BinaryDigits

      BinaryDigits:
          BinaryDigit
          BinaryDigit BinaryDigitsAndUnderscoresopt BinaryDigit

      BinaryDigit: one of
          0 1

      BinaryDigitsAndUnderscores:
          BinaryDigitOrUnderscore
          BinaryDigitsAndUnderscores BinaryDigitOrUnderscore

      BinaryDigitOrUnderscore:
          BinaryDigit
          _
       */
      val binaryDigit = CharIn("01")
      val binaryNumeral = P("0" ~ ("b" | "B") ~ binaryDigit ~ (binaryDigit | "_").rep)
    }
    val parser = binaryIntegerLiteral | octalIntegerLiteral | hexIntegerLiteral | decimalLiteral //parsers are in reverse order, as they must fail first
  }

  object FloatingPointLiteral extends Token {
    object DecimalFloatingPointLiteral extends Token {
      /*
      DecimalFloatingPointLiteral:
          Digits . Digitsopt ExponentPartopt FloatTypeSuffixopt
          . Digits ExponentPartopt FloatTypeSuffixopt
          Digits ExponentPart FloatTypeSuffixopt
          Digits ExponentPartopt FloatTypeSuffix

      ExponentPart:
          ExponentIndicator SignedInteger

      ExponentIndicator: one of
          e E

      SignedInteger:
          Signopt Digits

      Sign: one of
          + -

      FloatTypeSuffix: one of
          f F d D
      */
      val exponentIndicator = P("e" | "E")
      val sign = P("-" | "+")
      val floatTypeSuffix = CharIn("fFdD")
      val digits = IntegerLiteral.DecimalNumeral.digits
      val dot = P(".")
      val signedInt = P(sign.? ~ digits)
      val parser = P(((dot ~ digits) | (digits ~ dot.?)) ~ (digits | (exponentIndicator ~ signedInt) | floatTypeSuffix ).rep)
    }

    object HexidecimalFloatingPointLiteral extends Token {
      /*
          HexadecimalFloatingPointLiteral:
              HexSignificand BinaryExponent FloatTypeSuffixopt

          HexSignificand:
              HexNumeral
              HexNumeral .
              0 x HexDigitsopt . HexDigits
              0 X HexDigitsopt . HexDigits

          BinaryExponent:
              BinaryExponentIndicator SignedInteger

          BinaryExponentIndicator:one of
              p P
       */
      val dot = P(".")
      val hexNumeral = IntegerLiteral.HexNumeral.hexNumeral
      val hexDigit = IntegerLiteral.HexNumeral.hexDigit
      val signedInt = FloatingPointLiteral.DecimalFloatingPointLiteral.signedInt
      val significand = P(hexNumeral | hexNumeral ~ dot | "0" ~ ("x" | "X") ~ hexDigit.?.rep ~ "." ~ hexDigit.rep)
      val parser = P(significand ~ (("P" | "p") ~ signedInt).? ~ FloatingPointLiteral.DecimalFloatingPointLiteral.floatTypeSuffix.?)
    }

    val parser = HexidecimalFloatingPointLiteral.parser | DecimalFloatingPointLiteral.parser
  }

  object BooleanLiteral extends Token {
    val parser = P("true" | "false")
  }

  object EscapeSequence extends Token {
    /*
        EscapeSequence:
            \ b    /* \u0008: backspace BS */
            \ t    /* \u0009: horizontal tab HT */
            \ n    /* \u000a: linefeed LF */
            \ f    /* \u000c: form feed FF */
            \ r    /* \u000d: carriage return CR */
            \ "    /* \u0022: double quote " */
            \ '    /* \u0027: single quote ' */
            \ \              /* \u005c: backslash \ */
            OctalEscape        /* \u0000 to \u00ff: from octal value */

        OctalEscape:
            \ OctalDigit
            \ OctalDigit OctalDigit
            \ ZeroToThree OctalDigit OctalDigit

        OctalDigit: one of
            0 1 2 3 4 5 6 7

        ZeroToThree: one of
            0 1 2 3
     */
    val slash = P("\\")
    val octalEscape = P(slash ~ OctalNumeral.octalDigit | OctalNumeral.octalDigit ~ OctalNumeral.octalDigit | CharIn("0123") ~ OctalNumeral.octalDigit ~ OctalNumeral.octalDigit)
    val parser = P(
      slash ~ "b" |
      slash ~ "t" |
      slash ~ "n" |
      slash ~ "f" |
      slash ~ "r" |
      slash ~ "\"" |
      slash ~ "'" |
      slash ~ "\\" |
      octalEscape
    )
  }

  object CharacterLiteral extends Token {
    /*
        CharacterLiteral:
              ' SingleCharacter '
              ' EscapeSequence '

          SingleCharacter:
              InputCharacter but not ' or \
     */

    val singleCharacter = !("'" | "\\") ~ UnicodeEscape.unicodeInputCharacter
//    val singleCharacter =  UnicodeEscape.unicodeInputCharacter
    val parser =  P("'" ~ EscapeSequence.parser ~ "'" | "'" ~ singleCharacter ~ "'" | "'" ~ UnicodeEscape.unicodeInputCharacter ~ "'" | "'" ~ "\\" ~ CharsWhile("0123456789".contains(_)) ~ "'")
  }

  object StringLiteral extends Token {
    /*
    StringLiteral:
        " StringCharactersopt "

    StringCharacters:
        StringCharacter
        StringCharacters StringCharacter

    StringCharacter:
        InputCharacter but not " or \
        EscapeSequence
     */
    val stringChar = P(!("\"" | "\\") ~ UnicodeEscape.unicodeInputCharacter | EscapeSequence.parser)
    val parser = P("\"" ~ stringChar.rep ~ "\"")
  }

  object NullLiteral extends Token {
    val parser = P("null")
  }

  object Separators {
    val separators = "(){}[];,."
  }

  object Operators {
    val operators = Seq(
      "=", ">", "<", "!", "~", "?", ":",
      "==",  "<=",  ">=",  "!=",  "&&",  "||",  "++",  "--",
      "+",   "-", "*", "/",   "&",   "|",   "^",   "%",   "<<",   ">>",   ">>>",
      "+=",  "-=",  "*=",  "/=",  "&=",  "|=",  "^=",  "%=",  "<<=",  ">>=" , ">>>="
    )
  }

  println(P(IntegerLiteral.parser ~ End).!.parse("0b0_1_0"))
  println(P(IntegerLiteral.parser ~ End).!.parse("0_01234567"))
  println(P(IntegerLiteral.parser ~ End).!.parse("0xDEADB__E_EFL"))
  println(P(IntegerLiteral.parser ~ End).!.parse("0b0_1"))
  println(P(IntegerLiteral.parser ~ End).!.parse("1"))
  println(P(IntegerLiteral.parser ~ End).!.parse("15"))

  println(P(FloatingPointLiteral.parser ~ End).!.parse("6.022137e+23f"))
  println(P(FloatingPointLiteral.parser ~ End).!.parse("3.14f"))
  println(P(FloatingPointLiteral.parser ~ End).!.parse("0f"))
  println(P(FloatingPointLiteral.parser ~ End).!.parse(".3f"))
  println(P(FloatingPointLiteral.parser ~ End).!.parse("2.f"))
  println(P(FloatingPointLiteral.parser ~ End).!.parse("1e1f"))

  println(P(CharacterLiteral.parser ~ End).!.parse("'a'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'%'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'\\t'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'\\\\'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'\\''"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'\\u03a9'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'\\uFFFF'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'\\177'"))
  println(P(CharacterLiteral.parser ~ End).!.parse("'Ω'"))

  println(P(StringLiteral.parser ~ End).!.parse("\"\""))
  println(P(StringLiteral.parser ~ End).!.parse("\"\\\"\""))
  println(P(StringLiteral.parser ~ End).!.parse("\"This is a string\""))

//  println ((Comment.parser ~ End).parse("/* this comment /* // /** ends here: */"))
//  println (Comment.parser.parse("// this comment /* // /** ends here: */"))

  println ((Identifiers.parser ).parse("fornew"))


  // the following implements chapter 4 of the java language specification: http://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html

  object Type extends Token {
    val identifier = Identifiers.identifier
    val dot = P(".")

    val primitives = StringIn("byte", "short", "int", "long", "char", "float", "double", "boolean")
    val typeName = P(identifier ~ (dot ~ identifier).?).rep
    val classOrInterfaceType = P(typeName ~ TypeArguments.typeArguments.?)
    val typeVariable = P(!typeName ~ identifier)
    val ref : P[_] = P(classOrInterfaceType | typeVariable | (primitives | ref) ~ "[" ~ "]")
//    val parser : P[_] = ReferenceType.parser | PrimitiveType.parser
    val parser : P[_] = ref | primitives
  }
  object PrimitiveType extends Token {
    val parser = StringIn("byte", "short", "int", "long", "char", "float", "double", "boolean")
  }

  object ReferenceType extends Token {
    /*
      ReferenceType:
          ClassOrInterfaceType
          TypeVariable
          ArrayType

//      ClassOrInterfaceType:
//          ClassType
//          InterfaceType
//
//      ClassType:
//          TypeDeclSpecifier TypeArgumentsopt
//
//      InterfaceType:
//          TypeDeclSpecifier TypeArgumentsopt
//
//      TypeDeclSpecifier:
//          TypeName
//          ClassOrInterfaceType . Identifier
//
//      TypeName:
//          Identifier
//          TypeName . Identifier

      TypeVariable:
          Identifier

      ArrayType:
          Type [ ]
     */
    val identifier = Identifiers.identifier
    val dot = P(".")
//    val typeName : P[_] = P(identifier | typeName ~ dot ~ identifier)
//    val typeDeclSpecifier : P[_] = P(typeName | classOrInterfaceType ~ dot ~ identifier)
//    val classOrInterfaceType = P(classType | interfaceType)
//    val classType = P(typeDeclSpecifier ~ TypeArguments.typeArguments.?)
//    val interfaceType = P(typeDeclSpecifier ~ TypeArguments.typeArguments.?)
//    val typeVariable = identifier
//    val arrayType = P(Type.parser ~ "[" ~ "]")
    val parser = P(classOrInterfaceType | typeVariable | arrayType)
    val typeName = P (identifier ~ (dot ~ identifier).rep).log()
    val classOrInterfaceType = P(typeName ~ TypeArguments.typeArguments.rep).log()
    val typeVariable = identifier.log()
    val arrayType = P(Type.parser ~ "[" ~ "]").log()
//    val parser = P(arrayType | typeVariable | classOrInterfaceType).log()
  }

  object TypeArguments {
    /*
        TypeArguments:
            < TypeArgumentList >

        TypeArgumentList:
            TypeArgument
            TypeArgumentList , TypeArgument

        TypeArgument:
            ReferenceType
            Wildcard

        Wildcard:
            ? WildcardBoundsopt

        WildcardBounds:
            extends ReferenceType
            super ReferenceType

     */
    val wildCardBounds = P("extends" ~ ReferenceType.parser | "super" ~ ReferenceType.parser)
    val wildCard : P[_] = P("?" ~ wildCardBounds.?)
    val typeArgument = P(ReferenceType.parser | wildCard)
    val typeArgumentList : P[_]= P(typeArgument | typeArgumentList ~ "," ~ typeArgument)
    val typeArguments = P("<" ~ typeArgumentList ~ ">")
  }

  println(P(Type.parser ~ End).log().!.parse("int[]"))
  println(P(Type.parser ~ End).log().!.parse("GenericOuter.Inner<?>"))
  println(P(Type.parser ~ End).log().!.parse("GenericOuter<Integer>"))
  println(P(Type.parser ~ End).log().!.parse("GenericOuter<Integer>.Inner"))
  println(P(Type.parser ~ End).log().!.parse("GenericOuter<Integer>.Inner<Double>"))
}
