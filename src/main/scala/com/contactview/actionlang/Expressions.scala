package com.contactview.actionlang

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/28/16
  * ************************************************************************/


object Expressions {
  import fastparse.noApi._
  object  WsApi extends fastparse.WhitespaceApi.Wrapper(Lexical.wscomment)
  import WsApi._

  def op[T](s: P0, rhs: T) = s.!.map(_ => rhs)
  val Lt = op("<", Ast.compareop.Lt)
  val Gt = op(">", Ast.compareop.Gt)
  val Eq = op("==", Ast.compareop.Eq)
  val GtE = op(">=", Ast.compareop.GtE)
  val LtE = op("<=", Ast.compareop.LtE)
  val NotEq = op( "!=", Ast.compareop.NotEq)
  val comp_op = P( LtE|GtE|Eq|Gt|Lt|NotEq)
  val Add = op("+", Ast.operator.Add)
  val Sub = op("-", Ast.operator.Sub)
  val Pow = op("^", Ast.operator.Pow)
  val Mul= op("*", Ast.operator.Mul)
  val Div = op("/", Ast.operator.Div)
  val Mod = op("%", Ast.operator.Mod)


  def Chain(p: P[Ast.Expr], op: P[Ast.operator]) = P( p ~ (op ~ p).rep ).map {
    case (lhs, chunks) =>
      chunks.foldLeft(lhs) { case (_lhs, (_op, _rhs)) => Ast.expr.BinOp(_lhs, _op, _rhs) }
  }

  def parseExpression(expression : String, context : Context.Context) : Parsed[Ast.Expr] = {
    val variables : Seq[Context.Variable] = context.variables

    val NAME: P[Ast.Identifier] = Lexical.identifier
    val STRING = Lexical.stringliteral
    val NUMBER = P( Lexical.floatnumber | Lexical.longinteger | Lexical.integer | Lexical.imagnumber ).!.map(Ast.Number)

    /*
      atom is the base level identifier. It needs to recognize variable names, literals and built ins.
      it is the base for expression parsing.
     */
    def atom : P[Ast.Expr] = {
      val attr = P("." ~ NAME ~ (" " | ";" | End))
      val call = P("." ~ NAME ~ "(" ~ test.rep(sep = ",") ~ ")")
      val OBJREF = P(NAME.rep(1, sep = !(call | attr) ~ ".").map {v => v.map(_.name).mkString(".") }.map(Ast.Identifier).map(Ast.expr.ObjRef))//  ~ trailers).map{v => v._2(v._1) }
      val variableParser : Parser[Ast.Expr] = P(StringIn(variables.map(_.name):_*)).!.map(Ast.Reference)
      val lengthOfParser : Parser[Ast.Expr] = P("length of " ~/ atom).map(Ast.LengthOf)
      val loggedInParser : Parser[Ast.Expr] = P("number of agents logged in to " ~/ atom).map(Ast.LoggedInAgents)

      (("(" ~ test ~ ")" |
//        "{" ~ test.rep(sep = ";").map(Ast.expr.Block) ~ "}" | //works but commenting out because current DSL has no use for expression blocks
        variableParser | lengthOfParser | loggedInParser |
        STRING.rep(1).map(_.mkString).map(Ast.String) |
        OBJREF |
        NUMBER) ~ trailers.rep.?).map {
        case (lhs, None) => lhs
        case (lhs, Some(rhs)) => rhs.foldLeft(lhs) { case (l, r) => r(l) }
      }
    }
    /*
      following any atomic type can be either a named attribute such as ".length" or a method call, such as ".toString()"
      these trailers take a left hand side (derrived from the atomic type above)
     */
    def trailers : Parser[Ast.Expr => Ast.Expr] = {
      val attribute = P("." ~ NAME).map(id => (lhs : Ast.Expr) => Ast.expr.Attribute(lhs, id))
      val call = P("(" ~ test.rep(sep = ",") ~ ")").map(args => (lhs : Ast.Expr) => Ast.expr.Call(lhs, args))
      P (attribute | call)
    }
    /*
      with our basic atomic type we can begin to move into order of operations parsing
      we first start with Exponents/Power
     */
    // its important to note that factor is forward defined, and both cross reference each other
    def power : Parser[Ast.Expr] = P (atom ~ trailers.rep ~ (Pow ~ factor).?).map {
      case (lhs, _trailers, rhs) =>
        val left = _trailers.foldLeft(lhs) { case (l,r) => r(l) }
        rhs match {
          case None => left //if no right hand side (power + factor) then return only the left
          case Some((op, _rhs)) => Ast.expr.BinOp(left, op, _rhs)
        }
    }
    // it's important to understand that the power expression neither needs the trailer to match (can be empty returning only the atom)
    // now to define factor
    def factor : Parser[Ast.Expr] = P ( ("+" | "-") ~ factor | power )
    /*
      we parse in top down fashion, power parser at the very end will identify a basic atom type if nothing else is identified
      now we parse multiplication, division and modulo
     */
    def term : Parser[Ast.Expr] = P( Chain(factor, Mul | Div | Mod) )
    /*
      finally we parse addition and subtraction
     */
    def arith_expr : Parser[Ast.Expr] = P( Chain(term, Add | Sub) )
    /*
      if we choose to extend math parsing we an do so here, for now we have satisfied our needs
      arith_expr is our top most parser
     */
    def expr : Parser[Ast.Expr] = arith_expr

    /*
      now the expr parser can parse basic math operations, folding them together to form a single expression representing
      the order of operations we defined: atom -> factor -> mul,div,mod -> add,sub
     */
    /*
      we can begin to parse more complete expressions by parsing comparisons
      we support the the general comparisons along with && and ||
      we will start the chain first with the general comparisons
     */
    def comparison : Parser[Ast.Expr] = P( expr ~ (comp_op ~ expr).rep ).map {
      case (lhs, Nil) => lhs //this allows us to test for simple expressions that have no comparisons or math operations
      case (lhs, chunks) =>
        chunks.foldLeft(lhs) { case (l, (op, r)) => Ast.expr.Compare(l, op, r) }
    }
    /*
      now to support the && comparison
     */
    def and_test : Parser[Ast.Expr] = P( comparison.rep(min = 1, sep = "&&") ).map {
      case Seq(x) => x
      case xs => xs.tail.foldLeft(xs.head) { case (l, r) => Ast.expr.BoolOp(l, Ast.boolop.And, r) }
    }
    /*
      now do the same thing for ||
     */
    def or_test : Parser[Ast.Expr] = P( and_test.rep(min = 1, sep = "||") ).map {
      case Seq(x) => x
      case xs => xs.tail.foldLeft(xs.head) { case (l, r) => Ast.expr.BoolOp(l, Ast.boolop.Or, r) }
    }
    /*
      our tests are now complete, we should have a parser that can evaluate an expression like: 3+2==4-1
     */
    def test : Parser[Ast.Expr] = or_test

    test.parse(expression)
  }

}
