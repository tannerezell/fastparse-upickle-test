package com.contactview.actionlang

import upickle.Js

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/28/16
  * ************************************************************************/


object Ast {

  case class Identifier(name : java.lang.String)

  sealed trait BaseExpr
  sealed trait Expr extends BaseExpr
  sealed trait Literal extends Expr

  case class Reference(name : java.lang.String) extends Expr

  sealed trait BaseSupportedType

  // base types supported
  case class Number(value : java.lang.String) extends Literal with BaseSupportedType
  case class Prompt(value : Expr) extends Literal with BaseSupportedType
  case class String(value : java.lang.String) extends Literal with BaseSupportedType

  object expr {
    case class Compare(lhs : Expr, op : compareop, rhs : Expr) extends Expr
    case class BinOp(lhs : Expr, op : operator, rhs : Expr) extends Expr
    case class BoolOp(lhs : Expr, op : boolop, rhs : Expr) extends Expr
    case class Attribute(value : Expr, id : Identifier) extends Expr
    case class Call(func : Expr, args : Seq[Expr]) extends Expr
    case class ObjRef(obj : Identifier) extends Expr
    case class Block(stmt : Seq[Expr]) extends Expr
  }

  sealed trait compareop extends BaseExpr
  object compareop {
    case object Lt extends compareop
    case object LtE extends compareop
    case object Gt extends compareop
    case object GtE extends compareop
    case object Eq extends compareop
    case object NotEq extends compareop
  }

  sealed trait operator extends BaseExpr
  object operator {
    case object Add extends operator
    case object Sub extends operator
    case object Mul extends operator
    case object Div extends operator
    case object Pow extends operator
    case object Mod extends operator
  }

  sealed trait boolop extends BaseExpr
  object boolop {
    case object And extends boolop
    case object Or extends boolop
  }

  // built in/inline actions
  case class LoggedInAgents(expr : Expr) extends Expr

  // built in helpers
  case class LengthOf(expr : Expr) extends Expr
}
