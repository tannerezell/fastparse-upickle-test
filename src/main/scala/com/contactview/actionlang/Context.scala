package com.contactview.actionlang

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/28/16
  * ************************************************************************/


object Context {

  case class Variable(name : String)
  case class Context(variables : Seq[Variable])
}
