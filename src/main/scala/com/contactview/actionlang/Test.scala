package com.contactview.actionlang

import scala.scalajs.js.annotation.JSExport

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/28/16
  * ************************************************************************/

@JSExport("Test")
object Test extends App {
  import fastparse.all._

  @JSExport
  def runTest() = {
    val str = Lexical.identifier

    val nameId = P(str.rep(sep = !".toString()" ~ ".").!)
    println (nameId.parse("com.ctilogic.Object.toString()"))


    val ctx = Context.Context(Seq(Context.Variable("my int")))
    println (Expressions.parseExpression("3", ctx))
    println (Expressions.parseExpression("3.toString", ctx))
    println (Expressions.parseExpression("3.toString(5.hi(\"bye\"))", ctx))
    println (Expressions.parseExpression("number of agents logged in to 3.toString()", ctx))
    println (upickle.default.write(Expressions.parseExpression("5 + number of agents logged in to (com.ctilogic.Queue + \"hello\")", ctx).get))
    println (Expressions.parseExpression("com.ctilogic.Object.helloWorld(\"foobar\")", ctx))
    println (Expressions.parseExpression("( 3 + 2 == 4 - 1 && 4 == 1 ).length(length of 5)", ctx))
  }
}
