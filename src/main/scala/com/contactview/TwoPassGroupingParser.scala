package com.contactview

import java.util.UUID

import scala.collection.mutable.ListBuffer

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/25/16
  * ************************************************************************/


object TwoPassGroupingParser extends App {
  import fastparse.all._

  val space = P ( CharsWhile(" ".contains(_: Char)).? )
  val whitespace = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))

  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )

  def strChars(exclude : String = "\"\\") = P( CharsWhile(!exclude.contains(_: Char)))
  val string = P( space ~ "\"" ~/ (strChars() | escape).rep.! ~ "\"")

  sealed trait Expr {
    def className : String
  }
  sealed trait OperatorIntermediate extends Expr {
    def className = "OperatorIntermediateAst"
  }
  sealed trait Operator extends OperatorIntermediate {
    def left : Expr
    def right : Expr
    override def className : String = right.className
  }
  sealed trait ComparisonOperator extends Operator {
    override def className = BaseClassTypes.Boolean
  }
  case class Literal(value : String, className : String) extends Expr
  case class Reference(id : UUID, className : String) extends Expr
  case class Cast(expr : Expr, className : String) extends Expr
  case class Grouping(expr : Expr) extends Expr { def className = expr.className }
  case class ExprEnd() extends Expr { def className = "end" }

  case object ParenLImd extends OperatorIntermediate
  case object ParenRImd extends OperatorIntermediate
  case object AddOpImd extends OperatorIntermediate
  case object SubOpImd extends OperatorIntermediate
  case object DivOpImd extends OperatorIntermediate
  case object MulOpImd extends OperatorIntermediate
  case object ModOpImd extends OperatorIntermediate
  case object CmpOpImd extends OperatorIntermediate
  case object GtOpImd  extends OperatorIntermediate
  case object GtEOpImd extends OperatorIntermediate
  case object LtOpImd  extends OperatorIntermediate
  case object LtEOpImd extends OperatorIntermediate

  case class AddOp(left : Expr, right : Expr) extends Operator
  case class SubOp(left : Expr, right : Expr) extends Operator
  case class DivOp(left : Expr, right : Expr) extends Operator
  case class MulOp(left : Expr, right : Expr) extends Operator
  case class ModOp(left : Expr, right : Expr) extends Operator
  case class CmpOp(left : Expr, right : Expr) extends Operator
  case class GtOp (left : Expr, right : Expr) extends ComparisonOperator
  case class GtEOp(left : Expr, right : Expr) extends ComparisonOperator
  case class LtOp (left : Expr, right : Expr) extends ComparisonOperator
  case class LtEOp(left : Expr, right : Expr) extends ComparisonOperator

  case class ActionParser(parser : (ParserContext) => Parser[Expr], className : String)
  trait ParserContext {
//    def parsers : Seq[ActionParser]
    def typeParsers : Seq[ClassTypeParser] = Seq(String, Int)
    def baseParsers : Parser[Expr] = combineParsers(typeParsers.map(_.literalParser) ++ OperatorParsers.baseParsers)

    private def combineParsers(parsers : Seq[Parser[Expr]]) = parsers.tail.foldLeft(parsers.head) { case (l, r) => l | r }

    def parse(expression : String) : List[Parsed[Expr]] = {
      val parsedTokens = ListBuffer.empty[Parsed[Expr]]
      val endParser = P(End.opaque("end of expression")).map(v => ExprEnd())
      def getClassTypeParserForExpr(expr : Expr) : Option[ClassTypeParser] = expr match {
        case se : Expr => typeParsers.find(_.className == se.className)
      }
      def parseExpression(expression : String, index : Int = 0) : Parsed[Expr] = {

        val parseResult : Parsed[Expr] = P(baseParsers | endParser).parse(expression, index)
        parseResult match {
          case Parsed.Success(value : ExprEnd, _index) => parseResult
          case Parsed.Success(value : Expr, _index) =>
            parsedTokens += parseResult
            parseExpression(expression, _index)
          case failure : Parsed.Failure =>
            parsedTokens += parseResult
            failure
        }
      }
      parseExpression(expression, 0)
      parsedTokens.toList
    }
  }

  object OperatorParsers {
    private def opParser(op : String) : Parser[Unit] = P(space ~ op ~ space).opaque(op)
    val addParser : Parser[Expr] = opParser("+").map(v => AddOpImd)
    val subParser : Parser[Expr] = opParser("-").map(v => SubOpImd)
    val parenL : Parser[Expr] = opParser("(").map(v => ParenLImd)
    val parenR : Parser[Expr] = opParser(")").map(v => ParenRImd)

    private def combineParsers(parsers : Seq[Parser[Expr]]) = parsers.tail.foldLeft(parsers.head) { case (l, r) => l | r }

//    def baseParsers : Seq[Parser[Expr]] = Seq(addParser, subParser, parenL, parenR)
    def baseParsers : Seq[Parser[Expr]] = Seq(addParser, subParser)
    def parsers = combineParsers(baseParsers)

  }

  object BaseClassTypes {
    final val String = "java.lang.String"
    final val Integer = "java.lang.Integer"
    final val Boolean = "java.lang.Boolean"
  }

  // type parsers should be the parsers for any type of data type, such as String or Int
  // a type parser needs to be able to define the valid operations for a generic type
  // such as +, -, ==, etc.
  // it should be called when this type has been identified
  trait ClassTypeParser {
    def className : String
//    def opParser(left : Expr)(implicit ctx : ParserContext) : Parser[Expr]
    def literalParser : Parser[Expr]
    def actionLiteralParser = ActionParser((ctx) => literalParser, className)
//    def opParser(op : String, parser : Parser[Expr]) : Parser[Expr] = P(space ~ op ~ space ~ parser).opaque(op)
    def cast(expr : Expr) : Expr = if (expr.className == className) expr else Cast(expr, className)
  }

//  implicit def ctp2p(ctp : ClassTypeParser)(implicit ctx : ParserContext) = ctx.getParserByClassName(ctp.className)

  case object String extends ClassTypeParser {
    override def className: String = BaseClassTypes.String

    override def literalParser: Parser[Expr] = P(string).map(Literal(_, className))
  }

  case object Int extends ClassTypeParser {
    override def className: String = BaseClassTypes.Integer

    override def literalParser = P(digits).!.map(v => Literal(v, className))

  }

  case class UccxActionContext() extends ParserContext
  val ctx = UccxActionContext()

  def baseParsers  = {
    val tempBase = ctx.baseParsers
    val parenBase = P (space ~ "(" ~/ space ~ tempBase ~ ")" ~ space).!
    val exprBase = (tempBase | parenBase).rep
    exprBase.!
  }

  println (baseParsers.parse("\"hello\" + (3 + (\"world\" + \"foo\"))"))
//  val phase1 = ctx.parse("\"hello\" + (3 + (\"world\" + \"foo\"))")
//  println (phase1)
//  println (extractGroups(phase1))

  case class GroupingImd(parts : List[Parsed[Expr]]) extends Expr { def className = "GroupingImdAst" }
  // run through each part of phase1 results, when a ParenL is found, all a subsequent parts will be grouped under it.
  // when a ParenR is found, return all parts as a GroupingImd(parts)

  // this works but does not allow us for clean validation
  def extractGroups(parts : List[Parsed[Expr]], index : Int = 0) : List[Parsed[Expr]] = {
    val result = ListBuffer.empty[Parsed[Expr]]
    for (i <- index until parts.length) {
      parts(i) match {
        case Parsed.Success(ParenLImd, _index) =>
          result += Parsed.Success(GroupingImd(extractGroups(parts, i + 1)), _index)
          return result.toList
        case Parsed.Success(ParenRImd, _index) => return result.toList
        case Parsed.Success(_, _index) => result += parts(i)
      }
    }
    // no good way to indicate end of list but missing parenR (when parsing in a parenL)
    result.toList
  }
}
