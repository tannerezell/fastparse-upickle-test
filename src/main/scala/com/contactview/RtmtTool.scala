package com.contactview

import java.io.{BufferedReader, InputStreamReader}
import java.lang.reflect.Field
import java.net.URL
import javax.security.auth.login.LoginContext

import com.cisco.ccm.serviceability.rtmt.login.rtmtlogin.RtmtLogin
import com.cisco.ccm.serviceability.rtmt.tct.{ConnectionHandler, TraceUtil}
import com.cisco.ccm.serviceability.rtmt.ui.UrlFactory
import fastparse.core.Parsed

import scala.xml.XML

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : rtmt-log-test
  * Date    : 5/1/15
  * ************************************************************************/


object RtmtTool extends App {

  System.setProperty("java.awt.headless", "true")
  login()

  def login() : Unit = {
    SecurityBypasser.destroyAllSSLSecurityForTheEntireVMForever()
    val username = "administrator"
    val password = "1ptC1sc0!"
    val ipaddress = "10.1.10.14"
    val port = 8443
    val useSSL = true
    UrlFactory.setBase64Authentication(username, password)


//    val serviceName = "Cisco Unified CCX Engine"
    val serviceName = "Cisco Unified CCX Administration"
    val serviceType = "log"
    val query = s"""xmlQuery=<?xml version="1.0"?><Query><JobType>RealTimeFileMon</JobType><svcName>$serviceName</svcName><TraceType>$serviceType</TraceType></Query>"""

    val onDemandUrl = "/tracecollection/MainServlet.class?htxtFunctionName=ServiceabilityQueryController&OnDemandSubType=Browse"

    val connectionHandler = ConnectionHandler.getInstance()
    val urlConnection = connectionHandler.getURLConnection(s"https://$ipaddress:$port$onDemandUrl")
    UrlFactory.setAuthenticationHeader(urlConnection)

    val localOutputStream = TraceUtil.handleCookie4URLOutputConnection(urlConnection)
    localOutputStream.write(query.getBytes("UTF-8"))
    localOutputStream.flush()

    val localInputStream = urlConnection.getInputStream

    TraceUtil.storeCookies(urlConnection)

    var runLoop = true
    val stringBuffer = new StringBuffer()
    while (runLoop) {
      val i = localInputStream.read()
      if (i == -1) runLoop = false
      else stringBuffer.append(i.toChar)
    }
    println ("raw response is " + stringBuffer.toString)

    val xmlResponse = XML.loadString(stringBuffer.toString.trim)

    println ("response:\n" + xmlResponse)

    val taskId = (xmlResponse \ "ID").text

    println ("taskId = " + taskId)

    val commandUrl = s"cmd=status&schId=$taskId&"
    val resultUrl = s"cmd=result&schId=$taskId&"
    val statusUrl = "/tracecollection/MainServlet.class?htxtFunctionName=ServiceabilityStatusController"


    val statusBuffer = new StringBuffer()

    val parser = new FileSyncParser()

    var statusLine = ""
    var counterTillDeath = 300
    while (counterTillDeath > 0) {

      val statusUrlConnection = connectionHandler.getURLConnection(s"https://$ipaddress:$port$statusUrl")
      UrlFactory.setAuthenticationHeader(statusUrlConnection)

      val statusOS = TraceUtil.handleCookie4URLOutputConnection(statusUrlConnection)

      statusOS.write(resultUrl.getBytes("UTF-8"))

      val statusReader = new BufferedReader(new InputStreamReader(statusUrlConnection.getInputStream))
      var line = ""
      val buffer = new StringBuffer()
      while (line != null) {
        line = statusReader.readLine()
        if (line != null) buffer.append(line + "\n")
      }
//      println (buffer)
//      val xml = XML.loadString(buffer.toString)
//      val content = (xml \ "RTFileOutput").text
      val content = buffer.toString.replaceAll("<?xml version=\"1.0\"?>", "").replaceAll("<RTFileOutput>", "").replaceAll("</RTFileOutput>", "")
//      println (s"sending content for parsing $content")
      if (content.nonEmpty) {
        parser.parser.parse(content) match {
          case Parsed.Success(value, _) =>
            val collected = {
              value.collect {
                case fc : FileChangedOnUCCX => fc
                case fc : FileDeletedOnUCCX => fc
                case fc : FileRenamedOnUCCX => fc
              }
            }
            println (s"collected the following logs:")
            collected.foreach(println)
          case f : Parsed.Failure[_, _] =>
        }
      }
      statusReader.close()
      statusOS.close()
      counterTillDeath -= 1
      Thread.sleep(1000L)
      //      if (statusLine != null) println ("read line: " + statusLine)
    }

    println (s"statusLine=$statusLine and counterTillDeath=$counterTillDeath")
    val cancelCmd = s"cmd=cancel&schId=$taskId&"

    val statusUrlConnection2 = connectionHandler.getURLConnection(s"https://$ipaddress:$port$statusUrl")
    UrlFactory.setAuthenticationHeader(statusUrlConnection2)

    val statusOS2 = TraceUtil.handleCookie4URLOutputConnection(statusUrlConnection2)
    statusOS2.write(cancelCmd.getBytes("UTF-8"))
    statusOS2.flush()

    val response = statusUrlConnection2.getInputStream

    runLoop = true
    stringBuffer.delete(0, stringBuffer.length())
    while (runLoop) {
      val i = response.read()
      if (i == -1) runLoop = false
      stringBuffer.append(i.toChar)
    }

    println ("cancel response: " + stringBuffer.toString)


    localInputStream.close()
    //    statusReader.close()
    localOutputStream.close()
    //    statusOS.close()
    statusOS2.close()

  }

  def setFieldValue(fieldName : String, obj : AnyRef, value : AnyRef) = {
    val field = getReflectedField(obj, fieldName)
    field.set(obj, value)
  }
  def getReflectedField(obj : Object, fieldName : String) : Field = {
    val field = obj.getClass.getDeclaredField(fieldName)
    field.setAccessible(true)
    field
  }
}

object SecurityBypasser {
  import java.security.cert.X509Certificate
  import javax.net.ssl._


  // The all-permisive trust manager.
  object AllTM extends X509TrustManager {
    def getAcceptedIssuers(): Array[X509Certificate] = null
    def checkClientTrusted(certs: Array[X509Certificate], authType: String) {}
    def checkServerTrusted(certs: Array[X509Certificate], authType: String) {}
  }v


  // The all-permissive hostname verifier.
  object AllHosts extends HostnameVerifier {
    def verify(urlHostName: String, session: SSLSession) = true
  }


  def destroyAllSSLSecurityForTheEntireVMForever() {
    val trustAllCerts = Array[TrustManager](AllTM)
    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, trustAllCerts, null);
    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory())
    HttpsURLConnection.setDefaultHostnameVerifier(AllHosts);
  }
}