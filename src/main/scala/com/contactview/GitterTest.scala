package com.contactview

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/30/16
  * ************************************************************************/


object GitterTest extends App {
  import fastparse.all._

  // Sample texts
  val txtList =
    """* a1 list item
      |* another list item
      |* another list item
      |* another list item
      |
      |Other text following blank-line end-of-list
      |another annoying line
      |* and now a new list
      |* with this second item""".stripMargin

  val txtListFollowingNLs =
    """
      |
      |* a2 list item
      |* another list item
      |
      |* this begins a 2nd list
      |* because of the blank line""".stripMargin

  case class List(listItems : Seq[ListItem])
  case class ListItem(id : String, str : String)
  case class TextLine(text : String)

  def strChars(exclude : String = "\n") = P( CharsWhile(!exclude.contains(_: Char)))

  val ws = P(CharsWhile("\n\t".contains(_)))

  val list = P("*".! ~ strChars("\n").! ~ "\n".?).map(v => ListItem(v._1, v._2.trim())).rep.map(List).filter(_.listItems.nonEmpty)
  val textLine = strChars().!.map(TextLine)
  val listParser = P(Start ~ (ws.? ~ list | textLine).rep(sep="\n\n" | "\n") ~ End)

  println (listParser.parse(txtListFollowingNLs))
  println (listParser.parse(txtList))

}
