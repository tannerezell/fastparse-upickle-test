package com.contactview

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/25/16
  * ************************************************************************/


object AstParserTest extends App {

  import fastparse.all._

  trait Expr

  case class Literal(value : Any) extends Expr
  case class Unary(unary : String, expr : Expr) extends Expr
  case class BinOp(lhs : Expr, op : String, rhs : Expr) extends Expr

  type RParser = P[Expr] => P[Expr]

  val numbers = CharIn('0' to '9')

  val integer = numbers.rep(1).!.map(Literal)

  val atom = integer
//  val unary = (("+" | "-").! ~ level2).map(v => Unary(v._1, v._2)) | atom


  val mul_div : P[Expr] = P(atom ~ (("*" | "/").! ~/ factor).?).map {
    case (lhs, Some((op, rhs))) =>
      BinOp(lhs, op, rhs)
    case (lhs, None) => lhs
  }

  val factor : P[Expr] = P( (("+" | "-").! ~ factor).map(v => Unary(v._1, v._2)) | mul_div)

  val mul : RParser = (p) => P(atom ~ (("*" | "/").! ~/ p).?).map {
    case (lhs, rhs) =>
      rhs match {
        case Some((op, rhs)) => BinOp(lhs, op, rhs)
        case None => lhs
      }
  }

  val fact : RParser = (p) => P(  (("+" | "-").! ~ fact(p) ~/).map(v => Unary(v._1, v._2)) | p)



  val parser : P[Expr] = {
    val base = atom | parser
    val level1 = fact(base)
    val level2 = mul(level1)
    val level3 = fact(level2)
    val level4 = mul(level3)
    level4
  }

  val levels : Map[Int, RParser] = Map (
    1 -> ((p : P[Expr]) => fact(p)),
    2 -> ((p : P[Expr]) => mul(p))
  )

  case object Empty extends Expr
  val buildLevels : P[Expr] = {

    def _buildLevels(index : Int = 1, rparser : Option[RParser] = None, finalStop : Int = Integer.MAX_VALUE) : RParser = {
      if (finalStop == index) {
        rparser.get
      } else {
        //  start with level one, go through up until the end level.
        // then wehn the end is reached, do it one more time
        // each time passinging the modified parser up the chain
        levels.get(index) match {
          case Some(level) =>
            val result = rparser match {
              case Some(lowerParser) =>
                (p : P[Expr]) => level(lowerParser(p))
              case None =>
                (p : P[Expr]) => level(p)
            }
            _buildLevels(index + 1, Some(result) , finalStop)
          case None =>
            _buildLevels(1, rparser, finalStop = index)
        }
      }
    }
    val base = atom | buildLevels
    _buildLevels(1, None)(base)
  }

  val _bl = buildLevels
  def bl(index : Int, max : Int) : P[Expr] = {
    if (index == max) Fail
    else {
      _bl | bl(index + 1, max)
    }
  }

  println (factor.parse("3*-4/-1*4*5*4*2*3*4/2/4/2/6/8/9"))
  println (parser.parse("3*-4/-1*4*5*4*2*3*4/2/4/2/6/8/9"))
  println (bl(0, 7).parse("3*-4/-1*4*5*4*2*3*4/2/4/2/6/8/9"))
}
