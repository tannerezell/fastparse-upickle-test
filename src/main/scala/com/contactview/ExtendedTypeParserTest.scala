package com.contactview

import java.util.UUID

import fastparse.all
import fastparse.all._

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/24/16
  * ************************************************************************/


object ExtendedTypeParserTest extends App {

  val space = P ( CharsWhile(" ".contains(_: Char)).? )
  val whitespace = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))

  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )

  def strChars(exclude : String = "\"\\") = P( CharsWhile(!exclude.contains(_: Char)))
  val string = P( space ~ "\"" ~/ (strChars() | escape).rep.! ~ "\"")

  sealed trait Expr {
    def className : String
  }
  sealed trait Operator extends Expr {
    def left : Expr
    def right : Expr
    def className : String = right.className
  }
  sealed trait ComparisonOperator extends Operator {
    override def className = BaseClassTypes.Boolean
  }
  case class Literal(value : String, className : String) extends Expr
  case class Reference(id : UUID, className : String) extends Expr
  case class Cast(expr : Expr, className : String) extends Expr
  case class Grouping(expr : Expr) extends Expr { def className = expr.className }
  case class ExprEnd() extends Expr { def className = "end" }

  case class AddOp(left : Expr, right : Expr) extends Operator
  case class SubOp(left : Expr, right : Expr) extends Operator
  case class DivOp(left : Expr, right : Expr) extends Operator
  case class MulOp(left : Expr, right : Expr) extends Operator
  case class ModOp(left : Expr, right : Expr) extends Operator
  case class CmpOp(left : Expr, right : Expr) extends Operator
  case class GtOp (left : Expr, right : Expr) extends ComparisonOperator
  case class GtEOp(left : Expr, right : Expr) extends ComparisonOperator
  case class LtOp (left : Expr, right : Expr) extends ComparisonOperator
  case class LtEOp(left : Expr, right : Expr) extends ComparisonOperator

  case class ActionParser(parser : (ParserContext) => Parser[Expr], className : String)
  trait ParserContext {
    def parsers : Seq[ActionParser]
    def typeParsers : Seq[ClassTypeParser] = Seq(String, Int)
    def baseParsers = combineParsers(typeParsers.map(_.literalParser))
    def getClassTypeParserByClassName(className : String) : Seq[ClassTypeParser] = typeParsers.filter(_.className == className)
    def getParserByClassName(className : String) : Parser[Expr] = {
      if (parsers.nonEmpty)
        combineParsers(getClassTypeParserByClassName(className).map(_.literalParser) ++ parsers.filter(_.className == className).map(_.parser(this))).opaque(className)
      else {

        combineParsers(getClassTypeParserByClassName(className).map(_.literalParser))
      }
    }
    private def combineParsers(parsers : Seq[Parser[Expr]]) = parsers.tail.foldLeft(parsers.head) { case (l, r) => l | r }

    def parse(expression : String) = {
      val endParser = P(End.opaque("end of expression")).map(v => ExprEnd())
      def getClassTypeParserForExpr(expr : Expr) : Option[ClassTypeParser] = expr match {
        case se : Expr => typeParsers.find(_.className == se.className)
      }
      def parseExpression(expression : String, index : Int = 0, previousParsedExpr : Option[Expr] = None) : Parsed[Expr] = {
        def parser : Parser[Expr] = previousParsedExpr match {
          case Some(previousExpr) => getClassTypeParserForExpr(previousExpr).map(_.opParser(previousExpr)(this)).getOrElse(baseParsers)
          case None => baseParsers
        }
//        val parens : Parser[Expr] = P("(" ~ parser ~/ ")")
        val parseResult : Parsed[Expr] = P(parser | endParser).parse(expression, index)
        parseResult match {
          case Parsed.Success(value : ExprEnd, _index) => parseResult
          case Parsed.Success(value : Expr, _index) => parseExpression(expression, _index, Some(value))
          case failure : Parsed.Failure => failure
        }
      }
      parseExpression(expression, 0, None)
    }
  }

  object BaseClassTypes {
    final val String = "java.lang.String"
    final val Integer = "java.lang.Integer"
    final val Boolean = "java.lang.Boolean"
  }

  // type parsers should be the parsers for any type of data type, such as String or Int
  // a type parser needs to be able to define the valid operations for a generic type
  // such as +, -, ==, etc.
  // it should be called when this type has been identified
  trait ClassTypeParser {
    def className : String
    def opParser(left : Expr)(implicit ctx : ParserContext) : Parser[Expr]
    def literalParser : Parser[Expr]
    def actionLiteralParser = ActionParser((ctx) => literalParser, className)
    def opParser(op : String, parser : Parser[Expr]) : Parser[Expr] = P(space ~ op ~ space ~ parser).opaque(op)
    def cast(expr : Expr) : Expr = if (expr.className == className) expr else Cast(expr, className)
  }

  implicit def ctp2p(ctp : ClassTypeParser)(implicit ctx : ParserContext) = ctx.getParserByClassName(ctp.className)

  case object String extends ClassTypeParser {
    override def className: String = BaseClassTypes.String

    override def literalParser: Parser[Expr] = P(string).map(Literal(_, className))

    override def opParser(left : Expr)(implicit ctx : ParserContext): all.Parser[Expr] = {
      left.className match {
        case BaseClassTypes.String =>
          opParser("+", String).map(r => AddOp(left, r)) |
          opParser("+", Int).map(r => AddOp(left, cast(r))) |
          opParser("==", String).map(r => CmpOp(left, r))
        case BaseClassTypes.Integer =>
          opParser("+", String).map(r => AddOp(cast(left), r)) |
          opParser("==", String).map(r => CmpOp(cast(left), r))
      }
    }
  }

  case object Int extends ClassTypeParser {
    override def className: String = BaseClassTypes.Integer

    override def literalParser = P(digits).!.map(v => Literal(v, className))

    override def opParser(left: Expr)(implicit ctx : ParserContext): Parser[Expr] = {
      left.className match {
        case BaseClassTypes.String =>
          opParser("+",  Int).map(v => AddOp(left, String.cast(v)))
        case BaseClassTypes.Integer =>
          opParser("+",  Int).map(v => AddOp(left, v)) |
          opParser("+",  String).map(v => AddOp(String.cast(left), v)) |
          opParser("-",  Int).map(v => SubOp(left, v)) |
          opParser("/",  Int).map(v => DivOp(left, v)) |
          opParser("*",  Int).map(v => MulOp(left, v)) |
          opParser("%",  Int).map(v => ModOp(left, v)) |
          opParser(">",  Int).map(v => GtOp(left, v))  |
          opParser(">=", Int).map(v => GtEOp(left, v)) |
          opParser("<",  Int).map(v => LtOp(left, v))  |
          opParser("<=", Int).map(v => LtEOp(left, v)) |
          opParser("==", Int).map(v => CmpOp(left, v))
      }
    }
  }

//  var ctx = ParserContext(Seq())
//
//  println(ctx.parse("\"hello world\" + \"foo bar\""))
  case class MyStr(str : Expr) extends Expr { def className = BaseClassTypes.String }
//
  def myStrParser(ctx : ParserContext) : Parser[Expr] = P("my str-" ~ String.literalParser).map(v => MyStr(v))
  val myStrActionParser = ActionParser((ctx) => myStrParser(ctx), BaseClassTypes.String)
//  ctx = ctx.copy(parsers = ctx.parsers ++ Seq(myStrActionParser))
//
//  println(upickle.default.write(ctx.parse("\"hello world\" + \"foo bar\" + my str-\"my str again!\"").left.get))

  case class Variable(id : UUID, name : String, value : Expr, className : String)
  case class UccxActionContext(variables : Seq[Variable]) extends ParserContext {
    def variableParsers = variables.map(v => ActionParser((ctx) => P(IgnoreCase(v.name)).map(t => Reference(v.id, v.className)),v.className))
    override def parsers: Seq[ActionParser] = variableParsers ++ Seq(
      myStrActionParser
    )
  }

  val ctx = UccxActionContext(Seq(Variable(UUID.randomUUID(), "hibye", Literal("\"\"", BaseClassTypes.String), BaseClassTypes.String)))

//  println(upickle.default.write(ctx.parse("\"hello world\" + \"foo bar\" + hibye + my str-\"my str again!\"").left.get))
  println(ctx.parse("\"hello world\" + \"foo bar\" + hibYe ==  my str-\"my str again!\""))
  println(ctx.parse("3+4"))





}

/* pre-grouping parser - works
    def parse(expression : String) = {
      def getClassTypeParserForExpr(expr : Expr) : Option[ClassTypeParser] = expr match {
        case se : Expr => typeParsers.find(_.className == se.className)
      }
      def parseExpression(expression : String, index : Int = 0, previousParsedExpr : Option[Expr] = None) : Either[Expr, Parsed.Failure] = {
        def parser : Parser[Expr] = previousParsedExpr match {
          case Some(previousExpr) => getClassTypeParserForExpr(previousExpr).map(_.opParser(previousExpr)(this)).getOrElse(baseParsers)
          case None => baseParsers
        }
        P(parser | End.opaque("end of expression")).parse(expression, index) match {
          case Parsed.Success(value : Expr, _index) => parseExpression(expression, _index, Some(value))
          case Parsed.Success(_, _index) => Left(previousParsedExpr.get)
          case failure : Parsed.Failure => Right(failure)
        }
      }
      parseExpression(expression, 0, None)
    }
 */