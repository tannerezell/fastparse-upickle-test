package com.contactview

/** ***********************************************************************
  * Copyright (C) 2010-2016 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 5/16/17
  * ************************************************************************/

object AddressTest extends App {

  val addresses = Seq(
    "4995 MURPHY CANYON RD\nSAN DIEGO, CA , 92123\nUSA",
    "7343 Ronson Road\nSuite 1\nSan Diego, CA , 92121\nUSA",
    "6815 Flanders Drive\nCenterPark Plaza\nSan Diego, CA , 92121\nUSA",
    "9605 SCRANTON RD - #402\nSAN DIEGO, CA , 92121\nUSA"
  )


//  System.setProperty("java.library.path", "/Users/tannerezell/fastparse_upickle_test/src/main/jniLibs")
  import com.mapzen.jpostal.AddressParser

  // Singleton, parser setup is done in the constructor
  val p = AddressParser.getInstanceDataDir("/Users/tannerezell/fastparse_upickle_test/src/main/resources/ddir/libpostal/")
//  val components = p.parseAddress("The Book Club 100-106 Leonard St, Shoreditch, London, Greater London, EC2A 4RH, United Kingdom")
  addresses.foreach {
    address =>
      val components = p.parseAddress(address)

      components.foreach {
        component =>
          println (s"${component.getLabel} = ${component.getValue}")
      }
  }
}
