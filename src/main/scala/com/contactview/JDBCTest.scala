package com.contactview

import java.sql._

import scala.util.Try
import scala.util.control.NonFatal

/** ***********************************************************************
  * Copyright (C) 2010-2016 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 5/16/17
  * ************************************************************************/

object JDBCTest extends App {
  def getDatabaseNames(implicit con : Connection) = {
    val rs = con.getMetaData.getCatalogs

    while (rs.next()) {
      println (s"table_cat=${rs.getString("TABLE_CAT")}")
    }
  }

  def getTableNames(databaseName : String)(implicit connection: Connection) = {
    val rs = connection.getMetaData.getTables(null, databaseName, "%", null)
    while (rs.next()) {
      println (s"table name=${rs.getString("TABLE_NAME")}")
    }
  }

  def getTableColumns(databaseName : String, tableName : String)(implicit connection: Connection) = {
    val stmt = connection.createStatement()
    val query = s"SELECT * FROM $databaseName.$tableName;"
    println (s"query=$query")
    val rs = stmt.executeQuery(query)
    while (rs.next()) {
      println (s"rs.getMetaData.getColumnCount=${rs.getMetaData.getColumnCount}")
      for (i <- 1 until rs.getMetaData.getColumnCount + 1) yield {
        println (s"i=$i")
        println (s"rs.getMetaData.getColumnName(i)=${Try(rs.getMetaData.getColumnName(i)).getOrElse("error")}")
        println (s"rs.getMetaData.getColumnClassName(i)=${Try(rs.getMetaData.getColumnClassName(i)).getOrElse("error")}")
        println (s"rs.getMetaData.getColumnLabel(i)=${Try(rs.getMetaData.getColumnLabel(i)).getOrElse("error")}")
        println (s"rs.getMetaData.getColumnType(i)=${Try(rs.getMetaData.getColumnType(i)).getOrElse("error")}")
        println (s"rs.getMetaData.getColumnTypeName(i)=${Try(rs.getMetaData.getColumnTypeName(i)).getOrElse("error")}")
        println (s"rs.getMetaData.getColumnDisplaySize(i)=${Try(rs.getMetaData.getColumnDisplaySize(i)).getOrElse("error")}")
      }
    }
    /*
      rs.getMetaData.getColumnCount=3
      i=1
      rs.getMetaData.getColumnName(i)=id
      rs.getMetaData.getColumnClassName(i)=java.lang.Integer
      rs.getMetaData.getColumnLabel(i)=id
      rs.getMetaData.getColumnType(i)=4
      rs.getMetaData.getColumnTypeName(i)=INT
      rs.getMetaData.getColumnDisplaySize(i)=10
      i=2
      rs.getMetaData.getColumnName(i)=name
      rs.getMetaData.getColumnClassName(i)=java.lang.String
      rs.getMetaData.getColumnLabel(i)=name
      rs.getMetaData.getColumnType(i)=12
      rs.getMetaData.getColumnTypeName(i)=VARCHAR
      rs.getMetaData.getColumnDisplaySize(i)=40
      i=3
      rs.getMetaData.getColumnName(i)=age
      rs.getMetaData.getColumnClassName(i)=java.lang.Integer
      rs.getMetaData.getColumnLabel(i)=age
      rs.getMetaData.getColumnType(i)=4
      rs.getMetaData.getColumnTypeName(i)=INT
      rs.getMetaData.getColumnDisplaySize(i)=3
     */
  }


  try{
    import scala.collection.JavaConversions._
    import scala.collection.JavaConverters._
    Class.forName("com.mysql.jdbc.Driver")
    implicit val con: Connection =DriverManager.getConnection("jdbc:mysql://localhost:32773","test","password")

    getDatabaseNames
    getTableNames("sonoo")
    getTableColumns("sonoo", "emp")
//    val con=DriverManager.getConnection("jdbc:mysql://localhost:3306/sonoo","root","root")
    //here sonoo is database name, root is username and password
//   val stmt=con.createStatement()
//    val rs=stmt.executeQuery("select * from emp")
//    while(rs.next())
//      System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3))
    con.close();
  } catch {
    case NonFatal(error) =>
      println (s"got an error: $error")
      error.printStackTrace()
  }
}

