package com.contactview

import fastparse.all.{CharsWhile, P, StringIn}
import org.joda.time.format.DateTimeFormat

/** ***********************************************************************
  * Copyright (C) 2010-2016 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 6/2/17
  * ************************************************************************/


sealed trait FileChanged
case class FileChangedOnUCCX(logTime : Long, className : String, fileName : String) extends FileChanged
case class FileDeletedOnUCCX(logTime : Long, className : String, domain : String, directory : String, fileName : String, userId : String) extends FileChanged
case class FileRenamedOnUCCX(logTime : Long, docType : String, fileFrom : String, fileTo : String) extends FileChanged
case class UnparsedFileStatement(string: String) extends FileChanged

class FileSyncParser {
  import fastparse.all._

  /*
  // rename file
  read line: '104381: Jun 02 09:05:32.095 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread::SYNC_RENAME:/default/crm_test_02.aef'
  read line: '104382: Jun 02 09:05:32.095 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread:renameTo: renamePath/default/crm_test_03.aef'

  //upload file

  read line: '106102: Jun 02 09:10:59.001 MST %MADM-ADM_CFG-7-UNK:FileMgmtServlet.uploadFileDB - clazz:class com.cisco.script.UserScript, pathName:/default/crm_test_03.aef'
    or
  read line: '106133: Jun 02 09:10:59.022 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread:SYNC_SYNC_UPDATE:/default/crm_test_03.aef'


  // delete file
  read line: '106912: Jun 02 09:13:29.180 MST %MADM-FILE_MGR-6-DELETE_FILE_FOLDER_REQ_BY_USER:THREAD:http-bio-443-exec-20:A file or folder delete was initiated by user: Class=class com.cisco.script.UserScript,Domain=default,Directory=/,Name=crm_test_03.aef,User Id=administrator'
  read line: '106923: Jun 02 09:13:29.184 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread:SYNC_DELETE:/default/crm_test_03.aef'

  //doc mgr
  read line: '1118435: Jun 02 09:21:34.618 MST %MIVR-FILE_MGR-7-UNK:THREAD:MIVR_FILE_MGR_TYPE_USERDOCUMENT-20-0-FileTypeSyncThread::DbManager:download:Class:com.cisco.doc.UserDocument:/en_US/my_document.txt'


  //prompt mgr
  read line: '1118571: Jun 02 09:22:47.992 MST %MIVR-FILE_MGR-7-UNK:THREAD:MIVR_FILE_MGR_TYPE_USERPROMPT-24-0-FileTypeSyncThread::DbManager:download:Class:com.cisco.prompt.UserPrompt:/en_US/my_prompt.wav'

   */

  val space = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))
  val string = P (CharsWhile(!"\"\\".contains(_: Char)))
  val stringNoComma = P (CharsWhile(!",".contains(_: Char)))
  val stringNoNewline = P (CharsWhile(!"\n".contains(_: Char)))
  val stringNoCommaNoSpace = P (CharsWhile(!", ".contains(_: Char)))
  val stringNoCommaNoRBracket = P (CharsWhile(!",]".contains(_: Char)))
  val stringNoCommaNoRCurlyBracket = P (CharsWhile(!",}".contains(_: Char)))
  val stringNoCommaNoRClose = P (CharsWhile(!",>".contains(_: Char)))
  val boolean = P ("true" | "false")

  val lineIdParser = P( space ~ (digits.! ~ ":").map(_.toLong) ~ &(" " ~ logTimeParser))
  val months = List ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
  val tzAbbr = List(
    "ACDT",    "ACST",   "ACT",    "ACT",     "ADT",    "AEDT",    "AEST",    "AFT",     "AKDT",   "AKST",   "AMST",
    "AMT",     "AMT",    "ART",    "AST",     "AST",    "AWDT",    "AWST",    "AZOST",   "AZT",    "BDT",    "BDT",
    "BIOT",    "BIT",    "BOT",    "BRST",    "BRT",    "BST",     "BST",     "BST",     "BTT",    "CAT",    "CCT",
    "CDT",     "CDT",    "CEDT",   "CEST",    "CET",    "CHADT",   "CHAST",   "CHOT",    "ChST",   "CHUT",
    "CIST",    "CIT",    "CKT",    "CLST",    "CLT",    "COST",    "COT",     "CST",     "CST",    "CST",    "CST",
    "CST",     "CT",     "CVT",    "CWST",    "CXT",    "DAVT",    "DDUT",    "DFT",     "EASST",  "EAST",   "EAT",
    "ECT",     "ECT",    "EDT",    "EEDT",    "EEST",   "EET",     "EGST",    "EGT",     "EIT",    "EST",    "EST",
    "FET",     "FJT",    "FKST",   "FKST",    "FKT",    "FNT",     "GALT",    "GAMT",    "GET",    "GFT",    "GILT",
    "GIT",     "GMT",    "GST",    "GST",     "GYT",    "HADT",    "HAEC",    "HAST",    "HKT",    "HMT",    "HOVT",
    "HST",     "IBST",   "ICT",    "IDT",     "IOT",    "IRDT",    "IRKT",    "IRST",    "IST",    "IST",    "IST",
    "JST",     "KGT",    "KOST",   "KRAT",    "KST",    "LHST",    "LHST",    "LINT",    "MAGT",   "MART",   "MAWT",
    "MDT",     "MET",    "MEST",   "MHT",     "MIST",   "MIT",     "MMT",     "MSK",     "MST",    "MST",    "MST",
    "MUT",     "MVT",    "MYT",    "NCT",     "NDT",    "NFT",     "NPT",     "NST",     "NT",     "NUT",    "NZDT",
    "NZST",    "OMST",   "ORAT",   "PDT",     "PET",    "PETT",    "PGT",     "PHOT",    "PKT",    "PMDT",   "PMST",
    "PONT",    "PST",    "PST",    "PYST",    "PYT",    "RET",     "ROTT",    "SAKT",    "SAMT",   "SAST",   "SBT",
    "SCT",     "SGT",    "SLST",   "SRET",    "SRT",    "SST",     "SST",     "SYOT",    "TAHT",   "THA",    "TFT",
    "TJT",     "TKT",    "TLT",    "TMT",     "TOT",    "TVT",     "UCT",     "ULAT",    "USZ1",   "UTC",    "UYST",
    "UYT",     "UZT",    "VET",    "VLAT",    "VOLT",   "VOST",    "VUT",     "WAKT",    "WAST",   "WAT",    "WEDT",
    "WEST",    "WET",    "WIT",    "WST",     "YAKT",   "YEKT",    "Z")

  val monthParser = StringIn(months : _ *)
  val tzParser = StringIn(tzAbbr : _ *)
  val timeParser : P[Long] = P ( digits ~ ":" ~ digits ~ ":" ~ digits ~ "." ~ digits).!.map {
    time =>
      val fmt = new java.text.SimpleDateFormat("HH:mm:ss.SSS")
      fmt.parse(time).getTime
  }

  val end = P("\n" | "\r" | End)

  val fmt = DateTimeFormat.forPattern("MMM dd Y HH:mm:ss.SSS Z")
  val year = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date())
  val logTimeParser : P[Long] = P ((monthParser.! ~ " " ~ digits.! ~ " " ~ timeParser.!) ~ " " ~ tzParser.!).map {
    case (month, day, time, tz) =>
      val date = fmt.withOffsetParsed().parseDateTime(s"$month $day $year $time ${FileSyncParser.tzAbbr2Offset(tz)}")
      date.getMillis
  }

  //132715: Jun 02 11:21:59.065 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread::DbManager:download:Class:com.cisco.script.UserScript:/default/crm_test_03.aef

  val engineFileChangesParser : P[FileChangedOnUCCX] = P(lineIdParser ~ " " ~ logTimeParser ~ space ~ "%M" ~ ("IVR" | "ADM") ~ "-FILE_MGR-" ~ digits ~ "-UNK:THREAD:" ~ CharsWhile(_ != ':') ~ "::DbManager:download:Class:" ~ CharsWhile(_ != ':').! ~ ":" ~ CharsWhile(!"\n\r".contains(_)).! ~ end).map {
    case (_, logTime, className, fileName) =>
      FileChangedOnUCCX(logTime, className, fileName)
  }

  val fileDeletedParser : P[FileDeletedOnUCCX] = P(
    lineIdParser ~ " " ~ logTimeParser ~ space ~
      "%M" ~ CharsWhile(_ != '-') ~ "-FILE_MGR-" ~ digits ~ "-DELETE_FILE_FOLDER_REQ_BY_USER:THREAD:http-bio-" ~ digits ~"-exec-" ~ digits ~ ":A file or folder delete was initiated by user: Class=class " ~
      CharsWhile(_ != ',').! ~ ",Domain=" ~ CharsWhile(_ != ',').! ~ ",Directory=" ~ CharsWhile(_ != ',').! ~ ",Name=" ~ CharsWhile(_ != ',').! ~ ",User Id=" ~ CharsWhile(!"\n\r".contains(_)).! ~ end).map {
    case (lineId, logTime, className, domain, directory, fileName, userId) =>
      FileDeletedOnUCCX(logTime, className, domain, directory, fileName, userId)
  }

  /*
  // rename file
  read line: '104381: Jun 02 09:05:32.095 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread::SYNC_RENAME:/default/crm_test_02.aef'
  read line: '104382: Jun 02 09:05:32.095 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread:renameTo: renamePath/default/crm_test_03.aef'
   */
  /*
              1123336: Jun 02 10:49:25.636 MST %MIVR-FILE_MGR-7-UNK:THREAD:MIVR_FILE_MGR_TYPE_USERDOCUMENT-20-0-FileTypeSyncThread::SYNC_RENAME:/en_US/2501.txt
              1123337: Jun 02 10:49:25.636 MST %MIVR-FILE_MGR-7-UNK:THREAD:MIVR_FILE_MGR_TYPE_USERDOCUMENT-20-0-FileTypeSyncThread:renameTo: renamePath/en_US/2500.txt
   */
//read line: '104381: Jun 02 09:05:32.095 MST %MADM-FILE_MGR-7-UNK:THREAD:MADM_FILE_MGR_TYPE_USERSCRIPT-18-0-FileTypeSyncThread::SYNC_RENAME:/default/crm_test_02.aef'

  val fileRenamedParser : P[FileRenamedOnUCCX] = P(
    lineIdParser ~ " " ~ logTimeParser ~ space ~ "%M" ~ ("IVR" | "ADM") ~ "-FILE_MGR-" ~ digits ~ "-UNK:THREAD:" ~ "M" ~ ("IVR" | "ADM") ~ "_FILE_MGR_TYPE_" ~ CharsWhile(_ != '-').! ~ "-" ~ CharsWhile(_ != ':') ~ "::SYNC_RENAME:" ~ CharsWhile(!"\n\r".contains(_)).! ~ "\n" ~
    lineIdParser ~ " " ~ logTimeParser ~ space ~ "%M" ~ ("IVR" | "ADM") ~ "-FILE_MGR-" ~ digits ~ "-UNK:THREAD:" ~ "M" ~ ("IVR" | "ADM") ~ "_FILE_MGR_TYPE_" ~ CharsWhile(_ != '-').! ~ "-" ~ CharsWhile(_ != ':') ~ ":renameTo: renamePath" ~ CharsWhile(!"\n\r".contains(_)).!  ~ end
  ).map {
    case (_, logtime, docType, fileFrom, _, _, _, renameTo) =>
      FileRenamedOnUCCX(logtime, docType, fileFrom, renameTo)
  }

  val ignoredParser : P[FileChanged] = P (lineIdParser.? ~ CharsWhile(!"\n".contains(_)).!.? ~ "\n" ~ space).map(v => UnparsedFileStatement(v._2.getOrElse("")))

  val parser : P[Seq[FileChanged]] = P( (engineFileChangesParser | fileDeletedParser | fileRenamedParser | ignoredParser).rep ~ End)

}

object FileSyncParser extends App {
  val parser = new FileSyncParser()
  def tzAbbr2Offset(tz : String) = tz match {
    case "ACDT" => "+1030"
    case "ACST" => "+0930"
    case "ACT" => "-0500"
    //    case "ACT" => "+0630"
    case "ADT" => "-0300"
    case "AEDT" => "+1100"
    case "AEST" => "+1000"
    case "AFT" => "+0430"
    case "AKDT" => "-0800"
    case "AKST" => "-0900"
    case "AMST" => "-0300"
    case "AMT" => "-0400"
    case "AMT" => "+0400"
    case "ART" => "-0300"
    case "AST" => "+0300"
    case "AST" => "-0400"
    case "AWST" => "+0800"
    case "AZOST" => "0000"
    case "AZOT" => "-0100"
    case "AZT" => "+0400"
    case "BDT" => "+0800"
    case "BIOT" => "+0600"
    case "BIT" => "-1200"
    case "BOT" => "-0400"
    case "BRST" => "-0200"
    case "BRT" => "-0300"
    case "BST" => "+0600"
    case "BST" => "+1100"
    case "BST" => "+0100"
    case "BTT" => "+0600"
    case "CAT" => "+0200"
    case "CCT" => "+0630"
    case "CDT" => "-0500"
    case "CDT" => "-0400"
    case "CEST" => "+0200"
    case "CET" => "+0100"
    case "CHADT" => "+1345"
    case "CHAST" => "+1245"
    case "CHOT" => "+0800"
    case "CHOST" => "+0900"
    case "CHST" => "+1000"
    case "CHUT" => "+1000"
    case "CIST" => "-0800"
    case "CIT" => "+0800"
    case "CKT" => "-1000"
    case "CLST" => "-0300"
    case "CLT" => "-0400"
    case "COST" => "-0400"
    case "COT" => "-0500"
    case "CST" => "-0600"
    case "CST" => "+0800"
    case "ACST" => "+0930"
    case "ACDT" => "+1030"
    case "CST" => "-0500"
    case "CT" => "+0800"
    case "CVT" => "-0100"
    case "CWST" => "+0845"
    case "CXT" => "+0700"
    case "DAVT" => "+0700"
    case "DDUT" => "+1000"
    case "DFT" => "+0100"
    case "EASST" => "-0500"
    case "EAST" => "-0600"
    case "EAT" => "+0300"
    case "ECT" => "-0400"
    case "ECT" => "-0500"
    case "EDT" => "-0400"
    case "AEDT" => "+1100"
    case "EEST" => "+0300"
    case "EET" => "+0200"
    case "EGST" => "0000"
    case "EGT" => "-0100"
    case "EIT" => "+0900"
    case "EST" => "-0500"
    case "AEST" => "+1000"
    case "FET" => "+0300"
    case "FJT" => "+1200"
    case "FKST" => "-0300"
    case "FKT" => "-0400"
    case "FNT" => "-0200"
    case "GALT" => "-0600"
    case "GAMT" => "-0900"
    case "GET" => "+0400"
    case "GFT" => "-0300"
    case "GILT" => "+1200"
    case "GIT" => "-0900"
    case "GMT" => "0000"
    case "GST" => "-0200"
    case "GST" => "+0400"
    case "GYT" => "-0400"
    case "HADT" => "-0900"
    case "HAEC" => "+0200"
    case "HAST" => "-1000"
    case "HKT" => "+0800"
    case "HMT" => "+0500"
    case "HOVST" => "+0800"
    case "HOVT" => "+0700"
    case "ICT" => "+0700"
    case "IDT" => "+0300"
    case "IOT" => "+0300"
    case "IRDT" => "+0430"
    case "IRKT" => "+0800"
    case "IRST" => "+0330"
    case "IST" => "+0530"
    case "IST" => "+0100"
    case "IST" => "+0200"
    case "JST" => "+0900"
    case "KGT" => "+0600"
    case "KOST" => "+1100"
    case "KRAT" => "+0700"
    case "KST" => "+0900"
    case "LHST" => "+1030"
    case "LHST" => "+1100"
    case "LINT" => "+1400"
    case "MAGT" => "+1200"
    case "MART" => "-93000"
    case "MAWT" => "+0500"
    case "MDT" => "-0600"
    case "MET" => "+0100"
    case "MEST" => "+0200"
    case "MHT" => "+1200"
    case "MIST" => "+1100"
    case "MIT" => "-93000"
    case "MMT" => "+0630"
    case "MSK" => "+0300"
    case "MST" => "+0800"
    case "MST" => "-0700"
    case "MUT" => "+0400"
    case "MVT" => "+0500"
    case "MYT" => "+0800"
    case "NCT" => "+1100"
    case "NDT" => "-23000"
    case "NFT" => "+1100"
    case "NPT" => "+0545"
    case "NST" => "-33000"
    case "NT" => "-33000"
    case "NUT" => "-1100"
    case "NZDT" => "+1300"
    case "NZST" => "+1200"
    case "OMST" => "+0600"
    case "ORAT" => "+0500"
    case "PDT" => "-0700"
    case "PET" => "-0500"
    case "PETT" => "+1200"
    case "PGT" => "+1000"
    case "PHOT" => "+1300"
    case "PHT" => "+0800"
    case "PKT" => "+0500"
    case "PMDT" => "-0200"
    case "PMST" => "-0300"
    case "PONT" => "+1100"
    case "PST" => "-0800"
    case "PST" => "+0800"
    case "PYST" => "-0300"
    case "PYT" => "-0400"
    case "RET" => "+0400"
    case "ROTT" => "-0300"
    case "SAKT" => "+1100"
    case "SAMT" => "+0400"
    case "SAST" => "+0200"
    case "SBT" => "+1100"
    case "SCT" => "+0400"
    case "SGT" => "+0800"
    case "SLST" => "+0530"
    case "SRET" => "+1100"
    case "SRT" => "-0300"
    case "SST" => "-1100"
    case "SST" => "+0800"
    case "SYOT" => "+0300"
    case "TAHT" => "-1000"
    case "THA" => "+0700"
    case "TFT" => "+0500"
    case "TJT" => "+0500"
    case "TKT" => "+1300"
    case "TLT" => "+0900"
    case "TMT" => "+0500"
    case "TRT" => "+0300"
    case "TOT" => "+1300"
    case "TVT" => "+1200"
    case "ULAST" => "+0900"
    case "ULAT" => "+0800"
    case "USZ1" => "+0200"
    case "UTC" => "0000"
    case "UYST" => "-0200"
    case "UYT" => "-0300"
    case "UZT" => "+0500"
    case "VET" => "-0400"
    case "VLAT" => "+1000"
    case "VOLT" => "+0400"
    case "VOST" => "+0600"
    case "VUT" => "+1100"
    case "WAKT" => "+1200"
    case "WAST" => "+0200"
    case "WAT" => "+0100"
    case "WEST" => "+0100"
    case "WET" => "0000"
    case "WIT" => "+0700"
    case "WST" => "+0800"
    case "YAKT" => "+0900"
    case "YEKT" => "+0500"
    case _ => "+0"
  }
}
