package com.contactview

import scala.util.Try

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 7/20/16
  * ************************************************************************/


object AnotherParserTest extends App {

  import fastparse.all._

  // right to left associtivity test

  trait Expr

  case class Literal(value: Any) extends Expr

  trait Op

  case object Mod extends Op

  case class BinOp(lhs: Expr, op: Op, rhs: Expr) extends Expr

  case class Bitwise(lhs: Expr, op: bitwiseop, rhs: Expr) extends Expr

  case class Equality(lhs: Expr, op: equalityop, rhs: Expr) extends Expr

  case class Relational(lhs: Expr, op: relationalop, rhs: Expr) extends Expr

  case class Shift(lhs: Expr, op: shiftop, rhs: Expr) extends Expr

  case class Additive(lhs: Expr, op: additiveop, rhs: Expr) extends Expr

  case class Multiplicative(lhs: Expr, op: multiplicativeop, rhs: Expr) extends Expr

  case class NewObj(identifier: Expr) extends Expr

  case class ObjCast(castAs: QualifiedIdentifier, toCast: Expr) extends Expr

  trait assignop

  object assignop {

    case object Assign extends assignop

    case object Add extends assignop

    case object Subtract extends assignop

    case object Multiply extends assignop

    case object Divide extends assignop

    case object Modulus extends assignop

    case object LeftShift extends assignop

    case object Bitwise extends assignop

    case object RightShift extends assignop

    case object BitwiseExclusiveOr extends assignop

    case object BitwiseInclusiveOr extends assignop

  }

  trait ifconditional

  case object ConditionalThen extends ifconditional

  case object ConditionalElse extends ifconditional

  trait conditionalop

  object conditionalop {

    case object And extends conditionalop

    case object Or extends conditionalop

  }

  trait bitwiseop

  object bitwiseop {

    case object And extends bitwiseop

    case object Or extends bitwiseop

    case object Xor extends bitwiseop

    //    case object Not extends bitwiseop
  }

  trait equalityop

  object equalityop {

    case object Equal extends equalityop

    case object NotEqual extends equalityop

  }

  trait relationalop

  object relationalop {

    case object Lt extends relationalop

    case object LtE extends relationalop

    case object Gt extends relationalop

    case object GtE extends relationalop

    case object InstanceOf extends relationalop

  }

  trait shiftop

  object shiftop {

    case object LeftShift extends shiftop

    case object RightShift extends shiftop

    case object ZeroFillRightShift extends shiftop

  }

  trait additiveop

  object additiveop {

    case object Add extends additiveop

    case object Sub extends additiveop

  }

  trait multiplicativeop

  object multiplicativeop {

    case object Multiply extends multiplicativeop

    case object Divide extends multiplicativeop

    case object Modulus extends multiplicativeop

  }

  trait castop

  object castop {

    case object Cast extends castop

    case object NewObj extends castop

  }


  case class IfConditionalOp(lhs: Expr, first: Expr, next: Expr) extends Expr

  case class Conditional(lhs: Expr, op: conditionalop, rhs: Expr) extends Expr

  case class Assignment(lhs: Expr, op: assignop, rhs: Expr) extends Expr

  val digit = CharIn('0' to '9')
  val integer = digit.rep(1).!.map(v => Literal(v.toInt))

  type RParser = P[Expr] => P[Expr]

  def Op[T](op: String, value: T): P[T] = P(op).map(v => value)

  def ChainRight[T](op: P[T])(fmt: (Expr, T, Expr) => Expr): RParser = (p: P[Expr]) => P(p ~ (op ~/ p).rep).map {
    case (lhs, chunks) =>

      chunks.foldRight(lhs) { case ((op, lhs), rhs) => fmt(lhs, op, rhs) }
  } | p

  def ChainLeft[T](op: P[T])(fmt: (Expr, T, Expr) => Expr): RParser = (p: P[Expr]) => P(p ~ (op ~/ p).rep).map {
    case (lhs, chunks) =>
      chunks.foldLeft(lhs) { case (lhs, (op, rhs)) => fmt(lhs, op, rhs) }
  }

  //  val test : P[Expr] = ChainRight(Op("^", Mod))(integer)

  //5^4^3^2 == 5^(4^(3^2))
  //  println(test.parse("5^4^3^2").get.value)
  //  println(test.parse("5^4^3^2").get.value == BinOp(Literal(5), Mod, BinOp(Literal(4), Mod, BinOp(Literal(3), Mod, Literal(2)))))

  val Assign = Op("=", assignop.Assign)
  val Add_AND = Op("+=", assignop.Add)
  val Subtract_AND = Op("-=", assignop.Subtract)
  val Multiply_AND = Op("*=", assignop.Multiply)
  val Divide_AND = Op("/=", assignop.Divide)
  val Modulus_AND = Op("%=", assignop.Modulus)
  val LeftShift = Op("<<=", assignop.LeftShift)
  val Bitwise_AND = Op(">>=", assignop.Bitwise)
  val RightShift = Op("&=", assignop.RightShift)
  val Bitwise_Ex_OR = Op("^=", assignop.BitwiseExclusiveOr)
  val Bitwise_In_OR = Op("|=", assignop.BitwiseInclusiveOr)

  val assignmentOperators: P[assignop] = P(Assign | Add_AND | Subtract_AND | Multiply_AND | Divide_AND | Modulus_AND | LeftShift | Bitwise_AND | RightShift | Bitwise_Ex_OR | Bitwise_In_OR)

  val assignment: RParser = (p: P[Expr]) => P(p ~ (assignmentOperators ~/ p).rep).map {
    case (lhs, chunks) =>
      chunks.foldRight(lhs) { case ((op, lhs), rhs) => Assignment(lhs, op, rhs) }
  } | p


  //  println(assignment(integer).parse("5+=19+=8"))

  val cond_then = Op("?", ConditionalThen)
  val cond_else = Op(":", ConditionalElse)

  val conditional: RParser = (p: P[Expr]) => P(p ~ (cond_then ~/ p ~ cond_else ~/ p).?).map {
    //    case (lhs, op1, first, op2, next) => IfConditionalOp(lhs, first, next)
    case (lhs, rhs) =>
      rhs match {
        case None => lhs
        case Some((op1, first, op2, last)) => IfConditionalOp(lhs, first, last)
      }
  }

  //  println(conditional(assignment(integer)).parse("5?3+=1:1"))

  val cond_or = ChainLeft(Op("||", conditionalop.Or)) { case (lhs, op, rhs) => Conditional(lhs, op, rhs) }
  val cond_and = ChainLeft(Op("&&", conditionalop.And)) { case (lhs, op, rhs) => Conditional(lhs, op, rhs) }

  val bitwise_or = ChainLeft(Op("|", bitwiseop.Or)) { case (lhs, op, rhs) => Bitwise(lhs, op, rhs) }
  val bitwise_xor = ChainLeft(Op("^", bitwiseop.Xor)) { case (lhs, op, rhs) => Bitwise(lhs, op, rhs) }
  val bitwise_and = ChainLeft(Op("&", bitwiseop.And)) { case (lhs, op, rhs) => Bitwise(lhs, op, rhs) }

  val equality = ChainLeft(Op("==", equalityop.Equal) | Op("!=", equalityop.NotEqual)) { case (lhs, op, rhs) => Equality(lhs, op, rhs) }
  val relational = ChainLeft(
    Op("<=", relationalop.LtE) | Op("<", relationalop.Lt) |
      Op(">=", relationalop.GtE) | Op(">", relationalop.Gt) |
      Op("instanceof", relationalop.InstanceOf)
  ) { case (lhs, op, rhs) => Relational(lhs, op, rhs) }

  val shift = ChainLeft(
    Op("<<", shiftop.LeftShift) |
      Op(">>", shiftop.RightShift) |
      Op(">>>", shiftop.ZeroFillRightShift)
  ) { case (lhs, op, rhs) => Shift(lhs, op, rhs) }

  val additive = ChainLeft(
    Op("+", additiveop.Add) |
      Op("-", additiveop.Sub)
  ) { case (lhs, op, rhs) => Additive(lhs, op, rhs) }

  val multiplicative = ChainLeft(
    Op("*", multiplicativeop.Multiply) |
      Op("/", multiplicativeop.Divide) |
      Op("%", multiplicativeop.Modulus)
  ) { case (lhs, op, rhs) => Multiplicative(lhs, op, rhs) }

  case class QualifiedIdentifier(id: String) extends Expr

  val qualifiedId = CharIn(('a' to 'z') ++ ('A' to 'Z') ++ Set('_', '.')).rep.!.map(QualifiedIdentifier)

  val cast_newObj: RParser = (p: P[Expr]) =>
    P(
      ("(" ~ qualifiedId ~ ")" ~/ p).map {
        v => ObjCast(v._1, v._2)
      } |
        ("new" ~/ p).map(v => NewObj(v))
    )


  trait somethingop

  object somethingop {

    case object Increment extends somethingop

    case object Decrement extends somethingop

    case object UnaryPlus extends somethingop

    case object UnaryMinus extends somethingop

    case object LogicalNot extends somethingop

    case object BitwiseNot extends somethingop

  }

  case class Operation(lhs: Expr, op: somethingop, rhs: Expr) extends Expr

  val level2_ops =
    Op("++", somethingop.Increment) |
      Op("--", somethingop.Decrement) |
      Op("+", somethingop.UnaryPlus) |
      Op("-", somethingop.UnaryMinus) |
      Op("!", somethingop.LogicalNot) |
      Op("~", somethingop.BitwiseNot)

  case class L2(op: somethingop, expr: Expr) extends Expr

  //  val level2 = ChainRight(level2_ops){ case (lhs, op, rhs) => Operation(lhs, op, rhs) } //P(level2_ops ~/ p).map(v => Operation(v._1, v._2)) | p
  //  val level2 : RParser = (p) => P(( level2_ops) ~/ p).map(v => L2(v._1, v._2)) | p
  val level2: RParser = (p) => P((level2_ops ~/ p)).map {
//    case (lhs, rhs) =>
//      rhs match {
//        case None => lhs
//        case Some((op, rhs)) => L2(op, rhs)
//      }
    v => L2(v._1, v._2)
  } | p
//  val level1 : RParser = (p : P[Expr]) => P(p ~ level1_ops ~/).map(v => Operation(v._1, v._2))

  /*
  level 2 (integer) =
  (integer) => (integer ~ "+") ~ integer
   */
  case class Parens(expr : Expr) extends Expr
  val level1 : RParser = (p) => P("(" ~/ p ~ ")").map(Parens) | p

  case class Grouping(expr : Seq[Expr]) extends Expr

  val atom  = integer //(p : P[Expr]) => (integer | p).rep.map(Grouping)

  case class Something(lhs : Expr, rhs : Expr) extends Expr
  def oop(current : Int, max : Int = 2) : P[Expr] = {
    if (current == max) {
      Fail
    } else {
      assignment(  //15
        conditional( //14
          cond_or( //13
            cond_and( //12
              bitwise_or( //11
                bitwise_xor( //10
                  bitwise_and( //9
                    equality( //8
                      relational( //7
                        shift( //6
                          additive( //5
                            multiplicative( //4
                              //                            cast_newObj( //3
//                              level2( //2
                                //                                P (integer ~ oop.?).map {
                                //                                  case (lhs, None) => lhs
                                //                                  case (lhs, Some(rhs)) => Something(lhs, rhs)
                                //                                }
                                level1(
                                  (integer.log("integer") ~ oop(current+1,max).?.log("oop")).map {
                                    case (lhs, rhs) =>
                                      rhs match {
                                        case Some(x) => x
                                        case None => lhs
                                      }
                                  }
                                ).opaque("level1").log()
//                              ).opaque("level2").log()
                              //                            ).log()
                            ).opaque("multi").log()
                          ).opaque("additive").log()
                        ).opaque("shift").log()
                      ).opaque("relational").log()
                    ).opaque("equality").log()
                  ).opaque("bitwise_and").log()
                ).opaque("bitwise_xor").log()
              ).opaque("bitwise_or").log()
            ).opaque("cond_and").log()
          ).opaque("and_or").log()
        ).opaque("conditional").log()
      ).opaque("assignment").log()
    }
  }


  val parser : P[Expr] = {
    def bbb : P[Expr] = (integer ~ parser.?).map {
      case (lhs, rhs) =>
        rhs match {
          case None => lhs
          case Some(_rhs) => _rhs
        }
    }
    assignment(conditional(cond_or(cond_and(bitwise_or(bitwise_xor(bitwise_and(equality(relational(shift(additive(multiplicative(level2(atom | parser)))))))))))))
  }
//  println(oop(0, 2).parse("5-1*6/3+4")) // == ((5-1)*6)/3 should be 5-((1*6)/3)



}
