package com.contactview

import fastparse.noApi.{CharIn, P}

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 12/17/16
  * ************************************************************************/


package object javaparser {
  trait Token {
    val parser : P[_]
  }

  val hexDigit : P[Unit] = P(CharIn(('0' to '9') ++ ('a' to 'f') ++ ('A' to 'F')))
}
