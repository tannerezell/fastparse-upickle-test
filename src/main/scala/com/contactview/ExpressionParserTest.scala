package com.contactview

import fastparse.all._

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/22/16
  * ************************************************************************/


object ExpressionParserTest extends App {
  object SupportedClasses {
    final val String = "java.lang.String"
    final val Int = "java.lang.Integer"
    final val Prompt = "com.cisco.play.Playable"

    //todo:fixme this should be better, right now it is very basic
    def canRightBeLeft(right : String, left : String) : Boolean = {
      left == right || (right match {
        case String => left == Int
        case Int => left == String
        case _ => false
      })
    }
  }

  trait OpImd //intermediates, the right side of a complex expression. Operator Intermediate data types
  case class AddImd(right : Expr) extends OpImd
  case class SubImd(right : Expr) extends OpImd
  case class CompImd(right : Expr) extends OpImd

  sealed trait Expr {
    def resultType : String
    def value : String
    def exprString : String
    def isValid : Boolean
  }

  case class Literal(value : String, resultType : String) extends Expr {
    override def exprString: String = value

    override def isValid: Boolean = true
  }

  case class Reference(variable : Variable) extends Expr {

    override def exprString: String = value

    override def resultType: String = variable.resultType

    override def value: String = variable.value

    override def isValid: Boolean = true
  }

  sealed trait Operator extends Expr {
    def left : Expr
    def right : Expr
    def sign : String

    def isValidOp : Boolean = left.resultType == right.resultType

    override def isValid: Boolean = {
      (left.isValid && right.isValid) && isValidOp && // both expressions need to be valid first and foremeost
        (left.resultType == right.resultType || // if both expressions are the same type we're okay
          SupportedClasses.canRightBeLeft(right.resultType, left.resultType) //or if both expressions can be compatible
          )
    }
    override def resultType : String = left.resultType
    override def exprString: String = s"${left.exprString} $sign ${right.exprString}"
    override def value : String = exprString //todo:fixme
  }
  case class AddOp(left : Expr, right : Expr) extends Operator {
    override def sign: String = "+"
  }

  case class SubOp(left : Expr, right : Expr) extends Operator {
    override def sign: String = "-"
    override def isValidOp : Boolean = {
      super.isValidOp &&
      !(left.resultType == SupportedClasses.Prompt) &&
      !(left.resultType == SupportedClasses.String)
    }
  }

  case class CompareOp(left : Expr, right : Expr) extends Operator {
    override def sign: String = "=="
  }

  sealed trait Statement {
    def expr : Expr
    def isValid : Boolean = expr.isValid
  }
  case class ExpressionStatement(expr : Operator) extends Statement
  case class ReferenceStatement(expr : Reference) extends Statement
  case class LiteralStatement(expr : Literal) extends Statement

  def toStatement(expr : Expr) = expr match {
    case e : Operator => ExpressionStatement(e)
    case e : Reference => ReferenceStatement(e)
    case e : Literal => LiteralStatement(e)
  }

  // all expressions have a result type

  case class Variable(name : String, value : String, resultType : String)


  val vars = List(Variable("my int", "", SupportedClasses.Int), Variable("my string", "\"hello world!\"", SupportedClasses.String))

  def variableParser(variableList : List[Variable]) : Parser[Reference] =
    StringIn(variableList.map(_.name) : _ *)
      .!
      .map(_name =>
        variableList.find(v =>
          v.name == _name)
          .map(Reference)
          .get
      )

  val space = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))

  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )

  val strChars = P( CharsWhile(!"\"\\".contains(_: Char)))
  val string =
    P( space ~ "\"" ~/ (strChars | escape).rep.! ~ "\"").map(v => Literal('"' + v + '"', SupportedClasses.String))

  var int = digits.!.map(v => Literal(v, SupportedClasses.Int))



  val dt : Parser[Expr] = P (variableParser(vars) | string | int)

  println(dt.parse("\"hello world!\""))
  println(dt.parse("35"))

  val addImdParser : Parser[AddImd] = P (("+" ~ space ~/ dt).map(AddImd))
  val compImdParser : Parser[CompImd] = P (("==" ~ space ~/ dt).map(CompImd))
  val subImdParser : Parser[SubImd] = P (("-" ~ space ~/ dt).map(SubImd))
  val imdParsers = P (addImdParser | subImdParser | compImdParser)

  val statementParser : Parser[Statement] = P (dt ~ space ~/ imdParsers.rep).map {
    case (left, right) =>
      right.foldLeft(left) {
        case (l : Expr, r : OpImd) =>
          r match {
            case AddImd(expr) => AddOp(l, expr)
            case CompImd(expr) => CompareOp(l, expr)
            case SubImd(expr) => SubOp(l, expr)
          }
      }
  }.map(toStatement)

  val stmt = statementParser.parse("3  + 4+\"hello world\"==  3")
  println (stmt)
  println (stmt.get.value.expr.exprString)
  println (stmt.get.value.expr.isValid)

  println(upickle.default.write(stmt.get.value))
  println (statementParser.parse("\"hello world\" + \"my str \""))
  println (statementParser.parse("\"hello world\" + \"my str \"").get.value.isValid)
  println (statementParser.parse("\"hello world\" - \"my str \"").get.value)
  println (statementParser.parse("\"hello world\" - \"my str \"").get.value.isValid)
  println (statementParser.parse("4 - 5").get.value.isValid)
  println (statementParser.parse("4 - 5").get.index)

  // while prompts can be concatenated via the + operator, they cannot be subtracted.
  // Same with strings, in these cases str - str should be an invalid expression.



}
