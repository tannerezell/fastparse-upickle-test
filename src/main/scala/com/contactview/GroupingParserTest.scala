package com.contactview

import fastparse.all._

/** ***********************************************************************
  * Copyright (C) 2010-2013 CTI LOGIC, L.L.C <contact@ctilogic.com>
  *
  * This file can not be copied and/or distributed without the express
  *
  * written permission of CTI LOGIC, L.L.C.
  *
  * Project : parser-test
  * Date    : 3/24/16
  * ************************************************************************/


object GroupingParserTest extends App {

  import fastparse.all._
  val space = P ( CharsWhile(" ".contains(_: Char)).? )
  val whitespace = P ( CharsWhile(" \r\n".contains(_: Char)).? )
  val digits = P (CharsWhile('0' to '9' contains (_: Char)))

  val hexDigit      = P( CharIn('0'to'9', 'a'to'f', 'A'to'F') )
  val unicodeEscape = P( "u" ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit )
  val escape        = P( "\\" ~ (CharIn("\"/\\bfnrt") | unicodeEscape) )

  def strChars(exclude : String = "\"\\") = P( CharsWhile(!exclude.contains(_: Char)))
  val string = P( space ~ "\"" ~/ (strChars() | escape).rep.! ~ "\"")

  sealed trait Expr {
    def index : Int
    def className : String
  }
//  case class OperatorIntermediate(op : Operator, right : Expr) extends Expr {
//    def className = right.className
//  }
  sealed trait OperatorExpr extends Expr {
    def left : Expr
    def right : Expr
    def className = right.className
    def symbol : String
  }
  sealed trait ComparisonOperator extends BaseOperator {
    def className = BaseClassTypes.Boolean
  }

  sealed trait BaseOperator
  sealed trait BooleanOp extends BaseOperator
  object BooleanOp {
    case object And extends BooleanOp
    case object Or extends BooleanOp
  }

  sealed trait Operator extends BaseOperator
  object Operator {
    case object Add extends Operator
    case object Sub extends Operator
    case object Mul extends Operator
    case object Div extends Operator
    case object Mod extends Operator
    case object Pow extends Operator
  }

//  sealed trait ComparisonOperator extends BaseOperator
  object ComparisonOperator {
    case object Eq extends ComparisonOperator
    case object NotEq extends ComparisonOperator
    case object Lt extends ComparisonOperator
    case object LtEq extends ComparisonOperator
    case object Gt extends ComparisonOperator
    case object GtEq extends ComparisonOperator
  }

  case class Literal(value : String, className : String, index : Int) extends Expr
  case class Grouping(expr : Expr, index : Int) extends Expr { def className = expr.className }
  case class OpImd(op : BaseOperator, expr : Expr, index : Int) extends Expr { def className = expr.className }
  case class Op(left : Expr, op : BaseOperator, right : Expr, index : Int) extends Expr { def className = right.className }


  def ooParser(exprParser : Parser[Expr]) : Parser[Expr] = {
    def factor : P[Expr] = P (exprParser | parens)
    def div = P(Index ~ "/" ~/ factor).map(v => OpImd(Operator.Div, v._2, v._1))
    def mul = P(Index ~ "*" ~/ factor).map(v => OpImd(Operator.Mul, v._2, v._1))
    def add = P(Index ~ "+" ~/ divMul).map(v => OpImd(Operator.Add, v._2, v._1))
    def sub = P(Index ~ "-" ~/ divMul).map(v => OpImd(Operator.Sub, v._2, v._1))
    def mod = P(Index ~ "%" ~/ factor).map(v => OpImd(Operator.Mod, v._2, v._1))
    def pow = P(Index ~ "^" ~/ factor).map(v => OpImd(Operator.Pow, v._2, v._1))
    def eq = P(Index ~ "==" ~/ divMul).map(v => OpImd(ComparisonOperator.Eq, v._2, v._1))
    def notEq = P(Index ~ "!=" ~/ divMul).map(v => OpImd(ComparisonOperator.NotEq, v._2, v._1))
    def gt = P(Index ~ ">" ~/ divMul).map(v => OpImd(ComparisonOperator.Gt, v._2, v._1))
    def gtEq = P(Index ~ ">=" ~/ divMul).map(v => OpImd(ComparisonOperator.GtEq, v._2, v._1))
    def lt = P(Index ~ "<" ~/ divMul).map(v => OpImd(ComparisonOperator.Lt, v._2, v._1))
    def ltEq = P(Index ~ "<=" ~/ divMul).map(v => OpImd(ComparisonOperator.LtEq, v._2, v._1))

    def parens : P [Grouping] = P (Index ~ "(" ~/ addSub ~ ")" | Index ~ "[" ~/addSub ~ "]").map(v => Grouping(v._2, v._1))


    def divMul : Parser[Expr] = P (factor ~ (div | mul | mod | pow).rep).map(foldValues)
    def addSub : Parser[Expr] = P (divMul ~ (add | sub | eq | notEq | gt | lt | gtEq | ltEq).rep).map(foldValues)
    //    val expr = P (addSub ~ End)

    def foldValues(value : (Expr, Seq[OpImd])) : Expr = {
      val (base, ops) = value
      ops.foldLeft(base) {
        case (l : Expr, r : OpImd) =>
          // this would be a great place to implement implicit conversions
          //such as int to string, etc, or maybe that happens at compile time?
          Op(l, r.op, r.expr, r.index)
      }
    }

    P (addSub ~ End)
  }

  val str = P(Index ~ string).map(v => Literal(v._2, BaseClassTypes.String, v._1))
  val dig = P(Index ~ digits.! ~ Index).map(v => Literal(v._2, BaseClassTypes.Integer, v._1))
  val prompt = P(Index ~ ("prompt" ~ digits).! ~ Index).map(v => Literal(v._2, BaseClassTypes.Prompt, v._1))
  val expr = ooParser(str | dig | prompt)

  val inputString = "(1+prompt1)+1"
  val parsed = expr.parse(inputString)
  println (parsed)

  // now how to validate
  // valid combinations:
  // [int | str] + [int | str]
  // int - int
//  val test1 = AddOp(Literal("1", "Int", 1), Literal("2", "Prompt", 3),2)

  object BaseClassTypes {
    final val String = "java.lang.String"
    final val Integer = "java.lang.Integer"
    final val Boolean = "java.lang.Boolean"
    final val Prompt = "com.cisco.prompt.Playable"
  }

//  sealed trait ValidationResult {
//    def expr : Expr
//  }
//  sealed trait ValidationResultError extends ValidationResult {
//    def message : String
//    def index : Int
//  }
//  case class ValidationSuccessful(expr : Expr) extends ValidationResult
//  case class ValidationExpectedFound(expr : Expr, message : String, index : Int) extends ValidationResultError
//  case class ValidationInternalFailure(expr : Expr, message : String, index : Int) extends ValidationResultError
//  case class ValidationInvalidOperation(expr : Expr, message : String, index : Int) extends ValidationResultError
//
//  // recursively validate an operation expressions (left and right), then validate the operation itself
//  // a grouping is valid if the expression itself is valid. if it is invalid the validation error for the expression
//  // not the grouping, is returned
//
//
//  // need to be able to parse method calls
//  // 1.toString() = Method("toString", args = List(), Literal(1))
//
//  case class Accessor(methodName : String, args : Option[Seq[Expr]], expr : Expr, index : Int) extends Expr { def className = ??? }
//  val identifier = (Index ~ strChars(".").!).map(v => Literal(v._2, BaseClassTypes.String, v._1)) // strChars(".") should be any literal or reference parser.
//  val methodParser = P(identifier ~ ("." ~ strChars("()").! ~ "(" ~ (str | dig).rep(sep=",").? ~ ")").rep.?).map {
//      case (lit : Expr, Some(accessor)) =>
//        accessor.tail.foldLeft(accessor.head) {
//          case (l, r) =>
//            Accessor(l._1, l._2, lit, -1)
//        }
//    }
//  println (methodParser.parse("hello.toString(\"hello world\",3,4,5)"))
}
